﻿using System;
using System.Data;
using System.Xml;

namespace DXP
{
    public abstract class DxContent
    {
        public class Node
        {
            private XmlNode MyNode { get; set; }

            public string Value { get { return MyNode.InnerText; } set { MyNode.InnerText = string.Format("{0}", value).Trim(); } }

            private Node() { }
            internal Node(XmlNode node) { MyNode = node; }

            public Node Sub(string name) { return new Node(MyNode.AppendChild(MyNode.OwnerDocument.CreateElement(name))); }

            public void SetValue(string value) { Value = value; }
            public void SetAttr(string attrName, string attrValue)
            {
                XmlElement element = (XmlElement)MyNode;
                if (!element.HasAttribute(attrName))
                {
                    element.Attributes.Append(element.OwnerDocument.CreateAttribute(attrName));
                }
                element.SetAttribute(attrName, attrValue);
            }
            public string GetAttr(string attrName)
            {
                XmlElement element = (XmlElement)MyNode;
                if (element.HasAttribute(attrName))
                {
                    return element.GetAttribute(attrName);
                }
                return "";
            }
        }

        protected XmlDocument MyDom { get; private set; }
        public string MyID { get; protected set; }
        public DataRow MyRow { get; private set; }

        private DxContent() { }
        public DxContent(DataRow row)
        {
            MyID = DateTime.Now.Ticks.ToString();
            MyRow = row;
            MakeContent();
        }

        protected string S(string column, string format = "")
        {
            if (string.IsNullOrEmpty(column))
            {
                return string.Empty;
            }
            return string.Format(string.IsNullOrEmpty(format) ? "{0}" : ("{0:" + format.Trim() + "}"), MyRow[column]).Trim();
        }

        public Node Sub(string name) { return new Node(MyDom.AppendChild(MyDom.CreateElement(name))); }

        protected abstract void MakeContent();
    }
}
