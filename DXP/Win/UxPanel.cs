﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DXP.Win
{
    public class UxPanel : Panel
    {
        [Browsable(true)]
        [Description("边框宽度")]
        [Category("边框设置")]
        public int BorderWidth { get; set; }

        [Browsable(true)]
        [Description("边框颜色")]
        [Category("边框设置")]
        public Color BorderColor { get; set; }

        public UxPanel()
        {
            BorderWidth = 0;
            BorderColor = Color.Black;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (BorderWidth > 0)
            {
                SuspendLayout();
                ControlPaint.DrawBorder(e.Graphics, base.ClientRectangle, BorderColor, BorderWidth, ButtonBorderStyle.Solid, BorderColor, BorderWidth, ButtonBorderStyle.Solid, BorderColor, BorderWidth, ButtonBorderStyle.Solid, BorderColor, BorderWidth, ButtonBorderStyle.Solid);
                ResumeLayout(true);
            }
            else
            {
                base.OnPaint(e);
            }
        }
    }
}
