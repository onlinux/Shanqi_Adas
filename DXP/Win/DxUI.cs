﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace DXP.Win
{
    public static class DxUI
    {
        public static void DxSize(this Form form, int padding = 5)
        {
            if (form == null) { return; }
            if (padding < 0) { padding = 5; }
            //
            string direct = "XX";// E,W,N,S,NE,NW,SE,SW
            //
            Point pos = new Point(0);
            form.MouseLeave += (s, e) => { form.Cursor = Cursors.Default; };
            //
            form.MouseMove += (s, e) =>
            {
                if (e.Location.Y < form.Padding.Top)
                {
                    direct = (e.Location.X < form.Padding.Left ? "NW" : (e.Location.X > form.Width - form.Padding.Right ? "NE" : "NN"));
                    form.Cursor = (e.Location.X < form.Padding.Left ? Cursors.SizeNWSE : (e.Location.X > form.Width - form.Padding.Right ? Cursors.SizeNESW : Cursors.SizeNS));
                }
                else if (e.Location.Y > form.Height - form.Padding.Bottom)
                {
                    direct = (e.Location.X < form.Padding.Left ? "SW" : (e.Location.X > form.Width - form.Padding.Right ? "SE" : "SS"));
                    form.Cursor = (e.Location.X < form.Padding.Left ? Cursors.SizeNESW : (e.Location.X > form.Width - form.Padding.Right ? Cursors.SizeNWSE : Cursors.SizeNS));
                }
                else if (e.Location.X < form.Padding.Left) { direct = "WW"; form.Cursor = Cursors.SizeWE; }
                else if (e.Location.X > form.Width - form.Padding.Right) { direct = "EE"; form.Cursor = Cursors.SizeWE; }
                //
                if (e.Button == MouseButtons.Left)
                {
                    form.SuspendLayout();
                    if (direct == "NW" || direct == "NN" || direct == "NE") { form.Height = form.Bottom - Cursor.Position.Y; form.Top = Cursor.Position.Y; }
                    if (direct == "SW" || direct == "SS" || direct == "SE") { form.Height = Cursor.Position.Y - form.Top; }
                    if (direct == "NW" || direct == "WW" || direct == "SW") { form.Width = form.Right - Cursor.Position.X; form.Left = Cursor.Position.X; }
                    if (direct == "NE" || direct == "EE" || direct == "SE") { form.Width = Cursor.Position.X - form.Left; }
                    form.ResumeLayout(true);
                }
            };
        }
        //
        public static void DxMoveBy(this Form form, Control[] controls)
        {
            foreach (Control ctrl in controls)
            {
                ctrl.Cursor = Cursors.Hand;
                Point point = new Point();
                ctrl.MouseDown += (s, e) => { if (e.Button == MouseButtons.Left) { point.X = e.X; point.Y = e.Y; } };
                ctrl.MouseMove += (s, e) => { if (e.Button == MouseButtons.Left) { form.Left += (e.X - point.X); form.Top += (e.Y - point.Y); } };
            }
        }
        //
        public static void DxMaxBy(this Form form, PictureBox box)
        {
            box.Click += (s, e) =>
            {
                if (form.WindowState == FormWindowState.Normal)
                {
                    form.WindowState = FormWindowState.Maximized;
                }
                else if (form.WindowState == FormWindowState.Maximized)
                {
                    form.WindowState = FormWindowState.Normal;
                }
                form.Activate();
            };
        }
        //
        public static void DxSizeBox(PictureBox box)
        {
            box.Cursor = Cursors.Hand;
            Padding paddingOrig = box.Padding;
            Padding paddingSize = new Padding(paddingOrig.Left - 2, paddingOrig.Top - 2, paddingOrig.Right - 2, paddingOrig.Bottom - 2);

            box.MouseEnter += (s, e) =>
            {
                box.Padding = paddingSize;
                box.SizeMode = PictureBoxSizeMode.Zoom;
                box.SizeMode = PictureBoxSizeMode.StretchImage;
            };
            box.MouseLeave += (s, e) =>
            {
                box.Padding = paddingOrig;
                box.SizeMode = PictureBoxSizeMode.Zoom;
                box.SizeMode = PictureBoxSizeMode.StretchImage;
            };
        }
        //
        public static void Wait(int ms)
        {
            DateTime now = DateTime.Now;

            while (DateTime.Now.Subtract(now).TotalMilliseconds < ms)
            {
                Thread.Sleep(1);
                Application.DoEvents();
            }
        }
        //
        public static void DxAsync(this Control control, Action action, Action callback)
        {
            using (var worker = new BackgroundWorker())
            {
                worker.DoWork += (s, e) => { action(); };
                worker.RunWorkerCompleted += (s, e) => { if (callback != null) { control.Invoke(new MethodInvoker(callback)); } };
                worker.RunWorkerAsync();
            }
        }
        //
        public static Image IconToImage(Icon ico)
        {
            MemoryStream mStream = new MemoryStream();
            ico.Save(mStream);
            return Image.FromStream(mStream);
        }
        //
        public static Icon ImageToIcon(Image img)
        {
            using (MemoryStream msImg = new MemoryStream(), msIco = new MemoryStream())
            {
                img.Save(msImg, ImageFormat.Png);

                using (var bin = new BinaryWriter(msIco))
                {
                    //写图标头部
                    bin.Write((short)0);           //0-1保留
                    bin.Write((short)1);           //2-3文件类型。1=图标, 2=光标
                    bin.Write((short)1);           //4-5图像数量（图标可以包含多个图像）

                    bin.Write((byte)img.Width);  //6图标宽度
                    bin.Write((byte)img.Height); //7图标高度
                    bin.Write((byte)0);            //8颜色数（若像素位深>=8，填0。这是显然的，达到8bpp的颜色数最少是256，byte不够表示）
                    bin.Write((byte)0);            //9保留。必须为0
                    bin.Write((short)0);           //10-11调色板
                    bin.Write((short)32);          //12-13位深
                    bin.Write((int)msImg.Length);  //14-17位图数据大小
                    bin.Write(22);                 //18-21位图数据起始字节

                    //写图像数据
                    bin.Write(msImg.ToArray());

                    bin.Flush();
                    bin.Seek(0, SeekOrigin.Begin);
                    return new Icon(msIco);
                }
            }
        }
    }
}
