﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace DXP.Win
{
    public sealed class UxLogger : UserControl
    {
        private TabControl TabX;
        private readonly object locker = new object();
        private Dictionary<string, RichTextBox> Boxs = new Dictionary<string, RichTextBox>();

        public UxLogger()
        {
            this.Margin = new Padding(0);
            this.Padding = new Padding(0);
            this.Dock = DockStyle.Fill;
            TabX = new TabControl { ItemSize = new Size(20, 20), Padding = new Point(0), Margin = new Padding(0), Dock = DockStyle.Fill, Appearance = TabAppearance.FlatButtons };
            this.Controls.Add(TabX);

            this.Load += (xs, xe) => { DxLogger.Monitor += new DxLogger.LogMonitor(DxLogger_Monitor); };
        }
        private void DxLogger_Monitor(DxLogger.Log log)
        {
            #region
            Invoke(new MethodInvoker(delegate
            {
                lock (locker)
                {
                    string key = ((string.IsNullOrEmpty(log.Key) || log.Key == "_X_X_") ? "[默认]" : log.Key);
                    RichTextBox box = null;
                    if (Boxs.ContainsKey(key))
                    {
                        box = Boxs[key];
                    }
                    else
                    {
                        TabPage page = new TabPage { Name = log.Key, Text = key, Padding = new Padding(0), Margin = new Padding(0), BorderStyle = BorderStyle.None };
                        if (key == "[默认]")
                        {
                            TabX.TabPages.Insert(0, page);
                        }
                        else
                        {
                            TabX.TabPages.Add(page);
                        }
                        box = new RichTextBox { Dock = DockStyle.Fill, BorderStyle = BorderStyle.FixedSingle, ReadOnly = true, BackColor = Color.Black, Margin = new Padding(0) };
                        page.Controls.Add(box);
                        Boxs.Add(key, box);
                    }

                    if (box.Lines != null && box.Lines.Length > 3000)
                    {
                        box.Clear();
                    }
                    string text = string.Format("[{0:yyyy-MM-dd HH:mm:ss.ffff}] {1}", log.Time, log.Text);
                    box.Select(box.Text.Length, 0);
                    box.Focus();
                    box.SelectionColor = log.Color;
                    box.AppendText(text);
                    box.AppendText(Environment.NewLine);
                    box.Refresh();
                    box.ScrollToCaret();
                    Application.DoEvents();
                }
            }));
            #endregion
        }
    }
}
