﻿using System.Drawing;
using System.Windows.Forms;

namespace DXP.Win
{
    class ToolButton : ToolStripButton
    {
        private bool active = false;
        private bool hover = false;

        private Image backActive = null;
        private Image backSilent = null;

        private Color textActive = Color.Tomato;
        private Color textSilent = Color.RosyBrown;

        public bool Actived { get { return active; } set { active = value; Invalidate(); } }

        public Image BackActive { get { return backActive; } set { backActive = value; Invalidate(); } }
        public Image BackSilent { get { return backSilent; } set { backSilent = value; Invalidate(); } }

        public Color TextActive { get { return textActive; } set { textActive = value; Invalidate(); } }
        public Color TextSlient { get { return textSilent; } set { textSilent = value; Invalidate(); } }


        public ToolButton()
        {
            this.AutoSize = false;
            this.Click += (xs, xe) => { active = true; Invalidate(); };
            this.MouseEnter += (xs, xe) => { hover = true; Invalidate(); };
            this.MouseLeave += (xs, xe) => { hover = false; Invalidate(); };
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Width = (Parent.Width - Parent.Padding.Left - Parent.Padding.Right);
            //
            SizeF sz = e.Graphics.MeasureString(Text, Font, 500, new StringFormat { FormatFlags = StringFormatFlags.MeasureTrailingSpaces | StringFormatFlags.DirectionVertical });
            float W = sz.Width;
            float H = sz.Height;
            //
            //设置尺寸
            if (Image == null)
            {
                Height = (int)(Padding.Top + H + Padding.Bottom + 2);
            }
            else
            {
                Height = (int)(Padding.Top + (Width - Padding.Left - Padding.Right) + H + Padding.Bottom + 2);
            }
            //
            //绘背景
            e.Graphics.DrawImage(((active || hover) ? BackActive : BackSilent), new Rectangle(0, 0, Width, Height));
            //
            //绘图标
            if (Image != null)
            {
                int imgPad = 1, imgSize = (Width - Padding.Left - Padding.Right - imgPad * 2);
                Rectangle imgRect = new Rectangle(Padding.Left + imgPad, Padding.Top + imgPad, imgSize, imgSize);
                e.Graphics.DrawImage(Image, imgRect);
            }
            //
            //绘文字
            float left = (Width - W) / 2;
            float top = 0;
            if (Image == null)
            {
                top = (Height - H) / 2;
            }
            else
            {
                top = Width + (Height - Width - 2 - H) / 2;
            }
            RectangleF rect = new RectangleF(left, top, W, H);
            e.Graphics.DrawString(Text, Font, new SolidBrush(Actived ? TextActive : TextSlient), rect, new StringFormat(StringFormatFlags.DirectionVertical));
        }
    }
}
