﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DXP.Properties;

namespace DXP.Win
{
    public class UxApp : Form
    {
        #region 控件声明
        private IContainer AppContainer;
        private ToolTip AppTips;
        private NotifyIcon AppTray;
        private ToolStrip AppToolbar;
        private Panel AppHead, AppLeft, AppBody;
        protected Label AppTitle;
        protected PictureBox AppLogo, BtnMin, BtnMax, BtnExit, BtnTray;
        #endregion

        #region 构造函数
        protected UxApp(string title = "应用程序", Icon icon = null) : this() { Text = string.Format("{0}", title).Trim(); Icon = icon; AppTray.Icon = icon; }
        private UxApp()
        {
            this.Icon = Resources.AppIcon;
            //
            AppContainer = new Container();
            //
            #region Layout
            SuspendLayout();
            //
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            BackColor = Color.SteelBlue;
            Padding = new Padding(3);
            Size = new Size(1024, 704);
            //
            Controls.Add(AppBody = new Panel { Dock = DockStyle.Fill, BackColor = Color.SlateGray, BorderStyle = BorderStyle.Fixed3D, Margin = new Padding(0), Padding = new Padding(0) });
            Controls.Add(AppLeft = new Panel { Dock = DockStyle.Left, BackColor = Color.Snow, BorderStyle = BorderStyle.None, Width = 30, Padding = new Padding(0), Margin = new Padding(0) });
            Controls.Add(AppHead = new Panel { Dock = DockStyle.Top, BackColor = Color.Snow, BorderStyle = BorderStyle.None, Height = 30, Padding = new Padding(2), Margin = new Padding(0) });
            //
            AppHead.Controls.Add(AppTitle = new Label { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Padding = new Padding(5, 0, 0, 0), Font = new Font("微软雅黑", 12F, FontStyle.Bold), ForeColor = Color.Sienna });
            AppHead.Controls.Add(AppLogo = new PictureBox { Dock = DockStyle.Left, Width = 70, BackColor = Color.SteelBlue, BorderStyle = BorderStyle.FixedSingle, SizeMode = PictureBoxSizeMode.StretchImage });
            AppHead.Controls.Add(BtnTray = new PictureBox { Dock = DockStyle.Right, Width = (AppHead.Height - AppHead.Padding.Top - AppHead.Padding.Bottom), Padding = new Padding(5), SizeMode = PictureBoxSizeMode.StretchImage, Image = Resources.BtnTray });
            AppHead.Controls.Add(BtnMin = new PictureBox { Dock = DockStyle.Right, Width = (AppHead.Height - AppHead.Padding.Top - AppHead.Padding.Bottom), Padding = new Padding(5), SizeMode = PictureBoxSizeMode.StretchImage, Image = Resources.BtnTray });
            AppHead.Controls.Add(BtnMax = new PictureBox { Dock = DockStyle.Right, Width = (AppHead.Height - AppHead.Padding.Top - AppHead.Padding.Bottom), Padding = new Padding(5), SizeMode = PictureBoxSizeMode.StretchImage, Image = Resources.BtnMax });
            AppHead.Controls.Add(BtnExit = new PictureBox { Dock = DockStyle.Right, Width = (AppHead.Height - AppHead.Padding.Top - AppHead.Padding.Bottom), Padding = new Padding(5), SizeMode = PictureBoxSizeMode.StretchImage, Image = Resources.BtnExit });
            //
            AppLeft.Controls.Add(AppToolbar = new ToolStrip { Dock = DockStyle.Fill, BackColor = Color.Transparent, GripStyle = ToolStripGripStyle.Hidden, LayoutStyle = ToolStripLayoutStyle.VerticalStackWithOverflow, ShowItemToolTips = false, Stretch = true, Padding = new Padding(2) });
            //
            ResumeLayout(true);
            #endregion
            //
            #region AppTips
            AppTips = new ToolTip(AppContainer);
            AppTips.SetToolTip(BtnTray, "隐藏到托盘");
            AppTips.SetToolTip(BtnMax, "最大/还原");
            AppTips.SetToolTip(BtnMin, "最小化");
            AppTips.SetToolTip(BtnExit, "退出应用");
            #endregion
            //
            #region AppTray
            AppTray = new NotifyIcon(AppContainer) { ContextMenuStrip = new ContextMenuStrip() };
            AppTray.MouseClick += (s, e) =>
            {
                if (e.Button == MouseButtons.Left)
                {
                    this.Visible = true; this.Activate();
                    BtnExit.Tag = BtnExit.Visible;
                }
                else if (e.Button == MouseButtons.Right)
                {
                    AppTray.ContextMenuStrip.Items.Clear(); ;
                    AppTray.ContextMenuStrip.Items.Add("显示界面", DxUI.IconToImage(this.Icon), (xs, xe) => { this.Visible = true; this.Activate(); });
                    if (BtnExit.Visible)
                    {
                        AppTray.ContextMenuStrip.Items.Add("-");
                        AppTray.ContextMenuStrip.Items.Add("退出程序", BtnExit.Image, (xs, xe) => { this.Close(); });
                    }
                    AppTray.ContextMenuStrip.Show();
                }
            };
            #endregion
            //
            #region events
            this.Load += (s, e) => { AppTray.Icon = this.Icon = Resources.AppIcon; AppTray.Visible = true; };
            this.TextChanged += (xs, xe) => { AppTitle.Text = Text.Trim(); AppTray.Text = Text.Trim(); };
            AppTitle.TextChanged += (xs, xe) => { Text = AppTitle.Text.Trim(); AppTray.Text = AppTitle.Text.Trim(); };
            //
            DxUI.DxSizeBox(BtnMin);
            DxUI.DxSizeBox(BtnMax);
            DxUI.DxSizeBox(BtnTray);
            DxUI.DxSizeBox(BtnExit);
            //
            this.DxSize();
            this.DxMoveBy(new Control[] { AppLogo, AppTitle, AppHead });
            //
            BtnTray.Click += (s, e) => { AppTray.Icon = this.Icon; AppTray.Visible = true; this.Visible = false; };
            BtnMin.Click += (s, e) => { this.WindowState = FormWindowState.Minimized; };
            BtnMax.Click += (s, e) => { this.WindowState = (this.WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal); };
            BtnExit.Click += (s, e) => { this.Close(); };
            //
            FormClosing += (s, e) =>
            {
                if (DialogResult.No == DxBox.Show("确定退出程序？", "操作确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    e.Cancel = true;
                    return;
                }
                //
                if (AppTray != null)
                {
                    AppTray.Visible = false;
                    AppTray.Dispose();
                }
            };
            #endregion
        }
        #endregion

        #region 公开属性
        private Image toolBackActive = Resources.ToolBackActive, toolBackSilent = Resources.ToolBackSilent;
        //
        public Image ToolBackActive
        {
            get { return toolBackActive; }
            set { if (value != null) { toolBackActive = value; foreach (ToolButton item in AppToolbar.Items) { item.BackActive = ToolBackActive; } } }
        }
        //
        public Image ToolBackSilent
        {
            get { return toolBackSilent; }
            set { if (value != null) { toolBackActive = value; foreach (ToolButton item in AppToolbar.Items) { item.BackSilent = ToolBackSilent; } } }
        }
        #endregion

        #region 公开函数
        public void AddForm(int index, string text, Image icon, Form form, bool active = false)
        {
            if (form == null) { return; }
            //
            string txt = string.Format("{0}", text).Replace("\r", "").Replace("\n", "").Trim();
            //txt = string.Join("\r", text.Trim().ToCharArray()).Trim();
            //
            form.FormBorderStyle = FormBorderStyle.None;
            form.TopLevel = false;
            form.Dock = DockStyle.Fill;
            AppBody.Controls.Add(form);
            //
            ToolButton button = new ToolButton
            {
                Margin = new Padding(0, 0, 0, 2),
                Padding = new Padding(2),
                Image = icon,
                DisplayStyle = ToolStripItemDisplayStyle.ImageAndText,
                TextImageRelation = TextImageRelation.ImageAboveText,
                TextAlign = ContentAlignment.MiddleCenter,
                Font = new Font("微软雅黑", 14, FontStyle.Bold, GraphicsUnit.Pixel),
                BackActive = ToolBackActive,
                BackSilent = ToolBackSilent,
                BackgroundImageLayout = ImageLayout.Stretch,
                ForeColor = Color.SaddleBrown,
                Text = txt,
                Tag = form,
            };
            button.Click += (s, e) => { ClickButton(button); };
            AppToolbar.Items.Insert((index > AppToolbar.Items.Count ? AppToolbar.Items.Count : index), button);
            if (active) { ClickButton(button); }
        }
        //
        private void ClickButton(ToolButton button)
        {
            if (button == null) { return; }
            foreach (ToolButton item in AppToolbar.Items)
            {
                if (item != button) { item.Actived = false; }
            }
            button.Actived = true;
            //
            Text = string.Format("{0} - {1}", DxConfig.X().Get("App/Title", "应用程序"), button.Text.Replace("\r", "").Replace("\n", ""));
            //
            Form form = button.Tag as Form; if (form != null) { form.Show(); form.BringToFront(); }
        }

        #endregion

    }
}
