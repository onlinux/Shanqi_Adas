﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.IO.Ports;
using System;

namespace DXP.Win
{
    public class UxSerial : UserControl
    {
        #region
        private GroupBox GrpX;
        private TableLayoutPanel PnlX;
        private Label LblPortName, LblBaudRate, LblDataBits, LblStopBits, LblParity;
        private UxCombo CbxPortName, CbxStopBits, CbxParity;
        private NumericUpDown NbxBaudRate, NbxDataBits;

        public UxSerial()
        {
            InitControls();
            InitValues();
        }

        private void InitControls()
        {
            SuspendLayout();
            //
            this.AutoSize = true;
            this.Padding = new Padding(3);
            //
            Controls.Add(GrpX = new GroupBox { AutoSize = true, Dock = DockStyle.Fill, Padding = new Padding(5, 5, 5, 5), Text = "串口参数" });
            //
            GrpX.Controls.Add(PnlX = new TableLayoutPanel { AutoSize = true, Dock = DockStyle.Fill, ColumnCount = 2, RowCount = 6 });
            //
            PnlX.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 55F));
            PnlX.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            //
            PnlX.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            PnlX.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            PnlX.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            PnlX.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            PnlX.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            PnlX.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            //
            PnlX.Controls.Add(LblPortName = new Label { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = "串口名" }, 0, 0);
            PnlX.Controls.Add(CbxPortName = new UxCombo { Dock = DockStyle.Fill }, 1, 0);
            //
            PnlX.Controls.Add(LblBaudRate = new Label { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = "波特率" }, 0, 1);
            PnlX.Controls.Add(NbxBaudRate = new NumericUpDown { Dock = DockStyle.Fill, Maximum = 999999999 }, 1, 1);
            //
            PnlX.Controls.Add(LblDataBits = new Label { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = "数据位" }, 0, 2);
            PnlX.Controls.Add(NbxDataBits = new NumericUpDown { Dock = DockStyle.Fill }, 1, 2);
            //
            PnlX.Controls.Add(LblStopBits = new Label { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = "停止位" }, 0, 3);
            PnlX.Controls.Add(CbxStopBits = new UxCombo { Dock = DockStyle.Fill }, 1, 3);
            //
            PnlX.Controls.Add(LblParity = new Label { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = "校验位" }, 0, 4);
            PnlX.Controls.Add(CbxParity = new UxCombo { Dock = DockStyle.Fill }, 1, 4);
            //
            ResumeLayout(false);
        }

        private void InitValues()
        {

            CbxPortName.Items.Clear();
            CbxPortName.Items.Add("-");
            CbxPortName.Items.AddRange(SerialPort.GetPortNames());
            CbxPortName.SelectedIndex = 0;
            //
            NbxBaudRate.Value = 9600;
            //
            NbxDataBits.Value = 8;
            //
            CbxStopBits.Items.Clear();
            CbxStopBits.Items.AddRange(Enum.GetNames(typeof(StopBits)));
            CbxStopBits.Text = "One";
            //
            CbxParity.Items.Clear();
            CbxParity.Items.AddRange(Enum.GetNames(typeof(Parity)));
            CbxParity.Text = "None";
        }

        #endregion

        #region
        public string Title { get { return GrpX.Text; } set { GrpX.Text = string.Format("{0}", value).Trim(); } }
        public string PortName { get { return CbxPortName.Text.Trim(); } set { CbxPortName.Text = string.Format("{0}", value).Trim(); } }
        public int BaudRate { get { return (int)NbxBaudRate.Value; } set { NbxBaudRate.Value = value; } }
        public int DataBits { get { return (int)NbxDataBits.Value; } set { NbxDataBits.Value = value; } }
        public StopBits StopBits { get { return (StopBits)Enum.Parse(typeof(StopBits), CbxStopBits.Text); } set { CbxStopBits.Text = value.ToString(); } }
        public Parity Parity { get { return (Parity)Enum.Parse(typeof(Parity), CbxParity.Text); } set { CbxParity.Text = value.ToString(); } }
        #endregion
    }
}
