﻿using System.Drawing;
using System.Windows.Forms;

namespace DXP.Win
{
    public class UxCombo : ComboBox
    {
        public UxCombo()
        {
            base.DrawMode = DrawMode.OwnerDrawFixed;
            base.DropDownStyle = ComboBoxStyle.DropDownList;
            base.Items.Clear();
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            SuspendLayout();
            if (e.Index < 0)
            {
                base.OnDrawItem(e);
                return;
            }
            string text = GetItemText(base.Items[e.Index]);
            Rectangle rect = e.Bounds;
            e.DrawBackground();
            e.DrawFocusRectangle();
            e.Graphics.DrawString(text, e.Font, new SolidBrush(e.ForeColor), rect.X, rect.Y + 2);
            ResumeLayout(true);
        }
    }
}
