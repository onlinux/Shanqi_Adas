﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace DXP.Net
{
    public sealed class SerialAgent
    {
        public delegate void MessageHandler(string msg, bool send = false);

        public class Task
        {
            public string SendText { get; private set; }
            public int TimeoutMs { get; private set; }
            public int ReceTimes { get; private set; }
            public MessageHandler Callback { get; private set; }

            private Task() { }
            public Task(string text, MessageHandler callback, int timeoutMs = 1000, int receTimes = 1)
            {
                SendText = text;
                TimeoutMs = (timeoutMs > 0 ? timeoutMs : 1000);
                ReceTimes = (receTimes > 0 ? receTimes : 1);
                Callback = callback;
            }
        }

        private SerialPort serial = new SerialPort();
        private System.Timers.Timer timer = new System.Timers.Timer { Enabled = false, Interval = 5, };
        private Queue<Task> tasks = new Queue<Task>();

        public bool Working { get { return serial != null && serial.IsOpen; } }

        private SerialAgent() { }
        public SerialAgent(int port = 1, int baudRate = 9600, int dataBits = 8, StopBits stopBits = System.IO.Ports.StopBits.One, Parity parity = Parity.None)
        {
            serial.PortName = string.Format("COM{0}", port);
            serial.BaudRate = baudRate;
            serial.DataBits = dataBits;
            serial.StopBits = stopBits;
            serial.Parity = parity;

            timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;
            if (tasks.Count > 0 && Working)
            {
                Task task = tasks.Dequeue();

                string text = task.SendText.Replace("\r", "").TrimStart();

                if (!string.IsNullOrEmpty(text) && task.Callback != null)
                {
                    task.Callback.Invoke(text, true);
                }
                serial.DiscardInBuffer();
                serial.DiscardOutBuffer();
                serial.Write(task.SendText);
                if (task.Callback != null)
                {
                    DateTime now = DateTime.Now;
                    for (int i = 0; i < task.ReceTimes; i++)
                    {
                        while (serial.BytesToRead == 0 && DateTime.Now.Subtract(now).TotalMilliseconds < task.TimeoutMs)
                        {
                            Thread.Sleep(1);
                        } 
                        Thread.Sleep(50);
                        string result = string.Format("{0}", serial.ReadExisting()).TrimStart(text.ToCharArray()).Replace("\r", "").Replace(">", "").Trim();
                        task.Callback.Invoke(result);
                    }
                }
            }
            timer.Enabled = true;
        }

        public void Start()
        {
            try
            {
                if (!Working)
                {
                    serial.Open();
                    timer.Enabled = true;
                }
            }
            catch { }
        }

        public void Stop()
        {
            try
            {
                if (Working)
                {
                    serial.Close();
                    timer.Enabled = false;
                }
            }
            catch { }
        }

        public void Say(string text, MessageHandler callback = null, int timeoutMs = 5000, int receTimes = 1)
        {
            Start();
            if (Working)
            {
                tasks.Enqueue(new Task(text, callback, timeoutMs, receTimes));
            }
        }
    }
}
