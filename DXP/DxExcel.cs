﻿using System;
using System.Drawing;
using System.Xml;

public sealed class DxExcel
{
    public sealed class Sheet
    {
        public class Row
        {
            private readonly XmlNamespaceManager xnm;
            private readonly XmlElement row;

            private Row() { }
            internal Row(XmlNode node, XmlNamespaceManager nsManager) { row = (XmlElement)node; xnm = nsManager; }

            public Cell NewCell(string value = "") { return new Cell(row.AppendChild(row.OwnerDocument.CreateElement("Cell")), xnm, value); }
        }

        public class Cell
        {
            private readonly XmlNamespaceManager xnm;
            private readonly XmlElement cell;

            public string StyleID { get { return PreNode(cell, "@ss:StyleID", xnm).InnerText; } set { PreNode(cell, "@ss:StyleID", xnm).InnerText = value; } }
            public string ValueType { get { return PreNode(cell, "Data/@ss:Type", xnm).InnerText; } set { PreNode(cell, "Data/@ss:Type", xnm).InnerText = value; } }
            public string Value { get { return PreNode(cell, "Data", xnm).InnerText; } set { PreNode(cell, "Data", xnm).InnerText = value; } }

            private Cell() { }
            internal Cell(XmlNode node, XmlNamespaceManager nsManager, string value = "") { cell = (XmlElement)node; Value = value; xnm = nsManager; ValueType = "String"; }
        }

        private readonly XmlNamespaceManager xnm;
        private readonly XmlElement sheet;

        public string Name { get { return PreNode(sheet, "@ss:Name", xnm).Value; } set { PreNode(sheet, "@ss:Name", xnm).Value = value; } }

        private Sheet() { }
        internal Sheet(XmlNode node, XmlNamespaceManager nsManager) { sheet = (XmlElement)node; xnm = nsManager; }

        public Row NewRow() { return new Row(PreNode(sheet, "Table", xnm).AppendChild(sheet.OwnerDocument.CreateElement("Row")), xnm); }

        public void SetColumnWidth(int index, int width)
        {
            string path = string.Format("Table/Column[@ss:Index=\"{0}\"]/@ss:Width", index);
            PreNode(sheet, path, xnm).Value = ((width > 0) ? width : 100).ToString();
        }
    }
    public sealed class SheetCollection
    {
        private readonly XmlNamespaceManager xnm;
        private readonly XmlElement sheets;

        public int Count { get { return sheets.SelectNodes("Worksheet").Count; } }
        public Sheet this[int index]
        {
            get
            {
                XmlNodeList list = sheets.SelectNodes("WorkSheet");
                if (index >= 0 && index < list.Count)
                {
                    return new Sheet(list[index], xnm);
                }
                return null;
            }
        }
        public Sheet this[string name]
        {
            get
            {
                XmlNodeList list = sheets.SelectNodes("WorkSheet[@ss:Name=\"" + name + "\"]", xnm);
                if (list.Count > 0)
                {
                    return new Sheet(list[0], xnm);
                }
                return null;
            }
        }

        private SheetCollection() { }
        internal SheetCollection(XmlNode book, XmlNamespaceManager nsManager) { sheets = (XmlElement)book; xnm = nsManager; }

        public Sheet New()
        {
            string name = "";
            if (string.IsNullOrEmpty(name))
            {
                for (int i = 1; i < 255; i++)
                {
                    name = "Worksheet" + i;
                    if (this[name] == null)
                    {
                        break;
                    }
                }
            }
            return new Sheet(PreNode(sheets, "Worksheet[@ss:Name=\"" + name + "\"]", xnm), xnm);
        }
        public Sheet RemoveAt(int i)
        {
            XmlNodeList list = sheets.SelectNodes("WorkSheet");
            if (i >= 0 || i < list.Count)
            {
                return new Sheet(sheets.RemoveChild(list[i]), xnm);
            }
            return null;
        }
        public Sheet Remove(string name)
        {
            XmlNodeList list = sheets.SelectNodes("WorkSheet[@ss:Name='{" + name + "}']");
            if (list.Count > 0)
            {
                return new Sheet(sheets.RemoveChild(list[0]), xnm);
            }
            return null;
        }
        public void RemoveAll()
        {
            XmlNodeList list = sheets.SelectNodes("WorkSheet");
            foreach (XmlNode node in list)
            {
                sheets.RemoveChild(node);
            }
        }
    }
    public sealed class Style
    {
        public enum Vertical { Top, Center, Bottom }
        public enum Horizontal { Left, Center, Right, Justify, Distributed }

        private readonly XmlNamespaceManager xnm;
        private readonly XmlElement style;

        public string ID { get { return PreNode(style, "@ss:ID", xnm).Value; } set { PreNode(style, "@ss:ID", xnm).Value = value; } }
        public string Name { get { return PreNode(style, "@ss:Name", xnm).Value; } set { PreNode(style, "@ss:Name", xnm).Value = value; } }
        public string FontName { get { return PreNode(style, "Font/@ss:FontName", xnm).Value; } set { PreNode(style, "Font/@ss:FontName", xnm).Value = value; } }
        public int FontSize { get { return int.Parse(PreNode(style, "Font/@ss:Size", xnm).Value); } set { PreNode(style, "Font/@ss:Size", xnm).Value = value.ToString(); } }
        public Color FontColor { get { return ColorTranslator.FromHtml(PreNode(style, "Font/@ss:Color", xnm).Value); } set { PreNode(style, "Font/@ss:Color", xnm).Value = ColorToHex(value); } }
        public bool FontBold { get { return (PreNode(style, "Font/@ss:Bold", xnm).Value == "1") ? true : false; } set { PreNode(style, "Font/@ss:Bold", xnm).Value = (value ? "1" : "0"); } }
        public string InteriorPattern { get { return PreNode(style, "Interior/@ss:Pattern", xnm).Value; } set { PreNode(style, "Interior/@ss:Pattern", xnm).Value = value; } }
        public Color InteriorColor
        {
            get
            {
                return ColorTranslator.FromHtml(PreNode(style, "Interior/@ss:Color", xnm).Value);
            }
            set
            {
                PreNode(style, "Interior/@ss:Color", xnm).Value = ColorToHex(value);
                if (string.IsNullOrEmpty(InteriorPattern))
                {
                    InteriorPattern = "Solid";
                }
            }
        }
        public int BorderLeftWeight { get { return int.Parse(PreNode(style, "Borders/Border[@ss:Position=\"Left\"]/@ss:Weight", xnm).Value); } set { PreNode(style, "Borders/Border[@ss:Position=\"Left\"]/@ss:Weight", xnm).Value = value.ToString(); } }
        public string BorderLeftLineStyle { get { return PreNode(style, "Borders/Border[@ss:Position=\"Left\"]/@ss:LineStyle", xnm).Value; } set { PreNode(style, "Borders/Border[@ss:Position=\"Left\"]/@ss:LineStyle", xnm).Value = value; } }
        public int BorderRightWeight { get { return int.Parse(PreNode(style, "Borders/Border[@ss:Position=\"Right\"]/@ss:Weight", xnm).Value); } set { PreNode(style, "Borders/Border[@ss:Position=\"Right\"]/@ss:Weight", xnm).Value = value.ToString(); } }
        public string BorderRightLineStyle { get { return PreNode(style, "Borders/Border[@ss:Position=\"Right\"]/@ss:LineStyle", xnm).Value; } set { PreNode(style, "Borders/Border[@ss:Position=\"Right\"]/@ss:LineStyle", xnm).Value = value; } }
        public int BorderTopWeight { get { return int.Parse(PreNode(style, "Borders/Border[@ss:Position=\"Top\"]/@ss:Weight", xnm).Value); } set { PreNode(style, "Borders/Border[@ss:Position=\"Top\"]/@ss:Weight", xnm).Value = value.ToString(); } }
        public string BorderTopLineStyle { get { return PreNode(style, "Borders/Border[@ss:Position=\"Top\"]/@ss:LineStyle", xnm).Value; } set { PreNode(style, "Borders/Border[@ss:Position=\"Top\"]/@ss:LineStyle", xnm).Value = value; } }
        public int BorderBottomWeight { get { return int.Parse(PreNode(style, "Borders/Border[@ss:Position=\"Bottom\"]/@ss:Weight", xnm).Value); } set { PreNode(style, "Borders/Border[@ss:Position=\"Bottom\"]/@ss:Weight", xnm).Value = value.ToString(); } }
        public string BorderBottomLineStyle { get { return PreNode(style, "Borders/Border[@ss:Position=\"Bottom\"]/@ss:LineStyle", xnm).Value; } set { PreNode(style, "Borders/Border[@ss:Position=\"Bottom\"]/@ss:LineStyle", xnm).Value = value; } }
        public Horizontal TextHorizontal { get { return (Horizontal)Enum.Parse(typeof(Horizontal), PreNode(style, "Alignment/@ss:Horizontal", xnm).Value); } set { PreNode(style, "Alignment/@ss:Horizontal", xnm).Value = value.ToString(); } }
        public Vertical TextVertical { get { return (Vertical)Enum.Parse(typeof(Vertical), PreNode(style, "Alignment/@ss:Vertical", xnm).Value); } set { PreNode(style, "Alignment/@ss:Vertical", xnm).Value = value.ToString(); } }

        private Style() { }
        internal Style(XmlNode node, XmlNamespaceManager nsManager) { style = (XmlElement)node; xnm = nsManager; }

        public Style Copy() { return new Style(PreNode(style.OwnerDocument.DocumentElement, "Styles", xnm).AppendChild(style.Clone()), xnm) { ID = "" }; }
    }
    public sealed class StyleCollection
    {
        private readonly XmlNamespaceManager xnm;
        private readonly XmlElement styles;

        private StyleCollection() { }
        internal StyleCollection(XmlNode node, XmlNamespaceManager nsManager) { styles = (XmlElement)node; xnm = nsManager; }

        public Style New() { return new Style(PreNode(styles, "Style", xnm), xnm); }
    }

    private XmlDocument xml = new XmlDocument();
    private XmlNamespaceManager xnm;

    public SheetCollection Sheets { get; private set; }
    public StyleCollection Styles { get; private set; }

    public DxExcel()
    {
        xml.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><?mso-application progid=\"Excel.Sheet\"?><Workbook/>");
        xml.DocumentElement.SetAttribute("xmlns", "urn:schemas-microsoft-com:office:spreadsheet");
        xml.DocumentElement.SetAttribute("xmlns:o", "urn:schemas-microsoft-com:office:office");
        xml.DocumentElement.SetAttribute("xmlns:x", "urn:schemas-microsoft-com:office:excel");
        xml.DocumentElement.SetAttribute("xmlns:ss", "urn:schemas-microsoft-com:office:spreadsheet");
        xml.DocumentElement.SetAttribute("xmlns:html", "http://www.w3.org/TR/REC-html40");
        xml.DocumentElement.SetAttribute("xmlns:dt", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
        xnm = new XmlNamespaceManager(xml.NameTable);
        xnm.AddNamespace("", "urn:schemas-microsoft-com:office:spreadsheet");
        xnm.AddNamespace("o", "urn:schemas-microsoft-com:office:office");
        xnm.AddNamespace("x", "urn:schemas-microsoft-com:office:excel");
        xnm.AddNamespace("ss", "urn:schemas-microsoft-com:office:spreadsheet");
        xnm.AddNamespace("html", "http://www.w3.org/TR/REC-html40");
        xnm.AddNamespace("dt", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
        Sheets = new SheetCollection(xml.DocumentElement, xnm);
        Styles = new StyleCollection(PreNode(xml.DocumentElement, "Styles", xnm), xnm);
    }

    public void SaveAs(string filename) { xml.Save(filename); }

    internal static XmlNode PreNode(XmlNode parent, string nodePath, XmlNamespaceManager nsManager)
    {
        XmlNode node = parent;
        string[] items = string.Format("{0}", nodePath).Trim().Replace("\\", "/")
            .Split('/');
        foreach (string item in items)
        {
            if (string.IsNullOrEmpty(item))
            {
                continue;
            }
            if (node.SelectNodes(item, nsManager).Count == 0)
            {
                if (item.StartsWith("@"))
                {
                    string str = item.Substring(1);
                    if (str.IndexOf(":") > 0)
                    {
                        string prix4 = item.Substring(1).Split(':')[0];
                        string name4 = item.Substring(1).Split(':')[1];
                        node.Attributes.Append(node.OwnerDocument.CreateAttribute(prix4, name4, nsManager.LookupNamespace(prix4)));
                    }
                    else
                    {
                        node.Attributes.Append(node.OwnerDocument.CreateAttribute(str));
                    }
                }
                else if (item.IndexOf("[@") > 0)
                {
                    string nodeName = item.Substring(0, item.IndexOf("[@"));
                    XmlNode mynode;
                    if (nodeName.IndexOf(":") > 0)
                    {
                        string prix3 = nodeName.Split(':')[0];
                        string name3 = nodeName.Split(':')[1];
                        mynode = node.AppendChild(node.OwnerDocument.CreateNode(XmlNodeType.Element, prix3, name3, nsManager.LookupNamespace(prix3)));
                    }
                    else
                    {
                        mynode = node.AppendChild(node.OwnerDocument.CreateElement(nodeName));
                    }
                    string[] ss = item.Substring(item.IndexOf("[@") + 2).Replace("]", "").Replace("\"", "")
                        .Split('=');
                    string attrName = ss[0];
                    string attrValue = ss[1];
                    if (attrName.IndexOf(":") > 0)
                    {
                        string prix2 = attrName.Split(':')[0];
                        string name2 = attrName.Split(':')[1];
                        mynode.Attributes.Append(node.OwnerDocument.CreateAttribute(prix2, name2, nsManager.LookupNamespace(prix2))).Value = attrValue;
                    }
                    else
                    {
                        mynode.Attributes.Append(node.OwnerDocument.CreateAttribute(attrName)).Value = attrValue;
                    }
                }
                else if (item.IndexOf(":") > 0)
                {
                    string prix = item.Split(':')[0];
                    string name = item.Split(':')[1];
                    node.AppendChild(node.OwnerDocument.CreateNode(XmlNodeType.Element, prix, name, nsManager.LookupNamespace(prix)));
                }
                else
                {
                    node.AppendChild(node.OwnerDocument.CreateElement(item));
                }
            }
            node = node.SelectSingleNode(item, nsManager);
        }
        return node;
    }
    internal static string ColorToHex(Color color) { return "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2"); }
}
