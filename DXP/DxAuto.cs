﻿using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DXP
{
    public sealed class DxAuto
    {
        /// <summary>
        /// 所属注册表键
        /// </summary>
        private static string RegistryKey { get { return @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; } }
        /// <summary>
        /// 是否已存在应用的启动项
        /// </summary>
        /// <param name="appkey">应用标识</param>
        /// <returns></returns>
        public static bool Exists(string appkey) { { return new List<string>(Registry.CurrentUser.CreateSubKey(RegistryKey).GetValueNames()).Contains(appkey); } }
        /// <summary>
        /// 设置应用启动项
        /// </summary>
        /// <param name="appkey">应用标识</param>
        public static void SetAuto(string appkey) { Registry.CurrentUser.CreateSubKey(RegistryKey).SetValue(appkey, Application.ExecutablePath); }
        /// <summary>
        /// 删除应用启动项
        /// </summary>
        /// <param name="appkey"></param>
        public static void DelAuto(string appkey) { Registry.CurrentUser.CreateSubKey(RegistryKey).DeleteValue(appkey); }

    }
}
