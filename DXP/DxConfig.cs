﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace DXP
{
    public sealed class DxConfig
    {
        public static DxConfig X(string name = "")
        {
            FileInfo file = new FileInfo(Application.ExecutablePath);
            string key = file.Name.Replace(".EXE.", ".exe.");
            if (false == string.IsNullOrEmpty(name))
            {
                key = string.Format("{0}", name).Trim();
            }
            if (false == configs.ContainsKey(key))
            {
                configs.Add(key, new DxConfig(key));
            }
            return configs[key];
        }
        //
        public T Get<T>(string path, T nullValue = default(T)) { return RootNode.Get<T>(path, nullValue); }
        public void Set(string path, string value) { RootNode.Set(path, value); }
        public bool Save() { try { XmlDoc.Save(FilePath); return true; } catch { return false; } }
        //
        public bool AutoSave { get { return (RootNode.Get("@AutoSave", "1") == "1"); } set { RootNode.Set("@AutoSave", value ? "1" : "0"); } }
        //
        private static Dictionary<string, DxConfig> configs = new Dictionary<string, DxConfig>();
        private XmlDocument XmlDoc = new XmlDocument();
        private string FilePath { get; set; }
        private Node RootNode { get; set; }
        //
        private DxConfig() { }
        private DxConfig(string key)
        {
            try
            {
                FileInfo fi = new FileInfo(Path.Combine(Application.StartupPath, "config", key + ".dxc"));
                FilePath = fi.FullName;

                if (fi.Directory.Exists == false)
                {
                    fi.Directory.Create();
                }
                if (fi.Exists)
                {
                    XmlDoc.Load(fi.FullName);
                }
            }
            catch (Exception ex)
            {
                DxLogger.X().AddLog(ex.ToString(), Color.Red);
            }
            if (XmlDoc.DocumentElement == null)
            {
                XmlDoc.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><DXC></DXC>");
            }
            RootNode = new Node(this, XmlDoc.DocumentElement);
        }
        //
        #region Node
        private sealed class Node
        {
            public T Get<T>(string path, T nullValue = default(T))
            {
                try
                {
                    XmlNode node = PreNode(path);
                    if (string.IsNullOrEmpty(node.InnerText))
                    {
                        node.InnerText = string.Format("{0}", nullValue).Trim();
                        if (BaseConfig.AutoSave)
                        {
                            BaseConfig.Save();
                        }
                    }

                    Type type = typeof(T);
                    if (type.IsEnum)
                    {
                        return (T)Enum.Parse(type, node.InnerText);
                    }
                    return (T)Convert.ChangeType(node.InnerText, typeof(T));
                }
                catch
                {
                    return nullValue;
                }
            }
            //
            public void Set(string path, string value)
            {
                PreNode(path).InnerText = string.Format("{0}", value);
                if (BaseConfig.AutoSave)
                {
                    BaseConfig.Save();
                }
            }
            //
            private DxConfig BaseConfig { get; set; }
            private XmlNode BaseNode { get; set; }
            private Node() { }
            internal Node(DxConfig config, XmlNode node) { BaseConfig = config; BaseNode = node; }
            //
            private XmlNode PreNode(string path)
            {
                XmlNode node = BaseNode;
                //
                foreach (var str in string.Format("{0}", path).Trim().Replace(@"\", "/").Split('/'))
                {
                    string item = string.Format("{0}", str).Trim(); if (string.IsNullOrEmpty(item)) { continue; }
                    //
                    if (node.SelectNodes(item).Count == 0)
                    {
                        if (item.StartsWith("@"))
                        {
                            node.Attributes.Append(node.OwnerDocument.CreateAttribute(item.Replace("@", "").Trim()));
                        }
                        else if (item.IndexOf("[@") > 0)
                        {
                            string name = item.Substring(0, item.IndexOf("[@"));
                            string[] attr = item.Substring(item.IndexOf("[@") + 2).Replace("]", "").Replace("\"", "").Split('=');
                            node.AppendChild(node.OwnerDocument.CreateElement(name))
                                .Attributes.Append(node.OwnerDocument.CreateAttribute(attr[0].Trim())).Value = attr[1].Trim();
                        }
                        else
                        {
                            node.AppendChild(node.OwnerDocument.CreateElement(item));
                        }
                    }
                    node = node.SelectSingleNode(item);
                }
                //
                return node;
            }
        }
        #endregion
    }
}
