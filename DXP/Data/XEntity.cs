﻿using System.Collections.Generic;
using System.Linq;

namespace DXP.Data
{
    public abstract class XEntity
    {
        internal List<XField> Fields { get; private set; }
        public string Table { get; private set; }
        public bool Changed { get { return Fields.Count(o => o.IsAuto == false && o.Changed) > 0; } }

        private XEntity() { }
        protected XEntity(string table)
        {
            Fields = new List<XField>();
            Table = string.Format("{0}", table).Trim();
        }

        protected T Get<T>(string column, T nullValue)
        {
            if (string.IsNullOrEmpty(column)) { return nullValue; }

            if (Fields.Count(o => string.Compare(o.Column, string.Format("{0}", column).Trim(), true) == 0) == 0)
            {
                return nullValue;
            }
            else
            {
                return Fields.First(o => string.Compare(o.Column, string.Format("{0}", column).Trim(), true) == 0).Get<T>(nullValue);
            }
        }
        protected void Set<T>(string column, T value, bool iskey = false, bool isauto = false)
        {
            if (string.IsNullOrEmpty(column)) { return; }

            lock (Fields)
            {
                var list = Fields.Where(o => string.Compare(o.Column, string.Format("{0}", column).Trim(), true) == 0).ToList();
                if (list.Count > 0)
                {
                    list[0].SetNew(value);
                }
                else
                {
                    Fields.Add(new XField(column, iskey, isauto).SetNew(value));
                }
            }
        }
    }
}
