﻿using System;

namespace DXP.Data
{
    public class XField
    {
        public string Column { get; private set; }
        public bool IsKey { get; private set; }
        public bool IsAuto { get; private set; }
        public object ValNew { get; private set; }
        public object ValOld { get; private set; }
        public bool Changed { get { return (ValNew != ValOld); } }

        private XField() { }
        internal XField(string column, bool iskey = false, bool isauto = false) { Column = string.Format("{0}", column).Trim(); IsKey = iskey; IsAuto = isauto; }

        public XField SetNew(object value) { ValNew = value; return this; }
        public XField SetOld(object value) { ValOld = value; return this; }
        public T Get<T>(T nullValue = default(T))
        {
            if (ValNew == null)
            {
                return nullValue;
            }

            try
            {
                var type = typeof(T);
                if (type.IsEnum)
                {
                    return (T)Enum.Parse(type, string.Format("{0}", ValNew).Trim());
                }
                return (T)Convert.ChangeType(ValNew, type);
            }
            catch
            {
                return nullValue;
            }
        }
    }
}
