﻿
namespace DXP.Data
{
    public enum XDbType
    {
        Access = 0,
        SqlServer,
        MySql,
        Oracle,
    }
}
