﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace DXP.Data
{
    public class XContext : IDisposable
    {
        #region defines
        enum TaskType { Insert = 0, Update, Delete }

        class Task
        {
            public TaskType Type { get; private set; }
            public DateTime Time { get; private set; }
            public XEntity Entity { get; private set; }
            private Task() { }
            public Task(TaskType type, XEntity entity) { Type = type; Entity = entity; Time = DateTime.Now; }
        }
        #endregion

        #region properties
        private List<Task> Tasks = new List<Task>();
        private DbConnection DbConn = null;

        public XDbType DbType { get; private set; }
        public string ConnStr { get; private set; }
        #endregion

        #region construction
        private XContext() { }

        public XContext(XDbType dbType, string dbConnStr) { DbType = dbType; ConnStr = string.Format("{0}", dbConnStr).Trim(); }
        ~XContext() { Dispose(); }
        public void Dispose() { if (DbConn != null) { DbConn.Dispose(); } }

        #endregion

        #region entity methods
        public void InsertOnCommit(XEntity entity) { Tasks.Add(new Task(TaskType.Insert, entity)); }
        public void InsertOnCommit(IEnumerable<XEntity> entities) { Tasks.AddRange(entities.Select(o => new Task(TaskType.Insert, o))); }
        public void UpdateOnCommit(XEntity entity) { Tasks.Add(new Task(TaskType.Update, entity)); }
        public void UpdateOnCommit(IEnumerable<XEntity> entities) { Tasks.AddRange(entities.Select(o => new Task(TaskType.Insert, o))); }
        public void DeleteOnCommit(XEntity entity) { Tasks.Add(new Task(TaskType.Delete, entity)); }
        public void DeleteOnCommit(IEnumerable<XEntity> entities) { Tasks.AddRange(entities.Select(o => new Task(TaskType.Insert, o))); }
        public void ClearChanges() { Tasks.Clear(); }
        public bool CommitChanges()
        {
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    DbTransaction trans = conn.BeginTransaction();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.Transaction = trans;
                        foreach (var task in Tasks.OrderBy(o => o.Time))
                        {
                            cmd.Parameters.Clear();

                            #region 标记为删除的实体
                            if (task.Type == TaskType.Delete)
                            {
                                var fs = task.Entity.Fields.Where(o => o.IsKey == true).ToList();
                                cmd.CommandText = string.Format("DELETE FROM {0} WHERE {1}; ", task.Entity.Table, string.Join(" AND ", fs.Where(o => o.IsKey == true).Select(o => string.Format("{0}={1}{0}", o.Column, GetParaPrefix(DbType)))));
                                cmd.Parameters.AddRange(fs.Where(o => o.IsKey == true).Select(o => NewParameter(DbType, o.Column, o.ValNew)).ToArray());
                            }
                            #endregion

                            #region 标记为插入的实体
                            if (task.Type == TaskType.Insert)
                            {
                                var fs = task.Entity.Fields.Where(o => o.IsAuto == false && (o.Changed == true || o.IsKey == true)).ToList();

                                cmd.CommandText = string.Format("INSERT INTO {0}({1}) VALUES({2}); ", task.Entity.Table, string.Join(",", fs.Select(o => o.Column)), string.Join(",", fs.Select(o => string.Format("{0}{1}", GetParaPrefix(DbType), o.Column))));
                                cmd.Parameters.AddRange(fs.Select(o => NewParameter(DbType, o.Column, o.ValNew)).ToArray());
                            }
                            #endregion

                            #region 标记为更新的实体
                            if (task.Type == TaskType.Update)
                            {
                                var fs = task.Entity.Fields.Where(o => o.Changed == true || o.IsKey == true).ToList();
                                cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE {2}; ", task.Entity.Table, string.Join(",", fs.Where(o => o.IsKey == false).Select(o => string.Format("{0}={1}{0}", o.Column, GetParaPrefix(DbType)))), string.Join(" AND ", fs.Where(o => o.IsKey == true).Select(o => string.Format("{0}={1}{0}", o.Column, GetParaPrefix(DbType)))));
                                cmd.Parameters.AddRange(fs.Where(o => o.IsKey == false).Select(o => NewParameter(DbType, o.Column, o.ValNew)).ToArray());
                                cmd.Parameters.AddRange(fs.Where(o => o.IsKey == true).Select(o => NewParameter(DbType, o.Column, o.ValNew)).ToArray());
                            }
                            #endregion

                            cmd.ExecuteNonQuery();
                        }
                    }
                    try
                    {
                        trans.Commit();
                    }
                    catch
                    {
                        trans.Rollback(); return false;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region sql methods
        public XResult<int> RunSql(string sql, DbParameter[] parameters = null)
        {
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sql;
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        return new XResult<int>(true, cmd.ExecuteNonQuery());
                    }
                }
            }
            catch (Exception ex)
            {
                return new XResult<int>(false, 0, ex);
            }
        }
        public XResult<DataTable> GetTable(string sql, DbParameter[] parameters = null)
        {
            var table = new DataTable();
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sql;
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        using (var adapter = NewAdapter(DbType))
                        {
                            adapter.SelectCommand = cmd;
                            adapter.Fill(table);
                            return new XResult<DataTable>(true, table);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new XResult<DataTable>(false, table, ex);
            }
        }
        public XResult<DataSet> GetDataSet(string sql, DbParameter[] parameters = null)
        {
            var ds = new DataSet();
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sql;
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        using (var adapter = NewAdapter(DbType))
                        {
                            adapter.SelectCommand = cmd;
                            adapter.Fill(ds);
                            return new XResult<DataSet>(true, ds);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new XResult<DataSet>(false, ds, ex);
            }
        }
        public XResult<T> Scalar<T>(string sql, DbParameter[] parameters = null)
        {
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sql;
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        return new XResult<T>(true, (T)cmd.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                return new XResult<T>(false, default(T), ex);
            }
        }
        public XResult<T> Single<T>(string sql, DbParameter[] parameters = null) where T : new()
        {
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sql;
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                return new XResult<T>(true, ReaderToList<T>(reader)[0]);
                            }
                            else
                            {
                                return new XResult<T>(false, default(T), new Exception("无数据"));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new XResult<T>(false, default(T), ex);
            }
        }
        public XResult<List<T>> List<T>(string sql, DbParameter[] parameters = null) where T : new()
        {
            XResult<List<T>> result = new XResult<List<T>>(true, new List<T>());
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sql;
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                result = new XResult<List<T>>(true, ReaderToList<T>(reader));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = new XResult<List<T>>(false, new List<T>(), ex);
            }
            return result;
        }
        public XResult<XPager<T>> Page<T>(int pageSize, int pageIndex, string sqlCount, string sqlData, DbParameter[] parameters = null) where T : new()
        {
            XPager<T> xpr = new XPager<T> { PageSize = pageSize, PageIndex = pageIndex };
            try
            {
                using (var conn = NewConn(DbType, ConnStr))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }

                        cmd.CommandText = sqlCount;
                        xpr.DataCount = (int)cmd.ExecuteScalar();

                        cmd.CommandText = sqlData;
                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                xpr.PageData = ReaderToList<T>(reader);
                                return new XResult<XPager<T>>(true, xpr);
                            }
                            else
                            {
                                return new XResult<XPager<T>>(false, xpr, new Exception("没有数据"));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new XResult<XPager<T>>(false, xpr, ex);
            }
        }

        private List<T> ReaderToList<T>(DbDataReader reader) where T : new()
        {
            var list = new List<T>();
            while (reader.Read())
            {
                T xe = new T();
                foreach (var p in typeof(T).GetProperties().Where(o => o.CanWrite))
                {
                    try
                    {
                        object v = (reader[p.Name] is System.DBNull ? null : reader[p.Name]);
                        if (p.PropertyType.IsEnum)
                        {
                            p.SetValue(xe, Enum.ToObject(p.PropertyType, v), null);
                        }
                        else
                        {
                            p.SetValue(xe, v, null);
                        }
                    }
                    catch
                    {
                        p.SetValue(xe, null, null);
                    }
                }
                if (xe is XEntity)
                {
                    var entity = xe as XEntity;
                    foreach (var field in entity.Fields)
                    {
                        try
                        {
                            object val = (reader[field.Column] is System.DBNull ? null : reader[field.Column]);
                            field.SetNew(val).SetOld(val);
                        }
                        catch { }
                    }
                }
                list.Add(xe);
            }
            return list;
        }
        #endregion

        #region static
        public static DbProviderFactory GetFactory(XDbType dbType)
        {
            try
            {
                switch (dbType)
                {
                    case XDbType.SqlServer: return DbProviderFactories.GetFactory("System.Data.SqlClient");
                    case XDbType.MySql: return DbProviderFactories.GetFactory("MySql.Data.MySqlClient");
                    case XDbType.Access: return DbProviderFactories.GetFactory("System.Data.OleDb");
                    case XDbType.Oracle: return DbProviderFactories.GetFactory("Oracle.ManagedDataAccess.Client");
                    default: return null;
                }
            }
            catch { return null; }
        }
        public static DbConnection NewConn(XDbType dbType, string connstr)
        {
            var fact = GetFactory(dbType);
            if (fact != null)
            {
                DbConnection conn = fact.CreateConnection();
                conn.ConnectionString = connstr;
                return conn;
            }
            return null;
        }
        public static DbDataAdapter NewAdapter(XDbType dbType)
        {
            var fact = GetFactory(dbType);
            if (fact != null)
            {
                return fact.CreateDataAdapter();
            }
            return null;
        }
        public static DbParameter NewParameter(XDbType dbType, string name, object value)
        {
            DbParameter pm = null;
            var fact = GetFactory(dbType);
            if (fact != null)
            {
                pm = fact.CreateParameter();
            }
            if (pm != null)
            {
                pm.ParameterName = string.Format("{0}", name).Trim();
                pm.Value = value;

                if (dbType == XDbType.Access && value.GetType() == typeof(DateTime))
                {
                    pm.Value = string.Format("{0:yyyy-MM-dd HH:mm:ss}", value);
                }
            }
            return pm;
        }
        public static string GetParaPrefix(XDbType dbType)
        {
            switch (dbType)
            {
                case XDbType.Access: return "@";
                case XDbType.SqlServer: return "@";
                case XDbType.MySql: return "?";
                case XDbType.Oracle: return ":";
                default: return "@";
            }
        }
        #endregion
    }
}
