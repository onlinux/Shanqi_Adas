﻿using System;

namespace DXP.Data
{
    public class XResult
    {
        public bool Succeed { get; protected set; }
        public Exception Error { get; protected set; }

        private XResult() { }
        public XResult(bool succeed = true, Exception error = null)
        {
            Succeed = succeed;
            Error = error;
        }
    }

    public class XResult<T> : XResult
    {
        public T Data { get; protected set; }

        public XResult(bool succeed = true, T data = default(T), Exception error = null) : base(succeed, error) { Data = data; }
    }
}
