﻿using System.Collections.Generic;

namespace DXP.Data
{
    public class XPager<T>
    {
        private int pageSize = 30, pageIndex = 0, dataCount = 0, pageCount = 0;
        private List<T> list = new List<T>();

        public int PageSize { get { return pageSize; } set { pageSize = value; CalcPropery(); } }
        public int PageIndex { get { return pageIndex; } set { pageIndex = value; CalcPropery(); } }
        public int DataCount { get { return dataCount; } set { dataCount = value; CalcPropery(); } }
        public int PageCount { get { return pageCount; } }
        public List<T> PageData { get { return list; } set { list = value; if (list == null) { list = new List<T>(); } } }

        private void CalcPropery()
        {
            pageSize = (pageSize < 1 ? 30 : pageSize);
            pageIndex = (pageIndex < 0 ? 0 : pageIndex);
            dataCount = (dataCount < 0 ? 0 : dataCount);
            int mod = dataCount % pageSize;
            pageCount = (dataCount / pageSize) + (mod > 0 ? 1 : 0);
        }
    }
}
