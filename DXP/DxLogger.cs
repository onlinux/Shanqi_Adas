﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DXP
{
    public sealed class DxLogger
    {
        #region static
        private static readonly Dictionary<string, DxLogger> loggers = new Dictionary<string, DxLogger>();
        public static event LogMonitor Monitor;
        public static DxLogger X(string key = "")
        {
            lock (loggers)
            {
                string x = string.Format("{0}", key).Trim();
                if (string.IsNullOrEmpty(x))
                {
                    x = "";
                }
                if (false == loggers.ContainsKey(x))
                {
                    loggers.Add(x, new DxLogger(x));
                }
                return loggers[x];
            }
        }
        #endregion

        #region properties
        private readonly ConcurrentQueue<Log> que = new ConcurrentQueue<Log>();
        private System.Timers.Timer timer = new System.Timers.Timer { Interval = 5, Enabled = false };
        private StreamWriter writer = null;

        public string Key { get; private set; }
        public string FilePath { get; private set; }
        #endregion

        #region construct
        private DxLogger() { }
        private DxLogger(string key)
        {
            Key = string.Format("{0}", key).Trim();
            if (string.IsNullOrEmpty(Key)) { Key = "_X_X_"; }
            string name = string.Format("{0:yyyyMMdd}_{1}.log", DateTime.Now, Key).Replace("_X_X_.log", ".log");
            //
            FilePath = Path.Combine(Application.StartupPath, "log", name);
            //
            timer.Elapsed += (s, e) =>
            {
                timer.Enabled = false;
                if (Monitor != null && Monitor.GetInvocationList().Length > 0)
                {
                    Log log;
                    if (que.Count > 0 && que.TryDequeue(out log))
                    {
                        try
                        {
                            if (writer == null)
                            {
                                FileInfo file = new FileInfo(FilePath);
                                if (!file.Directory.Exists) { file.Directory.Create(); }
                                writer = new StreamWriter(FilePath, true, Encoding.UTF8) { AutoFlush = true };
                            }
                            writer.WriteLine(string.Format("[{0:yyyy-MM-dd HH:mm:ss.fff}] {1}", log.Time, log.Text));
                        }
                        catch { }
                        foreach (var de in Monitor.GetInvocationList())
                        {
                            try { ((LogMonitor)de).Invoke(log); }
                            catch { }
                        }
                    }
                }
                timer.Enabled = true;
            };
            timer.Enabled = true;
        }
        #endregion

        #region methods
        public void AddLog(string text, Color color)
        {
            que.Enqueue(new Log(text, color, Key));
        }
        public void AddLog(string text)
        {
            AddLog(text, Color.LightCyan);
        }
        #endregion

        #region defines
        public delegate void LogMonitor(Log log);
        public class Log
        {
            public DateTime Time { get; private set; }
            public string Key { get; private set; }
            public string Text { get; private set; }
            public Color Color { get; private set; }
            private Log() { }
            internal Log(string text, Color color, string key = "")
            {
                Time = DateTime.Now;
                Key = string.Format("{0}", key).Trim();
                Text = string.Format("{0}", text).Trim();
                Color = color;
            }
        }
        #endregion
    }
}
