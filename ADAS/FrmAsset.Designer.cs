﻿namespace ADAS
{
    partial class FrmAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAsset));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.axiSwitchLedX4 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.axiLedRoundX7 = new AxisDigitalLibrary.AxiLedRoundX();
            this.label8 = new System.Windows.Forms.Label();
            this.axiSwitchLedX3 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.axiLedRoundX6 = new AxisDigitalLibrary.AxiLedRoundX();
            this.label7 = new System.Windows.Forms.Label();
            this.axiSwitchLedX2 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.axiLedRoundX5 = new AxisDigitalLibrary.AxiLedRoundX();
            this.label2 = new System.Windows.Forms.Label();
            this.axiSwitchLedX1 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.axiLedRoundX4 = new AxisDigitalLibrary.AxiLedRoundX();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.axiLedRectangleX1 = new AxisDigitalLibrary.AxiLedRectangleX();
            this.label20 = new System.Windows.Forms.Label();
            this.AxManual = new AxisDigitalLibrary.AxiLedRoundX();
            this.label19 = new System.Windows.Forms.Label();
            this.AxAuto = new AxisDigitalLibrary.AxiLedRoundX();
            this.label18 = new System.Windows.Forms.Label();
            this.AxPowerOn = new AxisDigitalLibrary.AxiLedRoundX();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.AxPusherPosOk = new AxisDigitalLibrary.AxiLedRoundX();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.axiSwitchLedX11 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.label12 = new System.Windows.Forms.Label();
            this.axiSwitchLedX10 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.label11 = new System.Windows.Forms.Label();
            this.axiSwitchLedX9 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.GrpFcw = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.axiSwitchLedX24 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxMoveFcwUD = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxTargetFcwUD = new AxisDigitalLibrary.AxiIntegerOutputX();
            this.label9 = new System.Windows.Forms.Label();
            this.AxZeroFcwUD = new AxisDigitalLibrary.AxiSwitchLedX();
            this.label10 = new System.Windows.Forms.Label();
            this.AxPosOkFcwUD = new AxisDigitalLibrary.AxiLedRoundX();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.axiSwitchLedX23 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxMoveFcwLR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxTargetFcwLR = new AxisDigitalLibrary.AxiIntegerOutputX();
            this.label5 = new System.Windows.Forms.Label();
            this.AxZeroFcwLR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.label6 = new System.Windows.Forms.Label();
            this.AxPosOkFcwLR = new AxisDigitalLibrary.AxiLedRoundX();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.axiSwitchLedX22 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxMoveFcwFR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxTargetFcwFR = new AxisDigitalLibrary.AxiIntegerOutputX();
            this.label4 = new System.Windows.Forms.Label();
            this.AxZeroFcwFR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.label3 = new System.Windows.Forms.Label();
            this.AxPosOkFcwFR = new AxisDigitalLibrary.AxiLedRoundX();
            this.GrpLdw = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.axiSwitchLedX25 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxMoveLdwUD = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxTargetLdwUD = new AxisDigitalLibrary.AxiIntegerOutputX();
            this.label15 = new System.Windows.Forms.Label();
            this.AxZeroLdwUD = new AxisDigitalLibrary.AxiSwitchLedX();
            this.label16 = new System.Windows.Forms.Label();
            this.AxPosOkLdwUD = new AxisDigitalLibrary.AxiLedRoundX();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.axiSwitchLedX26 = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxMoveLdwFR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxTargetLdwFR = new AxisDigitalLibrary.AxiIntegerOutputX();
            this.label13 = new System.Windows.Forms.Label();
            this.AxZeroLdwFR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.label14 = new System.Windows.Forms.Label();
            this.AxPosOkLdwFR = new AxisDigitalLibrary.AxiLedRoundX();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.AsObdWireless = new AxisDigitalLibrary.AxiLedRectangleX();
            this.label24 = new System.Windows.Forms.Label();
            this.AsObdWired = new AxisDigitalLibrary.AxiLedRectangleX();
            this.label25 = new System.Windows.Forms.Label();
            this.AsScaner = new AxisDigitalLibrary.AxiLedRectangleX();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.AxPusherRunR = new AxisDigitalLibrary.AxiLedRoundX();
            this.label21 = new System.Windows.Forms.Label();
            this.AxPusherInR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxPusherOutR = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxPusherForcePercentR = new AxisDigitalLibrary.AxiIntegerOutputX();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.AxPusherRunF = new AxisDigitalLibrary.AxiLedRoundX();
            this.label22 = new System.Windows.Forms.Label();
            this.AxPusherInF = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxPusherOutF = new AxisDigitalLibrary.AxiSwitchLedX();
            this.AxPusherForcePercentF = new AxisDigitalLibrary.AxiIntegerOutputX();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX4)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRectangleX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxManual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPowerOn)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherPosOk)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX9)).BeginInit();
            this.GrpFcw.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveFcwUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetFcwUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroFcwUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkFcwUD)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveFcwLR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetFcwLR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroFcwLR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkFcwLR)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveFcwFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetFcwFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroFcwFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkFcwFR)).BeginInit();
            this.GrpLdw.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveLdwUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetLdwUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroLdwUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkLdwUD)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveLdwFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetLdwFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroLdwFR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkLdwFR)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AsObdWireless)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsObdWired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsScaner)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherRunR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherInR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherOutR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherForcePercentR)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherRunF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherInF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherOutF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherForcePercentF)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.axiSwitchLedX4);
            this.groupBox1.Controls.Add(this.axiLedRoundX7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.axiSwitchLedX3);
            this.groupBox1.Controls.Add(this.axiLedRoundX6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.axiSwitchLedX2);
            this.groupBox1.Controls.Add(this.axiLedRoundX5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.axiSwitchLedX1);
            this.groupBox1.Controls.Add(this.axiLedRoundX4);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox1.Location = new System.Drawing.Point(530, 20);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox1.Size = new System.Drawing.Size(490, 60);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "机柜塔灯";
            // 
            // axiSwitchLedX4
            // 
            this.axiSwitchLedX4.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiSwitchLedX4.Location = new System.Drawing.Point(248, 28);
            this.axiSwitchLedX4.Name = "axiSwitchLedX4";
            this.axiSwitchLedX4.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX4.OcxState")));
            this.axiSwitchLedX4.Size = new System.Drawing.Size(40, 22);
            this.axiSwitchLedX4.TabIndex = 65;
            // 
            // axiLedRoundX7
            // 
            this.axiLedRoundX7.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiLedRoundX7.Location = new System.Drawing.Point(226, 28);
            this.axiLedRoundX7.Name = "axiLedRoundX7";
            this.axiLedRoundX7.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiLedRoundX7.OcxState")));
            this.axiLedRoundX7.Size = new System.Drawing.Size(22, 22);
            this.axiLedRoundX7.TabIndex = 64;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.SlateGray;
            this.label8.Location = new System.Drawing.Point(216, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 22);
            this.label8.TabIndex = 63;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // axiSwitchLedX3
            // 
            this.axiSwitchLedX3.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiSwitchLedX3.Location = new System.Drawing.Point(176, 28);
            this.axiSwitchLedX3.Name = "axiSwitchLedX3";
            this.axiSwitchLedX3.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX3.OcxState")));
            this.axiSwitchLedX3.Size = new System.Drawing.Size(40, 22);
            this.axiSwitchLedX3.TabIndex = 60;
            // 
            // axiLedRoundX6
            // 
            this.axiLedRoundX6.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiLedRoundX6.Location = new System.Drawing.Point(154, 28);
            this.axiLedRoundX6.Name = "axiLedRoundX6";
            this.axiLedRoundX6.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiLedRoundX6.OcxState")));
            this.axiLedRoundX6.Size = new System.Drawing.Size(22, 22);
            this.axiLedRoundX6.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.SlateGray;
            this.label7.Location = new System.Drawing.Point(144, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 22);
            this.label7.TabIndex = 58;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // axiSwitchLedX2
            // 
            this.axiSwitchLedX2.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiSwitchLedX2.Location = new System.Drawing.Point(104, 28);
            this.axiSwitchLedX2.Name = "axiSwitchLedX2";
            this.axiSwitchLedX2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX2.OcxState")));
            this.axiSwitchLedX2.Size = new System.Drawing.Size(40, 22);
            this.axiSwitchLedX2.TabIndex = 53;
            // 
            // axiLedRoundX5
            // 
            this.axiLedRoundX5.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiLedRoundX5.Location = new System.Drawing.Point(82, 28);
            this.axiLedRoundX5.Name = "axiLedRoundX5";
            this.axiLedRoundX5.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiLedRoundX5.OcxState")));
            this.axiLedRoundX5.Size = new System.Drawing.Size(22, 22);
            this.axiLedRoundX5.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.SlateGray;
            this.label2.Location = new System.Drawing.Point(72, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 22);
            this.label2.TabIndex = 51;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // axiSwitchLedX1
            // 
            this.axiSwitchLedX1.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiSwitchLedX1.Location = new System.Drawing.Point(32, 28);
            this.axiSwitchLedX1.Name = "axiSwitchLedX1";
            this.axiSwitchLedX1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX1.OcxState")));
            this.axiSwitchLedX1.Size = new System.Drawing.Size(40, 22);
            this.axiSwitchLedX1.TabIndex = 44;
            // 
            // axiLedRoundX4
            // 
            this.axiLedRoundX4.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiLedRoundX4.Location = new System.Drawing.Point(10, 28);
            this.axiLedRoundX4.Name = "axiLedRoundX4";
            this.axiLedRoundX4.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiLedRoundX4.OcxState")));
            this.axiLedRoundX4.Size = new System.Drawing.Size(22, 22);
            this.axiLedRoundX4.TabIndex = 43;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.axiLedRectangleX1);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.AxManual);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.AxAuto);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.AxPowerOn);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox2.Location = new System.Drawing.Point(20, 20);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox2.Size = new System.Drawing.Size(490, 60);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "机柜按钮";
            // 
            // axiLedRectangleX1
            // 
            this.axiLedRectangleX1.Dock = System.Windows.Forms.DockStyle.Right;
            this.axiLedRectangleX1.Location = new System.Drawing.Point(458, 28);
            this.axiLedRectangleX1.Name = "axiLedRectangleX1";
            this.axiLedRectangleX1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiLedRectangleX1.OcxState")));
            this.axiLedRectangleX1.Size = new System.Drawing.Size(22, 22);
            this.axiLedRectangleX1.TabIndex = 48;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.SlateGray;
            this.label20.Location = new System.Drawing.Point(156, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 22);
            this.label20.TabIndex = 47;
            this.label20.Text = "手动";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxManual
            // 
            this.AxManual.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxManual.Location = new System.Drawing.Point(134, 28);
            this.AxManual.Name = "AxManual";
            this.AxManual.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxManual.OcxState")));
            this.AxManual.Size = new System.Drawing.Size(22, 22);
            this.AxManual.TabIndex = 46;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Left;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.SlateGray;
            this.label19.Location = new System.Drawing.Point(94, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 22);
            this.label19.TabIndex = 45;
            this.label19.Text = "自动";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxAuto
            // 
            this.AxAuto.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxAuto.Location = new System.Drawing.Point(72, 28);
            this.AxAuto.Name = "AxAuto";
            this.AxAuto.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxAuto.OcxState")));
            this.AxAuto.Size = new System.Drawing.Size(22, 22);
            this.AxAuto.TabIndex = 44;
            this.AxAuto.OnChange += new System.EventHandler(this.AxAuto_OnChange);
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.SlateGray;
            this.label18.Location = new System.Drawing.Point(32, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 22);
            this.label18.TabIndex = 43;
            this.label18.Text = "上电";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPowerOn
            // 
            this.AxPowerOn.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPowerOn.Location = new System.Drawing.Point(10, 28);
            this.AxPowerOn.Name = "AxPowerOn";
            this.AxPowerOn.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPowerOn.OcxState")));
            this.AxPowerOn.Size = new System.Drawing.Size(22, 22);
            this.AxPowerOn.TabIndex = 42;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.AxPusherPosOk);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox3.Location = new System.Drawing.Point(530, 177);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox3.Size = new System.Drawing.Size(490, 60);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "摆正器";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.SlateGray;
            this.label1.Location = new System.Drawing.Point(92, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 22);
            this.label1.TabIndex = 47;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Left;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.SlateGray;
            this.label17.Location = new System.Drawing.Point(32, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 22);
            this.label17.TabIndex = 44;
            this.label17.Text = "到位光电";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPusherPosOk
            // 
            this.AxPusherPosOk.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPusherPosOk.Location = new System.Drawing.Point(10, 28);
            this.AxPusherPosOk.Name = "AxPusherPosOk";
            this.AxPusherPosOk.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherPosOk.OcxState")));
            this.AxPusherPosOk.Size = new System.Drawing.Size(22, 22);
            this.AxPusherPosOk.TabIndex = 43;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.axiSwitchLedX11);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.axiSwitchLedX10);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.axiSwitchLedX9);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox4.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox4.Location = new System.Drawing.Point(530, 100);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox4.Size = new System.Drawing.Size(490, 60);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "操作台";
            // 
            // axiSwitchLedX11
            // 
            this.axiSwitchLedX11.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiSwitchLedX11.Location = new System.Drawing.Point(110, 28);
            this.axiSwitchLedX11.Name = "axiSwitchLedX11";
            this.axiSwitchLedX11.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX11.OcxState")));
            this.axiSwitchLedX11.Size = new System.Drawing.Size(40, 22);
            this.axiSwitchLedX11.TabIndex = 53;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.SlateGray;
            this.label12.Location = new System.Drawing.Point(100, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 22);
            this.label12.TabIndex = 52;
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // axiSwitchLedX10
            // 
            this.axiSwitchLedX10.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiSwitchLedX10.Location = new System.Drawing.Point(60, 28);
            this.axiSwitchLedX10.Name = "axiSwitchLedX10";
            this.axiSwitchLedX10.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX10.OcxState")));
            this.axiSwitchLedX10.Size = new System.Drawing.Size(40, 22);
            this.axiSwitchLedX10.TabIndex = 50;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Left;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.SlateGray;
            this.label11.Location = new System.Drawing.Point(50, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 22);
            this.label11.TabIndex = 49;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // axiSwitchLedX9
            // 
            this.axiSwitchLedX9.Dock = System.Windows.Forms.DockStyle.Left;
            this.axiSwitchLedX9.Location = new System.Drawing.Point(10, 28);
            this.axiSwitchLedX9.Name = "axiSwitchLedX9";
            this.axiSwitchLedX9.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX9.OcxState")));
            this.axiSwitchLedX9.Size = new System.Drawing.Size(40, 22);
            this.axiSwitchLedX9.TabIndex = 46;
            // 
            // GrpFcw
            // 
            this.GrpFcw.Controls.Add(this.groupBox8);
            this.GrpFcw.Controls.Add(this.groupBox7);
            this.GrpFcw.Controls.Add(this.groupBox6);
            this.GrpFcw.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.GrpFcw.ForeColor = System.Drawing.Color.SteelBlue;
            this.GrpFcw.Location = new System.Drawing.Point(20, 341);
            this.GrpFcw.Margin = new System.Windows.Forms.Padding(0);
            this.GrpFcw.Name = "GrpFcw";
            this.GrpFcw.Padding = new System.Windows.Forms.Padding(10);
            this.GrpFcw.Size = new System.Drawing.Size(490, 220);
            this.GrpFcw.TabIndex = 6;
            this.GrpFcw.TabStop = false;
            this.GrpFcw.Text = "FCW (雷达标定)";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.axiSwitchLedX24);
            this.groupBox8.Controls.Add(this.AxMoveFcwUD);
            this.groupBox8.Controls.Add(this.AxTargetFcwUD);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Controls.Add(this.AxZeroFcwUD);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Controls.Add(this.AxPosOkFcwUD);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox8.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox8.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox8.Location = new System.Drawing.Point(10, 148);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox8.Size = new System.Drawing.Size(470, 60);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "上下移动";
            // 
            // axiSwitchLedX24
            // 
            this.axiSwitchLedX24.Dock = System.Windows.Forms.DockStyle.Right;
            this.axiSwitchLedX24.Location = new System.Drawing.Point(395, 28);
            this.axiSwitchLedX24.Name = "axiSwitchLedX24";
            this.axiSwitchLedX24.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX24.OcxState")));
            this.axiSwitchLedX24.Size = new System.Drawing.Size(65, 22);
            this.axiSwitchLedX24.TabIndex = 57;
            // 
            // AxMoveFcwUD
            // 
            this.AxMoveFcwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxMoveFcwUD.Location = new System.Drawing.Point(322, 28);
            this.AxMoveFcwUD.Name = "AxMoveFcwUD";
            this.AxMoveFcwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxMoveFcwUD.OcxState")));
            this.AxMoveFcwUD.Size = new System.Drawing.Size(65, 22);
            this.AxMoveFcwUD.TabIndex = 56;
            // 
            // AxTargetFcwUD
            // 
            this.AxTargetFcwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxTargetFcwUD.Location = new System.Drawing.Point(262, 28);
            this.AxTargetFcwUD.Name = "AxTargetFcwUD";
            this.AxTargetFcwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxTargetFcwUD.OcxState")));
            this.AxTargetFcwUD.Size = new System.Drawing.Size(60, 22);
            this.AxTargetFcwUD.TabIndex = 55;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.SlateGray;
            this.label9.Location = new System.Drawing.Point(157, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 22);
            this.label9.TabIndex = 54;
            this.label9.Text = "目标位置(mm):";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AxZeroFcwUD
            // 
            this.AxZeroFcwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxZeroFcwUD.Location = new System.Drawing.Point(92, 28);
            this.AxZeroFcwUD.Name = "AxZeroFcwUD";
            this.AxZeroFcwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxZeroFcwUD.OcxState")));
            this.AxZeroFcwUD.Size = new System.Drawing.Size(65, 22);
            this.AxZeroFcwUD.TabIndex = 53;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.SlateGray;
            this.label10.Location = new System.Drawing.Point(32, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 22);
            this.label10.TabIndex = 52;
            this.label10.Text = "到位状态";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPosOkFcwUD
            // 
            this.AxPosOkFcwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPosOkFcwUD.Location = new System.Drawing.Point(10, 28);
            this.AxPosOkFcwUD.Name = "AxPosOkFcwUD";
            this.AxPosOkFcwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPosOkFcwUD.OcxState")));
            this.AxPosOkFcwUD.Size = new System.Drawing.Size(22, 22);
            this.AxPosOkFcwUD.TabIndex = 51;
            this.AxPosOkFcwUD.OnChange += new System.EventHandler(this.AxPosOkFcwUD_OnChange);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.axiSwitchLedX23);
            this.groupBox7.Controls.Add(this.AxMoveFcwLR);
            this.groupBox7.Controls.Add(this.AxTargetFcwLR);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.AxZeroFcwLR);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.AxPosOkFcwLR);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox7.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox7.Location = new System.Drawing.Point(10, 88);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox7.Size = new System.Drawing.Size(470, 60);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "左右移动";
            // 
            // axiSwitchLedX23
            // 
            this.axiSwitchLedX23.Dock = System.Windows.Forms.DockStyle.Right;
            this.axiSwitchLedX23.Location = new System.Drawing.Point(395, 28);
            this.axiSwitchLedX23.Name = "axiSwitchLedX23";
            this.axiSwitchLedX23.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX23.OcxState")));
            this.axiSwitchLedX23.Size = new System.Drawing.Size(65, 22);
            this.axiSwitchLedX23.TabIndex = 57;
            // 
            // AxMoveFcwLR
            // 
            this.AxMoveFcwLR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxMoveFcwLR.Location = new System.Drawing.Point(322, 28);
            this.AxMoveFcwLR.Name = "AxMoveFcwLR";
            this.AxMoveFcwLR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxMoveFcwLR.OcxState")));
            this.AxMoveFcwLR.Size = new System.Drawing.Size(65, 22);
            this.AxMoveFcwLR.TabIndex = 56;
            // 
            // AxTargetFcwLR
            // 
            this.AxTargetFcwLR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxTargetFcwLR.Location = new System.Drawing.Point(262, 28);
            this.AxTargetFcwLR.Name = "AxTargetFcwLR";
            this.AxTargetFcwLR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxTargetFcwLR.OcxState")));
            this.AxTargetFcwLR.Size = new System.Drawing.Size(60, 22);
            this.AxTargetFcwLR.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.SlateGray;
            this.label5.Location = new System.Drawing.Point(157, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 22);
            this.label5.TabIndex = 54;
            this.label5.Text = "目标位置(mm):";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AxZeroFcwLR
            // 
            this.AxZeroFcwLR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxZeroFcwLR.Location = new System.Drawing.Point(92, 28);
            this.AxZeroFcwLR.Name = "AxZeroFcwLR";
            this.AxZeroFcwLR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxZeroFcwLR.OcxState")));
            this.AxZeroFcwLR.Size = new System.Drawing.Size(65, 22);
            this.AxZeroFcwLR.TabIndex = 53;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.SlateGray;
            this.label6.Location = new System.Drawing.Point(32, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 22);
            this.label6.TabIndex = 52;
            this.label6.Text = "到位状态";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPosOkFcwLR
            // 
            this.AxPosOkFcwLR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPosOkFcwLR.Location = new System.Drawing.Point(10, 28);
            this.AxPosOkFcwLR.Name = "AxPosOkFcwLR";
            this.AxPosOkFcwLR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPosOkFcwLR.OcxState")));
            this.AxPosOkFcwLR.Size = new System.Drawing.Size(22, 22);
            this.AxPosOkFcwLR.TabIndex = 51;
            this.AxPosOkFcwLR.OnChange += new System.EventHandler(this.AxPosOkFcwLR_OnChange);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.axiSwitchLedX22);
            this.groupBox6.Controls.Add(this.AxMoveFcwFR);
            this.groupBox6.Controls.Add(this.AxTargetFcwFR);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.AxZeroFcwFR);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.AxPosOkFcwFR);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox6.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox6.Location = new System.Drawing.Point(10, 28);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox6.Size = new System.Drawing.Size(470, 60);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "前后移动";
            // 
            // axiSwitchLedX22
            // 
            this.axiSwitchLedX22.Dock = System.Windows.Forms.DockStyle.Right;
            this.axiSwitchLedX22.Location = new System.Drawing.Point(395, 28);
            this.axiSwitchLedX22.Name = "axiSwitchLedX22";
            this.axiSwitchLedX22.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX22.OcxState")));
            this.axiSwitchLedX22.Size = new System.Drawing.Size(65, 22);
            this.axiSwitchLedX22.TabIndex = 51;
            // 
            // AxMoveFcwFR
            // 
            this.AxMoveFcwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxMoveFcwFR.Location = new System.Drawing.Point(322, 28);
            this.AxMoveFcwFR.Name = "AxMoveFcwFR";
            this.AxMoveFcwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxMoveFcwFR.OcxState")));
            this.AxMoveFcwFR.Size = new System.Drawing.Size(65, 22);
            this.AxMoveFcwFR.TabIndex = 50;
            // 
            // AxTargetFcwFR
            // 
            this.AxTargetFcwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxTargetFcwFR.Location = new System.Drawing.Point(262, 28);
            this.AxTargetFcwFR.Name = "AxTargetFcwFR";
            this.AxTargetFcwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxTargetFcwFR.OcxState")));
            this.AxTargetFcwFR.Size = new System.Drawing.Size(60, 22);
            this.AxTargetFcwFR.TabIndex = 49;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.SlateGray;
            this.label4.Location = new System.Drawing.Point(157, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 22);
            this.label4.TabIndex = 48;
            this.label4.Text = "目标位置(mm):";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AxZeroFcwFR
            // 
            this.AxZeroFcwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxZeroFcwFR.Location = new System.Drawing.Point(92, 28);
            this.AxZeroFcwFR.Name = "AxZeroFcwFR";
            this.AxZeroFcwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxZeroFcwFR.OcxState")));
            this.AxZeroFcwFR.Size = new System.Drawing.Size(65, 22);
            this.AxZeroFcwFR.TabIndex = 47;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.SlateGray;
            this.label3.Location = new System.Drawing.Point(32, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 22);
            this.label3.TabIndex = 45;
            this.label3.Text = "到位状态";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPosOkFcwFR
            // 
            this.AxPosOkFcwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPosOkFcwFR.Location = new System.Drawing.Point(10, 28);
            this.AxPosOkFcwFR.Name = "AxPosOkFcwFR";
            this.AxPosOkFcwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPosOkFcwFR.OcxState")));
            this.AxPosOkFcwFR.Size = new System.Drawing.Size(22, 22);
            this.AxPosOkFcwFR.TabIndex = 44;
            this.AxPosOkFcwFR.OnChange += new System.EventHandler(this.AxPosOkFcwFR_OnChange);
            // 
            // GrpLdw
            // 
            this.GrpLdw.Controls.Add(this.groupBox10);
            this.GrpLdw.Controls.Add(this.groupBox12);
            this.GrpLdw.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.GrpLdw.ForeColor = System.Drawing.Color.SteelBlue;
            this.GrpLdw.Location = new System.Drawing.Point(530, 341);
            this.GrpLdw.Margin = new System.Windows.Forms.Padding(0);
            this.GrpLdw.Name = "GrpLdw";
            this.GrpLdw.Padding = new System.Windows.Forms.Padding(10);
            this.GrpLdw.Size = new System.Drawing.Size(490, 220);
            this.GrpLdw.TabIndex = 6;
            this.GrpLdw.TabStop = false;
            this.GrpLdw.Text = "LDW (摄像头标定)";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.axiSwitchLedX25);
            this.groupBox10.Controls.Add(this.AxMoveLdwUD);
            this.groupBox10.Controls.Add(this.AxTargetLdwUD);
            this.groupBox10.Controls.Add(this.label15);
            this.groupBox10.Controls.Add(this.AxZeroLdwUD);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Controls.Add(this.AxPosOkLdwUD);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox10.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox10.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox10.Location = new System.Drawing.Point(10, 88);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox10.Size = new System.Drawing.Size(470, 60);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "上下移动";
            // 
            // axiSwitchLedX25
            // 
            this.axiSwitchLedX25.Dock = System.Windows.Forms.DockStyle.Right;
            this.axiSwitchLedX25.Location = new System.Drawing.Point(395, 28);
            this.axiSwitchLedX25.Name = "axiSwitchLedX25";
            this.axiSwitchLedX25.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX25.OcxState")));
            this.axiSwitchLedX25.Size = new System.Drawing.Size(65, 22);
            this.axiSwitchLedX25.TabIndex = 57;
            // 
            // AxMoveLdwUD
            // 
            this.AxMoveLdwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxMoveLdwUD.Location = new System.Drawing.Point(322, 28);
            this.AxMoveLdwUD.Name = "AxMoveLdwUD";
            this.AxMoveLdwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxMoveLdwUD.OcxState")));
            this.AxMoveLdwUD.Size = new System.Drawing.Size(65, 22);
            this.AxMoveLdwUD.TabIndex = 56;
            // 
            // AxTargetLdwUD
            // 
            this.AxTargetLdwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxTargetLdwUD.Location = new System.Drawing.Point(262, 28);
            this.AxTargetLdwUD.Name = "AxTargetLdwUD";
            this.AxTargetLdwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxTargetLdwUD.OcxState")));
            this.AxTargetLdwUD.Size = new System.Drawing.Size(60, 22);
            this.AxTargetLdwUD.TabIndex = 55;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.SlateGray;
            this.label15.Location = new System.Drawing.Point(157, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 22);
            this.label15.TabIndex = 54;
            this.label15.Text = "目标位置(mm):";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AxZeroLdwUD
            // 
            this.AxZeroLdwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxZeroLdwUD.Location = new System.Drawing.Point(92, 28);
            this.AxZeroLdwUD.Name = "AxZeroLdwUD";
            this.AxZeroLdwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxZeroLdwUD.OcxState")));
            this.AxZeroLdwUD.Size = new System.Drawing.Size(65, 22);
            this.AxZeroLdwUD.TabIndex = 53;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Left;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.SlateGray;
            this.label16.Location = new System.Drawing.Point(32, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 22);
            this.label16.TabIndex = 52;
            this.label16.Text = "到位状态";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPosOkLdwUD
            // 
            this.AxPosOkLdwUD.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPosOkLdwUD.Location = new System.Drawing.Point(10, 28);
            this.AxPosOkLdwUD.Name = "AxPosOkLdwUD";
            this.AxPosOkLdwUD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPosOkLdwUD.OcxState")));
            this.AxPosOkLdwUD.Size = new System.Drawing.Size(22, 22);
            this.AxPosOkLdwUD.TabIndex = 51;
            this.AxPosOkLdwUD.OnChange += new System.EventHandler(this.AxPosOkLdwUD_OnChange);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.axiSwitchLedX26);
            this.groupBox12.Controls.Add(this.AxMoveLdwFR);
            this.groupBox12.Controls.Add(this.AxTargetLdwFR);
            this.groupBox12.Controls.Add(this.label13);
            this.groupBox12.Controls.Add(this.AxZeroLdwFR);
            this.groupBox12.Controls.Add(this.label14);
            this.groupBox12.Controls.Add(this.AxPosOkLdwFR);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox12.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox12.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox12.Location = new System.Drawing.Point(10, 28);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox12.Size = new System.Drawing.Size(470, 60);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "前后移动";
            // 
            // axiSwitchLedX26
            // 
            this.axiSwitchLedX26.Dock = System.Windows.Forms.DockStyle.Right;
            this.axiSwitchLedX26.Location = new System.Drawing.Point(395, 28);
            this.axiSwitchLedX26.Name = "axiSwitchLedX26";
            this.axiSwitchLedX26.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSwitchLedX26.OcxState")));
            this.axiSwitchLedX26.Size = new System.Drawing.Size(65, 22);
            this.axiSwitchLedX26.TabIndex = 57;
            // 
            // AxMoveLdwFR
            // 
            this.AxMoveLdwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxMoveLdwFR.Location = new System.Drawing.Point(322, 28);
            this.AxMoveLdwFR.Name = "AxMoveLdwFR";
            this.AxMoveLdwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxMoveLdwFR.OcxState")));
            this.AxMoveLdwFR.Size = new System.Drawing.Size(65, 22);
            this.AxMoveLdwFR.TabIndex = 56;
            // 
            // AxTargetLdwFR
            // 
            this.AxTargetLdwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxTargetLdwFR.Location = new System.Drawing.Point(262, 28);
            this.AxTargetLdwFR.Name = "AxTargetLdwFR";
            this.AxTargetLdwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxTargetLdwFR.OcxState")));
            this.AxTargetLdwFR.Size = new System.Drawing.Size(60, 22);
            this.AxTargetLdwFR.TabIndex = 55;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.SlateGray;
            this.label13.Location = new System.Drawing.Point(157, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 22);
            this.label13.TabIndex = 54;
            this.label13.Text = "目标位置(mm):";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AxZeroLdwFR
            // 
            this.AxZeroLdwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxZeroLdwFR.Location = new System.Drawing.Point(92, 28);
            this.AxZeroLdwFR.Name = "AxZeroLdwFR";
            this.AxZeroLdwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxZeroLdwFR.OcxState")));
            this.AxZeroLdwFR.Size = new System.Drawing.Size(65, 22);
            this.AxZeroLdwFR.TabIndex = 53;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.SlateGray;
            this.label14.Location = new System.Drawing.Point(32, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 22);
            this.label14.TabIndex = 52;
            this.label14.Text = "到位状态";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPosOkLdwFR
            // 
            this.AxPosOkLdwFR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPosOkLdwFR.Location = new System.Drawing.Point(10, 28);
            this.AxPosOkLdwFR.Name = "AxPosOkLdwFR";
            this.AxPosOkLdwFR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPosOkLdwFR.OcxState")));
            this.AxPosOkLdwFR.Size = new System.Drawing.Size(22, 22);
            this.AxPosOkLdwFR.TabIndex = 51;
            this.AxPosOkLdwFR.OnChange += new System.EventHandler(this.AxPosOkLdwFR_OnChange);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.AsObdWireless);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.AsObdWired);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.AsScaner);
            this.groupBox5.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.groupBox5.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox5.Location = new System.Drawing.Point(1040, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(250, 465);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "串口连接";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.Color.SteelBlue;
            this.label23.Location = new System.Drawing.Point(60, 140);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(107, 30);
            this.label23.TabIndex = 38;
            this.label23.Text = "OBD无线";
            // 
            // AsObdWireless
            // 
            this.AsObdWireless.Location = new System.Drawing.Point(20, 140);
            this.AsObdWireless.Name = "AsObdWireless";
            this.AsObdWireless.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsObdWireless.OcxState")));
            this.AsObdWireless.Size = new System.Drawing.Size(30, 30);
            this.AsObdWireless.TabIndex = 37;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.ForeColor = System.Drawing.Color.SteelBlue;
            this.label24.Location = new System.Drawing.Point(60, 90);
            this.label24.Margin = new System.Windows.Forms.Padding(0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 30);
            this.label24.TabIndex = 36;
            this.label24.Text = "OBD有线";
            // 
            // AsObdWired
            // 
            this.AsObdWired.Location = new System.Drawing.Point(20, 90);
            this.AsObdWired.Name = "AsObdWired";
            this.AsObdWired.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsObdWired.OcxState")));
            this.AsObdWired.Size = new System.Drawing.Size(30, 30);
            this.AsObdWired.TabIndex = 35;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.ForeColor = System.Drawing.Color.SteelBlue;
            this.label25.Location = new System.Drawing.Point(60, 40);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 30);
            this.label25.TabIndex = 34;
            this.label25.Text = "扫码枪";
            // 
            // AsScaner
            // 
            this.AsScaner.Location = new System.Drawing.Point(20, 40);
            this.AsScaner.Name = "AsScaner";
            this.AsScaner.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsScaner.OcxState")));
            this.AsScaner.Size = new System.Drawing.Size(30, 30);
            this.AsScaner.TabIndex = 33;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.groupBox11);
            this.groupBox9.Controls.Add(this.groupBox13);
            this.groupBox9.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox9.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox9.Location = new System.Drawing.Point(20, 100);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox9.Size = new System.Drawing.Size(490, 220);
            this.groupBox9.TabIndex = 9;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "摆正器";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.AxPusherRunR);
            this.groupBox11.Controls.Add(this.label21);
            this.groupBox11.Controls.Add(this.AxPusherInR);
            this.groupBox11.Controls.Add(this.AxPusherOutR);
            this.groupBox11.Controls.Add(this.AxPusherForcePercentR);
            this.groupBox11.Controls.Add(this.label26);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox11.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox11.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox11.Location = new System.Drawing.Point(10, 88);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox11.Size = new System.Drawing.Size(470, 60);
            this.groupBox11.TabIndex = 9;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "后轴";
            // 
            // AxPusherRunR
            // 
            this.AxPusherRunR.Dock = System.Windows.Forms.DockStyle.Right;
            this.AxPusherRunR.Location = new System.Drawing.Point(393, 28);
            this.AxPusherRunR.Name = "AxPusherRunR";
            this.AxPusherRunR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherRunR.OcxState")));
            this.AxPusherRunR.Size = new System.Drawing.Size(22, 22);
            this.AxPusherRunR.TabIndex = 59;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Right;
            this.label21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.SlateGray;
            this.label21.Location = new System.Drawing.Point(415, 28);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 22);
            this.label21.TabIndex = 58;
            this.label21.Text = "后变频";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPusherInR
            // 
            this.AxPusherInR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPusherInR.Location = new System.Drawing.Point(220, 28);
            this.AxPusherInR.Name = "AxPusherInR";
            this.AxPusherInR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherInR.OcxState")));
            this.AxPusherInR.Size = new System.Drawing.Size(55, 22);
            this.AxPusherInR.TabIndex = 57;
            // 
            // AxPusherOutR
            // 
            this.AxPusherOutR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPusherOutR.Location = new System.Drawing.Point(165, 28);
            this.AxPusherOutR.Name = "AxPusherOutR";
            this.AxPusherOutR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherOutR.OcxState")));
            this.AxPusherOutR.Size = new System.Drawing.Size(55, 22);
            this.AxPusherOutR.TabIndex = 56;
            // 
            // AxPusherForcePercentR
            // 
            this.AxPusherForcePercentR.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPusherForcePercentR.Location = new System.Drawing.Point(115, 28);
            this.AxPusherForcePercentR.Name = "AxPusherForcePercentR";
            this.AxPusherForcePercentR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherForcePercentR.OcxState")));
            this.AxPusherForcePercentR.Size = new System.Drawing.Size(50, 22);
            this.AxPusherForcePercentR.TabIndex = 55;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Left;
            this.label26.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.SlateGray;
            this.label26.Location = new System.Drawing.Point(10, 28);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(105, 22);
            this.label26.TabIndex = 54;
            this.label26.Text = "电机推力限值(%):";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.AxPusherRunF);
            this.groupBox13.Controls.Add(this.label22);
            this.groupBox13.Controls.Add(this.AxPusherInF);
            this.groupBox13.Controls.Add(this.AxPusherOutF);
            this.groupBox13.Controls.Add(this.AxPusherForcePercentF);
            this.groupBox13.Controls.Add(this.label28);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox13.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox13.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox13.Location = new System.Drawing.Point(10, 28);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox13.Size = new System.Drawing.Size(470, 60);
            this.groupBox13.TabIndex = 7;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "前轴";
            // 
            // AxPusherRunF
            // 
            this.AxPusherRunF.Dock = System.Windows.Forms.DockStyle.Right;
            this.AxPusherRunF.Location = new System.Drawing.Point(393, 28);
            this.AxPusherRunF.Name = "AxPusherRunF";
            this.AxPusherRunF.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherRunF.OcxState")));
            this.AxPusherRunF.Size = new System.Drawing.Size(22, 22);
            this.AxPusherRunF.TabIndex = 59;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Right;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.SlateGray;
            this.label22.Location = new System.Drawing.Point(415, 28);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(45, 22);
            this.label22.TabIndex = 58;
            this.label22.Text = "前变频";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AxPusherInF
            // 
            this.AxPusherInF.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPusherInF.Location = new System.Drawing.Point(220, 28);
            this.AxPusherInF.Name = "AxPusherInF";
            this.AxPusherInF.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherInF.OcxState")));
            this.AxPusherInF.Size = new System.Drawing.Size(55, 22);
            this.AxPusherInF.TabIndex = 57;
            // 
            // AxPusherOutF
            // 
            this.AxPusherOutF.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPusherOutF.Location = new System.Drawing.Point(165, 28);
            this.AxPusherOutF.Name = "AxPusherOutF";
            this.AxPusherOutF.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherOutF.OcxState")));
            this.AxPusherOutF.Size = new System.Drawing.Size(55, 22);
            this.AxPusherOutF.TabIndex = 56;
            // 
            // AxPusherForcePercentF
            // 
            this.AxPusherForcePercentF.Dock = System.Windows.Forms.DockStyle.Left;
            this.AxPusherForcePercentF.Location = new System.Drawing.Point(115, 28);
            this.AxPusherForcePercentF.Name = "AxPusherForcePercentF";
            this.AxPusherForcePercentF.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxPusherForcePercentF.OcxState")));
            this.AxPusherForcePercentF.Size = new System.Drawing.Size(50, 22);
            this.AxPusherForcePercentF.TabIndex = 55;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.Color.SlateGray;
            this.label28.Location = new System.Drawing.Point(10, 28);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(105, 22);
            this.label28.TabIndex = 54;
            this.label28.Text = "电机推力限值(%):";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1350, 611);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.GrpLdw);
            this.Controls.Add(this.GrpFcw);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmAsset";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "FrmAsset";
            this.Load += new System.EventHandler(this.FrmAsset_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRoundX4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiLedRectangleX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxManual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPowerOn)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherPosOk)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX9)).EndInit();
            this.GrpFcw.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveFcwUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetFcwUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroFcwUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkFcwUD)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveFcwLR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetFcwLR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroFcwLR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkFcwLR)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveFcwFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetFcwFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroFcwFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkFcwFR)).EndInit();
            this.GrpLdw.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveLdwUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetLdwUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroLdwUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkLdwUD)).EndInit();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axiSwitchLedX26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMoveLdwFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxTargetLdwFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxZeroLdwFR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPosOkLdwFR)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AsObdWireless)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsObdWired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsScaner)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherRunR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherInR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherOutR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherForcePercentR)).EndInit();
            this.groupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherRunF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherInF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherOutF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxPusherForcePercentF)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox GrpFcw;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox GrpLdw;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label20;
        private AxisDigitalLibrary.AxiLedRoundX AxManual;
        private System.Windows.Forms.Label label19;
        private AxisDigitalLibrary.AxiLedRoundX AxAuto;
        private System.Windows.Forms.Label label18;
        private AxisDigitalLibrary.AxiLedRoundX AxPowerOn;
        private AxisDigitalLibrary.AxiLedRoundX axiLedRoundX4;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX1;
        private AxisDigitalLibrary.AxiLedRectangleX axiLedRectangleX1;
        private System.Windows.Forms.Label label17;
        private AxisDigitalLibrary.AxiLedRoundX AxPusherPosOk;
        private System.Windows.Forms.Label label1;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX9;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX4;
        private AxisDigitalLibrary.AxiLedRoundX axiLedRoundX7;
        private System.Windows.Forms.Label label8;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX3;
        private AxisDigitalLibrary.AxiLedRoundX axiLedRoundX6;
        private System.Windows.Forms.Label label7;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX2;
        private AxisDigitalLibrary.AxiLedRoundX axiLedRoundX5;
        private System.Windows.Forms.Label label2;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX11;
        private System.Windows.Forms.Label label12;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX10;
        private System.Windows.Forms.Label label11;
        private AxisDigitalLibrary.AxiSwitchLedX AxZeroFcwFR;
        private System.Windows.Forms.Label label3;
        private AxisDigitalLibrary.AxiLedRoundX AxPosOkFcwFR;
        private AxisDigitalLibrary.AxiIntegerOutputX AxTargetFcwFR;
        private System.Windows.Forms.Label label4;
        private AxisDigitalLibrary.AxiSwitchLedX AxMoveFcwFR;
        private AxisDigitalLibrary.AxiSwitchLedX AxMoveFcwUD;
        private AxisDigitalLibrary.AxiIntegerOutputX AxTargetFcwUD;
        private System.Windows.Forms.Label label9;
        private AxisDigitalLibrary.AxiSwitchLedX AxZeroFcwUD;
        private System.Windows.Forms.Label label10;
        private AxisDigitalLibrary.AxiLedRoundX AxPosOkFcwUD;
        private AxisDigitalLibrary.AxiSwitchLedX AxMoveFcwLR;
        private AxisDigitalLibrary.AxiIntegerOutputX AxTargetFcwLR;
        private System.Windows.Forms.Label label5;
        private AxisDigitalLibrary.AxiSwitchLedX AxZeroFcwLR;
        private System.Windows.Forms.Label label6;
        private AxisDigitalLibrary.AxiLedRoundX AxPosOkFcwLR;
        private AxisDigitalLibrary.AxiSwitchLedX AxMoveLdwUD;
        private AxisDigitalLibrary.AxiIntegerOutputX AxTargetLdwUD;
        private System.Windows.Forms.Label label15;
        private AxisDigitalLibrary.AxiSwitchLedX AxZeroLdwUD;
        private System.Windows.Forms.Label label16;
        private AxisDigitalLibrary.AxiLedRoundX AxPosOkLdwUD;
        private AxisDigitalLibrary.AxiSwitchLedX AxMoveLdwFR;
        private AxisDigitalLibrary.AxiIntegerOutputX AxTargetLdwFR;
        private System.Windows.Forms.Label label13;
        private AxisDigitalLibrary.AxiSwitchLedX AxZeroLdwFR;
        private System.Windows.Forms.Label label14;
        private AxisDigitalLibrary.AxiLedRoundX AxPosOkLdwFR;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX22;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX24;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX23;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX25;
        private AxisDigitalLibrary.AxiSwitchLedX axiSwitchLedX26;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label23;
        private AxisDigitalLibrary.AxiLedRectangleX AsObdWireless;
        private System.Windows.Forms.Label label24;
        private AxisDigitalLibrary.AxiLedRectangleX AsObdWired;
        private System.Windows.Forms.Label label25;
        private AxisDigitalLibrary.AxiLedRectangleX AsScaner;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox11;
        private AxisDigitalLibrary.AxiIntegerOutputX AxPusherForcePercentR;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox13;
        private AxisDigitalLibrary.AxiIntegerOutputX AxPusherForcePercentF;
        private System.Windows.Forms.Label label28;
        private AxisDigitalLibrary.AxiLedRoundX AxPusherRunR;
        private System.Windows.Forms.Label label21;
        private AxisDigitalLibrary.AxiSwitchLedX AxPusherInR;
        private AxisDigitalLibrary.AxiSwitchLedX AxPusherOutR;
        private AxisDigitalLibrary.AxiLedRoundX AxPusherRunF;
        private System.Windows.Forms.Label label22;
        private AxisDigitalLibrary.AxiSwitchLedX AxPusherInF;
        private AxisDigitalLibrary.AxiSwitchLedX AxPusherOutF;


    }
}