﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using ADAS.Data;
using FastReport;
using System.IO;

namespace ADAS
{
    public partial class FrmQuery : Form
    {
        public FrmQuery()
        {
            InitializeComponent();
        }

        private void FrmQuery_Load(object sender, EventArgs e)
        {
            GrdX.AutoGenerateColumns = false;
            BtnQueryDate_Click(BtnFindTime, null);
        }

        private void BtnQueryDate_Click(object sender, EventArgs e)
        {
            using (BackgroundWorker worker = new BackgroundWorker())
            {
                GrdX.SelectionChanged -= new EventHandler(GrdX_SelectionChanged);
                BtnFindTime.Enabled = false;
                BtnFindVin.Enabled = false;
                DateTime dateS = DpkS.Value.Date, dateE = DpkE.Value.Date;
                List<VehiTask> list = new List<VehiTask>();
                worker.DoWork += (xs, xe) =>
                {
                    list = VehiTask.QueryByDate(dateS, dateE);
                };
                worker.RunWorkerCompleted += (xs, xe) =>
                {
                    GrdX.DataSource = list;
                    BtnFindTime.Enabled = true;
                    BtnFindVin.Enabled = true;
                    GrdX.SelectionChanged += new EventHandler(GrdX_SelectionChanged);
                    GrdX_SelectionChanged(GrdX, new EventArgs());
                };
                worker.RunWorkerAsync();
            }
        }

        private void BtnQueryVin_Click(object sender, EventArgs e)
        {
            using (BackgroundWorker worker = new BackgroundWorker())
            {
                GrdX.SelectionChanged -= new EventHandler(GrdX_SelectionChanged);

                BtnFindTime.Enabled = false;
                BtnFindVin.Enabled = false;
                string vin = TbxVin.Text.Trim();
                List<VehiTask> list = new List<VehiTask>();
                worker.DoWork += (xs, xe) =>
                {
                    list = VehiTask.QueryByVin(vin);
                };
                worker.RunWorkerCompleted += (xs, xe) =>
                {
                    GrdX.DataSource = list;
                    BtnFindTime.Enabled = true;
                    BtnFindVin.Enabled = true;
                    GrdX.SelectionChanged += new EventHandler(GrdX_SelectionChanged);
                    GrdX_SelectionChanged(GrdX, new EventArgs());
                };
                worker.RunWorkerAsync();
            }
        }

        public void QueryToday()
        {
            DpkS.Value = DpkE.Value = DateTime.Now.Date;
            BtnQueryDate_Click(BtnFindTime, new EventArgs());
        }

        #region Report
        private Report report = null;
        private void LoadReport(VehiTask task)
        {
            report = new Report();
            report.Load(Path.Combine(Application.StartupPath, "Report", "ReportPrint.frx"));
            report.Preview = ReportView;

            if (task != null)
            {
                report.SetText("TxtVinCode", task.VinCode);
                report.SetText("TxtModel", task.ModelCode);
                report.SetText("TxtTimeS", string.Format("{0:yyyy-MM-dd HH:mm:ss}", task.TimeS));
                report.SetText("TxtTimeE", string.Format("{0:yyyy-MM-dd HH:mm:ss}", task.TimeE));
                report.SetText("TxtFcwResult", (!task.CaliFcwDone ? "未进行" : (task.CaliFcwPass ? "标定成功" : "标定失败")));
                report.SetText("TxtFcwDesc", task.CaliFcwError);
                report.SetText("TxtLdwResult", (!task.CaliLdwDone ? "未进行" : (task.CaliLdwPass ? "标定成功" : "标定失败")));
                report.SetText("TxtLdwDesc", task.CaliLdwError);
                report.SetText("TxtTimePrint", string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            }
            try
            {
                ReportView.Update();
                report.Prepare();
                report.ShowPrepared();
            }
            catch { }
        }
        #endregion

        private void GrdX_SelectionChanged(object sender, EventArgs e)
        {
            VehiTask task = null;
            if (GrdX != null && GrdX.SelectedRows != null && GrdX.SelectedRows.Count > 0)
            {
                task = GrdX.SelectedRows[0].DataBoundItem as VehiTask;
            }
            if (task == null) { task = new VehiTask(); }
            LoadReport(task);
        }
    }
}
