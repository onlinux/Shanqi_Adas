﻿namespace ADAS
{
    partial class FrmLogger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxLogger1 = new DXP.Win.UxLogger();
            this.SuspendLayout();
            // 
            // uxLogger1
            // 
            this.uxLogger1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxLogger1.Location = new System.Drawing.Point(0, 0);
            this.uxLogger1.Margin = new System.Windows.Forms.Padding(0);
            this.uxLogger1.Name = "uxLogger1";
            this.uxLogger1.Size = new System.Drawing.Size(1350, 611);
            this.uxLogger1.TabIndex = 0;
            // 
            // FrmLogger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 611);
            this.Controls.Add(this.uxLogger1);
            this.Name = "FrmLogger";
            this.Text = "FrmLogger";
            this.ResumeLayout(false);

        }

        #endregion

        private DXP.Win.UxLogger uxLogger1;
    }
}