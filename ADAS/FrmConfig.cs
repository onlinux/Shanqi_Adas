﻿using System;
using System.Windows.Forms;
using ADAS.Appx;

namespace ADAS
{
    public partial class FrmConfig : Form
    {
        public FrmConfig()
        {
            InitializeComponent();
        }

        private void FrmConfig_Load(object sender, EventArgs e)
        {
            GrdX.SelectedObject = Setting.Current;
        }

    }
}
