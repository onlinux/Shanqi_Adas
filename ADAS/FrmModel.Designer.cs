﻿namespace ADAS
{
    partial class FrmModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TabX = new System.Windows.Forms.TabControl();
            this.PageModel = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.NbxModelID = new System.Windows.Forms.NumericUpDown();
            this.BtnModelSave = new System.Windows.Forms.Button();
            this.BtnModelDel = new System.Windows.Forms.Button();
            this.NbxCameraRoll = new System.Windows.Forms.NumericUpDown();
            this.NbxCameraYaw = new System.Windows.Forms.NumericUpDown();
            this.NbxCameraPitch = new System.Windows.Forms.NumericUpDown();
            this.NbxCameraNearMiddle = new System.Windows.Forms.NumericUpDown();
            this.NbxCameraNearHead = new System.Windows.Forms.NumericUpDown();
            this.NbxCameraHeight = new System.Windows.Forms.NumericUpDown();
            this.NbxVehiWidth = new System.Windows.Forms.NumericUpDown();
            this.TbxModelCode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NbxVehiHeight = new System.Windows.Forms.NumericUpDown();
            this.BtnModelNew = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.NbxRadarHeight = new System.Windows.Forms.NumericUpDown();
            this.NbxRadarNearHead = new System.Windows.Forms.NumericUpDown();
            this.NbxRadarNearMiddle = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.NbxPusherForcePercentF = new System.Windows.Forms.NumericUpDown();
            this.NbxPusherForcePercentR = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.CbxProtocol = new DXP.Win.UxCombo();
            this.CbxSecurity = new DXP.Win.UxCombo();
            this.PageRule = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.GrdRule = new System.Windows.Forms.DataGridView();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NbxRuleID = new System.Windows.Forms.NumericUpDown();
            this.BtnRuleSave = new System.Windows.Forms.Button();
            this.BtnRuleDel = new System.Windows.Forms.Button();
            this.TbxVinMap = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.BtnRileNew = new System.Windows.Forms.Button();
            this.GrdModel = new System.Windows.Forms.DataGridView();
            this.ColQR = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabX.SuspendLayout();
            this.PageModel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NbxModelID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraRoll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraYaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraPitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraNearMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraNearHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxVehiWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxVehiHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRadarHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRadarNearHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRadarNearMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxPusherForcePercentF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxPusherForcePercentR)).BeginInit();
            this.PageRule.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdRule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRuleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdModel)).BeginInit();
            this.SuspendLayout();
            // 
            // TabX
            // 
            this.TabX.Controls.Add(this.PageModel);
            this.TabX.Controls.Add(this.PageRule);
            this.TabX.Dock = System.Windows.Forms.DockStyle.Right;
            this.TabX.Location = new System.Drawing.Point(1033, 0);
            this.TabX.Name = "TabX";
            this.TabX.SelectedIndex = 0;
            this.TabX.Size = new System.Drawing.Size(317, 611);
            this.TabX.TabIndex = 3;
            // 
            // PageModel
            // 
            this.PageModel.Controls.Add(this.tableLayoutPanel1);
            this.PageModel.Location = new System.Drawing.Point(4, 22);
            this.PageModel.Name = "PageModel";
            this.PageModel.Padding = new System.Windows.Forms.Padding(5);
            this.PageModel.Size = new System.Drawing.Size(309, 585);
            this.PageModel.TabIndex = 0;
            this.PageModel.Text = "车型详情";
            this.PageModel.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.NbxModelID, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.BtnModelSave, 2, 22);
            this.tableLayoutPanel1.Controls.Add(this.BtnModelDel, 0, 22);
            this.tableLayoutPanel1.Controls.Add(this.NbxCameraRoll, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.NbxCameraYaw, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.NbxCameraPitch, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.NbxCameraNearMiddle, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.NbxCameraNearHead, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.NbxCameraHeight, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.NbxVehiWidth, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.TbxModelCode, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.NbxVehiHeight, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.BtnModelNew, 1, 22);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.NbxRadarHeight, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.NbxRadarNearHead, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.NbxRadarNearMiddle, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.NbxPusherForcePercentF, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.NbxPusherForcePercentR, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 20);
            this.tableLayoutPanel1.Controls.Add(this.CbxProtocol, 1, 19);
            this.tableLayoutPanel1.Controls.Add(this.CbxSecurity, 1, 20);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 23;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(299, 575);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // NbxModelID
            // 
            this.NbxModelID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxModelID, 2);
            this.NbxModelID.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NbxModelID.InterceptArrowKeys = false;
            this.NbxModelID.Location = new System.Drawing.Point(135, 2);
            this.NbxModelID.Margin = new System.Windows.Forms.Padding(0);
            this.NbxModelID.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NbxModelID.Name = "NbxModelID";
            this.NbxModelID.ReadOnly = true;
            this.NbxModelID.Size = new System.Drawing.Size(164, 21);
            this.NbxModelID.TabIndex = 28;
            // 
            // BtnModelSave
            // 
            this.BtnModelSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnModelSave.Enabled = false;
            this.BtnModelSave.Location = new System.Drawing.Point(220, 548);
            this.BtnModelSave.Name = "BtnModelSave";
            this.BtnModelSave.Size = new System.Drawing.Size(76, 24);
            this.BtnModelSave.TabIndex = 27;
            this.BtnModelSave.Text = "保存车型";
            this.BtnModelSave.UseVisualStyleBackColor = true;
            this.BtnModelSave.Click += new System.EventHandler(this.BtnModelSave_Click);
            // 
            // BtnModelDel
            // 
            this.BtnModelDel.BackColor = System.Drawing.Color.PeachPuff;
            this.BtnModelDel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnModelDel.Enabled = false;
            this.BtnModelDel.Location = new System.Drawing.Point(3, 548);
            this.BtnModelDel.Name = "BtnModelDel";
            this.BtnModelDel.Size = new System.Drawing.Size(129, 24);
            this.BtnModelDel.TabIndex = 26;
            this.BtnModelDel.Text = "删除车型";
            this.BtnModelDel.UseVisualStyleBackColor = false;
            this.BtnModelDel.Click += new System.EventHandler(this.BtnModelDel_Click);
            // 
            // NbxCameraRoll
            // 
            this.NbxCameraRoll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxCameraRoll, 2);
            this.NbxCameraRoll.Location = new System.Drawing.Point(135, 237);
            this.NbxCameraRoll.Margin = new System.Windows.Forms.Padding(0);
            this.NbxCameraRoll.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.NbxCameraRoll.Name = "NbxCameraRoll";
            this.NbxCameraRoll.Size = new System.Drawing.Size(164, 21);
            this.NbxCameraRoll.TabIndex = 24;
            // 
            // NbxCameraYaw
            // 
            this.NbxCameraYaw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxCameraYaw, 2);
            this.NbxCameraYaw.Location = new System.Drawing.Point(135, 212);
            this.NbxCameraYaw.Margin = new System.Windows.Forms.Padding(0);
            this.NbxCameraYaw.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.NbxCameraYaw.Name = "NbxCameraYaw";
            this.NbxCameraYaw.Size = new System.Drawing.Size(164, 21);
            this.NbxCameraYaw.TabIndex = 23;
            // 
            // NbxCameraPitch
            // 
            this.NbxCameraPitch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxCameraPitch, 2);
            this.NbxCameraPitch.Location = new System.Drawing.Point(135, 187);
            this.NbxCameraPitch.Margin = new System.Windows.Forms.Padding(0);
            this.NbxCameraPitch.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.NbxCameraPitch.Name = "NbxCameraPitch";
            this.NbxCameraPitch.Size = new System.Drawing.Size(164, 21);
            this.NbxCameraPitch.TabIndex = 22;
            // 
            // NbxCameraNearMiddle
            // 
            this.NbxCameraNearMiddle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxCameraNearMiddle, 2);
            this.NbxCameraNearMiddle.Location = new System.Drawing.Point(135, 162);
            this.NbxCameraNearMiddle.Margin = new System.Windows.Forms.Padding(0);
            this.NbxCameraNearMiddle.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxCameraNearMiddle.Name = "NbxCameraNearMiddle";
            this.NbxCameraNearMiddle.Size = new System.Drawing.Size(164, 21);
            this.NbxCameraNearMiddle.TabIndex = 21;
            // 
            // NbxCameraNearHead
            // 
            this.NbxCameraNearHead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxCameraNearHead, 2);
            this.NbxCameraNearHead.Location = new System.Drawing.Point(135, 137);
            this.NbxCameraNearHead.Margin = new System.Windows.Forms.Padding(0);
            this.NbxCameraNearHead.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxCameraNearHead.Name = "NbxCameraNearHead";
            this.NbxCameraNearHead.Size = new System.Drawing.Size(164, 21);
            this.NbxCameraNearHead.TabIndex = 20;
            // 
            // NbxCameraHeight
            // 
            this.NbxCameraHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxCameraHeight, 2);
            this.NbxCameraHeight.Location = new System.Drawing.Point(135, 112);
            this.NbxCameraHeight.Margin = new System.Windows.Forms.Padding(0);
            this.NbxCameraHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxCameraHeight.Name = "NbxCameraHeight";
            this.NbxCameraHeight.Size = new System.Drawing.Size(164, 21);
            this.NbxCameraHeight.TabIndex = 19;
            // 
            // NbxVehiWidth
            // 
            this.NbxVehiWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxVehiWidth, 2);
            this.NbxVehiWidth.Location = new System.Drawing.Point(135, 77);
            this.NbxVehiWidth.Margin = new System.Windows.Forms.Padding(0);
            this.NbxVehiWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxVehiWidth.Name = "NbxVehiWidth";
            this.NbxVehiWidth.Size = new System.Drawing.Size(164, 21);
            this.NbxVehiWidth.TabIndex = 18;
            // 
            // TbxModelCode
            // 
            this.TbxModelCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.TbxModelCode, 2);
            this.TbxModelCode.Location = new System.Drawing.Point(135, 27);
            this.TbxModelCode.Margin = new System.Windows.Forms.Padding(0);
            this.TbxModelCode.Name = "TbxModelCode";
            this.TbxModelCode.Size = new System.Drawing.Size(164, 21);
            this.TbxModelCode.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 241);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(129, 12);
            this.label11.TabIndex = 10;
            this.label11.Text = "摄像头Roll(x0.01)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 216);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 12);
            this.label10.TabIndex = 9;
            this.label10.Text = "摄像头Yaw(x0.01)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 191);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "摄像头Pitch(x0.01)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 166);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "摄像头距中线(mm)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "摄像头距前轴(mm)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "摄像头高度(mm)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "车身宽度(mm)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "车身高度(mm)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "车型代码";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "车型ID";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NbxVehiHeight
            // 
            this.NbxVehiHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxVehiHeight, 2);
            this.NbxVehiHeight.Location = new System.Drawing.Point(135, 52);
            this.NbxVehiHeight.Margin = new System.Windows.Forms.Padding(0);
            this.NbxVehiHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxVehiHeight.Name = "NbxVehiHeight";
            this.NbxVehiHeight.Size = new System.Drawing.Size(164, 21);
            this.NbxVehiHeight.TabIndex = 14;
            // 
            // BtnModelNew
            // 
            this.BtnModelNew.BackColor = System.Drawing.Color.PaleGreen;
            this.BtnModelNew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnModelNew.Location = new System.Drawing.Point(138, 548);
            this.BtnModelNew.Name = "BtnModelNew";
            this.BtnModelNew.Size = new System.Drawing.Size(76, 24);
            this.BtnModelNew.TabIndex = 25;
            this.BtnModelNew.Text = "添加车型";
            this.BtnModelNew.UseVisualStyleBackColor = false;
            this.BtnModelNew.Click += new System.EventHandler(this.BtnModelNew_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 276);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "雷达高度(mm)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 326);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 12);
            this.label12.TabIndex = 7;
            this.label12.Text = "雷达距中线(mm)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 301);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 12);
            this.label13.TabIndex = 6;
            this.label13.Text = "雷达距前轴(mm)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NbxRadarHeight
            // 
            this.NbxRadarHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxRadarHeight, 2);
            this.NbxRadarHeight.Location = new System.Drawing.Point(135, 272);
            this.NbxRadarHeight.Margin = new System.Windows.Forms.Padding(0);
            this.NbxRadarHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxRadarHeight.Name = "NbxRadarHeight";
            this.NbxRadarHeight.Size = new System.Drawing.Size(164, 21);
            this.NbxRadarHeight.TabIndex = 19;
            // 
            // NbxRadarNearHead
            // 
            this.NbxRadarNearHead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxRadarNearHead, 2);
            this.NbxRadarNearHead.Location = new System.Drawing.Point(135, 297);
            this.NbxRadarNearHead.Margin = new System.Windows.Forms.Padding(0);
            this.NbxRadarNearHead.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxRadarNearHead.Name = "NbxRadarNearHead";
            this.NbxRadarNearHead.Size = new System.Drawing.Size(164, 21);
            this.NbxRadarNearHead.TabIndex = 20;
            // 
            // NbxRadarNearMiddle
            // 
            this.NbxRadarNearMiddle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxRadarNearMiddle, 2);
            this.NbxRadarNearMiddle.Location = new System.Drawing.Point(135, 322);
            this.NbxRadarNearMiddle.Margin = new System.Windows.Forms.Padding(0);
            this.NbxRadarNearMiddle.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxRadarNearMiddle.Name = "NbxRadarNearMiddle";
            this.NbxRadarNearMiddle.Size = new System.Drawing.Size(164, 21);
            this.NbxRadarNearMiddle.TabIndex = 21;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 361);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(129, 12);
            this.label14.TabIndex = 6;
            this.label14.Text = "前轴推力百分比(%)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 386);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(129, 12);
            this.label15.TabIndex = 6;
            this.label15.Text = "后轴推力百分比(%)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NbxPusherForcePercentF
            // 
            this.NbxPusherForcePercentF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxPusherForcePercentF, 2);
            this.NbxPusherForcePercentF.Location = new System.Drawing.Point(135, 357);
            this.NbxPusherForcePercentF.Margin = new System.Windows.Forms.Padding(0);
            this.NbxPusherForcePercentF.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxPusherForcePercentF.Name = "NbxPusherForcePercentF";
            this.NbxPusherForcePercentF.Size = new System.Drawing.Size(164, 21);
            this.NbxPusherForcePercentF.TabIndex = 21;
            this.NbxPusherForcePercentF.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // NbxPusherForcePercentR
            // 
            this.NbxPusherForcePercentR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.NbxPusherForcePercentR, 2);
            this.NbxPusherForcePercentR.Location = new System.Drawing.Point(135, 382);
            this.NbxPusherForcePercentR.Margin = new System.Windows.Forms.Padding(0);
            this.NbxPusherForcePercentR.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbxPusherForcePercentR.Name = "NbxPusherForcePercentR";
            this.NbxPusherForcePercentR.Size = new System.Drawing.Size(164, 21);
            this.NbxPusherForcePercentR.TabIndex = 21;
            this.NbxPusherForcePercentR.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 421);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 12);
            this.label16.TabIndex = 6;
            this.label16.Text = "CAN线协议";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 446);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 12);
            this.label17.TabIndex = 6;
            this.label17.Text = "安全校验级别";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CbxProtocol
            // 
            this.CbxProtocol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.CbxProtocol, 2);
            this.CbxProtocol.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CbxProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxProtocol.FormattingEnabled = true;
            this.CbxProtocol.Location = new System.Drawing.Point(138, 418);
            this.CbxProtocol.Name = "CbxProtocol";
            this.CbxProtocol.Size = new System.Drawing.Size(158, 22);
            this.CbxProtocol.TabIndex = 29;
            // 
            // CbxSecurity
            // 
            this.CbxSecurity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.CbxSecurity, 2);
            this.CbxSecurity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CbxSecurity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxSecurity.FormattingEnabled = true;
            this.CbxSecurity.Location = new System.Drawing.Point(138, 443);
            this.CbxSecurity.Name = "CbxSecurity";
            this.CbxSecurity.Size = new System.Drawing.Size(158, 22);
            this.CbxSecurity.TabIndex = 29;
            // 
            // PageRule
            // 
            this.PageRule.Controls.Add(this.tableLayoutPanel2);
            this.PageRule.Location = new System.Drawing.Point(4, 22);
            this.PageRule.Name = "PageRule";
            this.PageRule.Padding = new System.Windows.Forms.Padding(5);
            this.PageRule.Size = new System.Drawing.Size(309, 585);
            this.PageRule.TabIndex = 1;
            this.PageRule.Text = "Vin码规则";
            this.PageRule.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.Controls.Add(this.GrdRule, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.NbxRuleID, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.BtnRuleSave, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.BtnRuleDel, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.TbxVinMap, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label20, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.BtnRileNew, 1, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(299, 575);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // GrdRule
            // 
            this.GrdRule.AllowUserToAddRows = false;
            this.GrdRule.AllowUserToDeleteRows = false;
            this.GrdRule.AllowUserToResizeRows = false;
            this.GrdRule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdRule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column15,
            this.Column16});
            this.tableLayoutPanel2.SetColumnSpan(this.GrdRule, 3);
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdRule.DefaultCellStyle = dataGridViewCellStyle1;
            this.GrdRule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdRule.Location = new System.Drawing.Point(3, 3);
            this.GrdRule.MultiSelect = false;
            this.GrdRule.Name = "GrdRule";
            this.GrdRule.ReadOnly = true;
            this.GrdRule.RowHeadersWidth = 20;
            this.GrdRule.RowTemplate.Height = 23;
            this.GrdRule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdRule.Size = new System.Drawing.Size(293, 459);
            this.GrdRule.TabIndex = 29;
            this.GrdRule.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdRule_CellClick);
            this.GrdRule.SelectionChanged += new System.EventHandler(this.GrdRule_SelectionChanged);
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "RuleID";
            this.Column15.HeaderText = "标识";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 60;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column16.DataPropertyName = "VinMap";
            this.Column16.HeaderText = "匹配规则";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // NbxRuleID
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.NbxRuleID, 2);
            this.NbxRuleID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NbxRuleID.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NbxRuleID.InterceptArrowKeys = false;
            this.NbxRuleID.Location = new System.Drawing.Point(102, 478);
            this.NbxRuleID.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NbxRuleID.Name = "NbxRuleID";
            this.NbxRuleID.ReadOnly = true;
            this.NbxRuleID.Size = new System.Drawing.Size(194, 21);
            this.NbxRuleID.TabIndex = 28;
            // 
            // BtnRuleSave
            // 
            this.BtnRuleSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnRuleSave.Enabled = false;
            this.BtnRuleSave.Location = new System.Drawing.Point(201, 548);
            this.BtnRuleSave.Name = "BtnRuleSave";
            this.BtnRuleSave.Size = new System.Drawing.Size(95, 24);
            this.BtnRuleSave.TabIndex = 27;
            this.BtnRuleSave.Text = "保存规则";
            this.BtnRuleSave.UseVisualStyleBackColor = true;
            this.BtnRuleSave.Click += new System.EventHandler(this.BtnRuleSave_Click);
            // 
            // BtnRuleDel
            // 
            this.BtnRuleDel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnRuleDel.Enabled = false;
            this.BtnRuleDel.Location = new System.Drawing.Point(3, 548);
            this.BtnRuleDel.Name = "BtnRuleDel";
            this.BtnRuleDel.Size = new System.Drawing.Size(93, 24);
            this.BtnRuleDel.TabIndex = 26;
            this.BtnRuleDel.Text = "删除规则";
            this.BtnRuleDel.UseVisualStyleBackColor = true;
            this.BtnRuleDel.Click += new System.EventHandler(this.BtnRuleDel_Click);
            // 
            // TbxVinMap
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.TbxVinMap, 2);
            this.TbxVinMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbxVinMap.Location = new System.Drawing.Point(102, 508);
            this.TbxVinMap.Name = "TbxVinMap";
            this.TbxVinMap.Size = new System.Drawing.Size(194, 21);
            this.TbxVinMap.TabIndex = 16;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Location = new System.Drawing.Point(3, 505);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(93, 30);
            this.label19.TabIndex = 1;
            this.label19.Text = "匹配模式：";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Location = new System.Drawing.Point(3, 475);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 30);
            this.label20.TabIndex = 0;
            this.label20.Text = "规则ID：";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnRileNew
            // 
            this.BtnRileNew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnRileNew.Location = new System.Drawing.Point(102, 548);
            this.BtnRileNew.Name = "BtnRileNew";
            this.BtnRileNew.Size = new System.Drawing.Size(93, 24);
            this.BtnRileNew.TabIndex = 25;
            this.BtnRileNew.Text = "添加规则";
            this.BtnRileNew.UseVisualStyleBackColor = true;
            this.BtnRileNew.Click += new System.EventHandler(this.BtnRileNew_Click);
            // 
            // GrdModel
            // 
            this.GrdModel.AllowUserToAddRows = false;
            this.GrdModel.AllowUserToDeleteRows = false;
            this.GrdModel.AllowUserToResizeRows = false;
            this.GrdModel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.GrdModel.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.GrdModel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GrdModel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColQR,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(2);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdModel.DefaultCellStyle = dataGridViewCellStyle3;
            this.GrdModel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdModel.Location = new System.Drawing.Point(0, 0);
            this.GrdModel.MultiSelect = false;
            this.GrdModel.Name = "GrdModel";
            this.GrdModel.ReadOnly = true;
            this.GrdModel.RowHeadersWidth = 20;
            this.GrdModel.RowTemplate.Height = 23;
            this.GrdModel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdModel.Size = new System.Drawing.Size(1033, 611);
            this.GrdModel.TabIndex = 4;
            this.GrdModel.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdModel_CellContentClick);
            this.GrdModel.SelectionChanged += new System.EventHandler(this.GrdModel_SelectionChanged);
            // 
            // ColQR
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "导出";
            this.ColQR.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColQR.Frozen = true;
            this.ColQR.HeaderText = "条形码";
            this.ColQR.Name = "ColQR";
            this.ColQR.ReadOnly = true;
            this.ColQR.Text = "导出";
            this.ColQR.Width = 51;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ModelCode";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "车型代号";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 82;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "VehiHeight";
            this.Column2.HeaderText = "车身高度";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 82;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "VehiWidth";
            this.Column3.HeaderText = "车身宽度";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 82;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "CameraHeight";
            this.Column4.HeaderText = "摄像头高度";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 94;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "CameraNearHead";
            this.Column5.HeaderText = "摄像头距前轴";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 106;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "CameraNearMiddle";
            this.Column6.HeaderText = "摄像头距中线";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 106;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "CameraPitch";
            this.Column7.HeaderText = "摄像头Pitch";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "CameraYaw";
            this.Column8.HeaderText = "摄像头Yaw";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 88;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "CameraRoll";
            this.Column9.HeaderText = "摄像头Roll";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 94;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "RadarHeight";
            this.Column10.HeaderText = "雷达高度";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 82;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "RadarNearHead";
            this.Column11.HeaderText = "雷达距前轴";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 94;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "RadarNearMiddle";
            this.Column12.HeaderText = "雷达距中线";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 94;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "PusherForcePercentF";
            this.Column13.HeaderText = "摆正器前轴推力(%)";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 136;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "PusherForcePercentR";
            this.Column14.HeaderText = "摆正器后轴推力(%)";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 136;
            // 
            // FrmModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 611);
            this.Controls.Add(this.GrdModel);
            this.Controls.Add(this.TabX);
            this.Name = "FrmModel";
            this.Text = "FrmModel";
            this.Load += new System.EventHandler(this.FrmModel_Load);
            this.TabX.ResumeLayout(false);
            this.PageModel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NbxModelID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraRoll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraYaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraPitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraNearMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraNearHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxCameraHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxVehiWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxVehiHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRadarHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRadarNearHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRadarNearMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxPusherForcePercentF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxPusherForcePercentR)).EndInit();
            this.PageRule.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdRule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbxRuleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdModel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabX;
        private System.Windows.Forms.TabPage PageModel;
        private System.Windows.Forms.TabPage PageRule;
        private System.Windows.Forms.DataGridView GrdModel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TbxModelCode;
        private System.Windows.Forms.NumericUpDown NbxVehiHeight;
        private System.Windows.Forms.NumericUpDown NbxCameraRoll;
        private System.Windows.Forms.NumericUpDown NbxCameraYaw;
        private System.Windows.Forms.NumericUpDown NbxCameraPitch;
        private System.Windows.Forms.NumericUpDown NbxCameraNearMiddle;
        private System.Windows.Forms.NumericUpDown NbxCameraNearHead;
        private System.Windows.Forms.NumericUpDown NbxCameraHeight;
        private System.Windows.Forms.NumericUpDown NbxVehiWidth;
        private System.Windows.Forms.Button BtnModelSave;
        private System.Windows.Forms.Button BtnModelDel;
        private System.Windows.Forms.Button BtnModelNew;
        private System.Windows.Forms.NumericUpDown NbxModelID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.NumericUpDown NbxRuleID;
        private System.Windows.Forms.Button BtnRuleSave;
        private System.Windows.Forms.Button BtnRuleDel;
        private System.Windows.Forms.TextBox TbxVinMap;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button BtnRileNew;
        private System.Windows.Forms.DataGridView GrdRule;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown NbxRadarHeight;
        private System.Windows.Forms.NumericUpDown NbxRadarNearHead;
        private System.Windows.Forms.NumericUpDown NbxRadarNearMiddle;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown NbxPusherForcePercentF;
        private System.Windows.Forms.NumericUpDown NbxPusherForcePercentR;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private DXP.Win.UxCombo CbxProtocol;
        private DXP.Win.UxCombo CbxSecurity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewButtonColumn ColQR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
    }
}