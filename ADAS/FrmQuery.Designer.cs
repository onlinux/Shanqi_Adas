﻿namespace ADAS
{
    partial class FrmQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnFindVin = new System.Windows.Forms.Button();
            this.TbxVin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnFindTime = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.DpkE = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.DpkS = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.ReportView = new FastReport.Preview.PreviewControl();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.GrdX = new System.Windows.Forms.DataGridView();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdX)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnFindVin);
            this.panel1.Controls.Add(this.TbxVin);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.BtnFindTime);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.DpkE);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.DpkS);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(1350, 31);
            this.panel1.TabIndex = 0;
            // 
            // BtnFindVin
            // 
            this.BtnFindVin.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnFindVin.Location = new System.Drawing.Point(583, 5);
            this.BtnFindVin.Name = "BtnFindVin";
            this.BtnFindVin.Size = new System.Drawing.Size(55, 21);
            this.BtnFindVin.TabIndex = 8;
            this.BtnFindVin.Text = "查询";
            this.BtnFindVin.UseVisualStyleBackColor = true;
            this.BtnFindVin.Click += new System.EventHandler(this.BtnQueryVin_Click);
            // 
            // TbxVin
            // 
            this.TbxVin.Dock = System.Windows.Forms.DockStyle.Left;
            this.TbxVin.Location = new System.Drawing.Point(456, 5);
            this.TbxVin.Name = "TbxVin";
            this.TbxVin.Size = new System.Drawing.Size(127, 21);
            this.TbxVin.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Location = new System.Drawing.Point(349, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "VIN码:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFindTime
            // 
            this.BtnFindTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnFindTime.Location = new System.Drawing.Point(294, 5);
            this.BtnFindTime.Name = "BtnFindTime";
            this.BtnFindTime.Size = new System.Drawing.Size(55, 21);
            this.BtnFindTime.TabIndex = 5;
            this.BtnFindTime.Text = "查询";
            this.BtnFindTime.UseVisualStyleBackColor = true;
            this.BtnFindTime.Click += new System.EventHandler(this.BtnQueryDate_Click);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Location = new System.Drawing.Point(284, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 21);
            this.label3.TabIndex = 4;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DpkE
            // 
            this.DpkE.CustomFormat = "yyyy-MM-dd";
            this.DpkE.Dock = System.Windows.Forms.DockStyle.Left;
            this.DpkE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpkE.Location = new System.Drawing.Point(182, 5);
            this.DpkE.Name = "DpkE";
            this.DpkE.Size = new System.Drawing.Size(102, 21);
            this.DpkE.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Location = new System.Drawing.Point(172, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "-";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DpkS
            // 
            this.DpkS.CustomFormat = "yyyy-MM-dd";
            this.DpkS.Dock = System.Windows.Forms.DockStyle.Left;
            this.DpkS.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DpkS.Location = new System.Drawing.Point(70, 5);
            this.DpkS.Name = "DpkS";
            this.DpkS.Size = new System.Drawing.Size(102, 21);
            this.DpkS.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "检测日期:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ReportView
            // 
            this.ReportView.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ReportView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ReportView.Buttons = ((FastReport.PreviewButtons)(((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                        | FastReport.PreviewButtons.PageSetup)));
            this.ReportView.Clouds = FastReport.PreviewClouds.None;
            this.ReportView.Dock = System.Windows.Forms.DockStyle.Right;
            this.ReportView.Font = new System.Drawing.Font("宋体", 9F);
            this.ReportView.Location = new System.Drawing.Point(915, 31);
            this.ReportView.Name = "ReportView";
            this.ReportView.PageOffset = new System.Drawing.Point(10, 10);
            this.ReportView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ReportView.SaveInitialDirectory = null;
            this.ReportView.Size = new System.Drawing.Size(435, 580);
            this.ReportView.StatusbarVisible = false;
            this.ReportView.TabIndex = 4;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(912, 31);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 580);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // GrdX
            // 
            this.GrdX.AllowUserToAddRows = false;
            this.GrdX.AllowUserToDeleteRows = false;
            this.GrdX.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdX.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GrdX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdX.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column12,
            this.Column1,
            this.Column2,
            this.Column9,
            this.Column10,
            this.Column8,
            this.Column11,
            this.Column13,
            this.Column7});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(2);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdX.DefaultCellStyle = dataGridViewCellStyle4;
            this.GrdX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdX.Location = new System.Drawing.Point(0, 31);
            this.GrdX.MultiSelect = false;
            this.GrdX.Name = "GrdX";
            this.GrdX.ReadOnly = true;
            this.GrdX.RowHeadersWidth = 10;
            this.GrdX.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GrdX.RowTemplate.Height = 23;
            this.GrdX.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdX.Size = new System.Drawing.Size(912, 580);
            this.GrdX.TabIndex = 6;
            this.GrdX.SelectionChanged += new System.EventHandler(this.GrdX_SelectionChanged);
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.DataPropertyName = "TimeS";
            dataGridViewCellStyle2.Format = "yyyy-MM-dd HH:mm:ss";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column12.HeaderText = "开始时间";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 82;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.DataPropertyName = "TimeE";
            dataGridViewCellStyle3.Format = "yyyy-MM-dd HH:mm:ss";
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "结束时间";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 82;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.DataPropertyName = "VinCode";
            this.Column2.HeaderText = "VIN码";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 64;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.DataPropertyName = "CaliFcwDone";
            this.Column9.HeaderText = "雷达有否标定";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 87;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column10.DataPropertyName = "CaliFcwPass";
            this.Column10.HeaderText = "雷达是否成功";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 87;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8.DataPropertyName = "CaliFcwError";
            this.Column8.HeaderText = "雷达标定信息";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 106;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column11.DataPropertyName = "CaliLdwDone";
            this.Column11.HeaderText = "摄像头有否标定";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 99;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column13.DataPropertyName = "CaliLdwPass";
            this.Column13.HeaderText = "摄像头是否成功";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 99;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.DataPropertyName = "CaliLdwError";
            this.Column7.HeaderText = "摄像头标定信息";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 118;
            // 
            // FrmQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 611);
            this.Controls.Add(this.GrdX);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.ReportView);
            this.Controls.Add(this.panel1);
            this.Name = "FrmQuery";
            this.Text = "FrmQuery";
            this.Load += new System.EventHandler(this.FrmQuery_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DpkS;
        private System.Windows.Forms.DateTimePicker DpkE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnFindTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TbxVin;
        private System.Windows.Forms.Button BtnFindVin;
        private FastReport.Preview.PreviewControl ReportView;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView GrdX;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
    }
}