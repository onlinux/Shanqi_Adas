﻿using System.Drawing;

namespace FastReport
{
    public static class FastReportAddin
    {
        public static void SetText(this Report report, string name, string text)
        {
            if (report == null)
            {
                return;
            }
            try
            {
                TextObject box = report.FindObject(name) as TextObject;
                if (box != null)
                {
                    box.Text = text;
                }
            }
            catch { }
        }
    }
}
