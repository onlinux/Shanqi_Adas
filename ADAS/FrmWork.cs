﻿using System;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ADAS.Appx;
using ADAS.Data;
using DXP;
using DXP.Win;
using System.Collections.Generic;
using FastReport;
using System.IO;

namespace ADAS
{
    public partial class FrmWork : Form
    {
        private readonly object busy_lock = new object();

        public FrmWork() { InitializeComponent(); }

        private void FrmWork_Load(object sender, EventArgs e)
        {
            TabX.SelectedTab = PageScan;
            LabTitle.Text = Setting.Current.程序标题;
            ShowGold(InfoWait, "程序正在初始化，请稍等...");

            this.DxAsync(() =>
            {
                DxUI.Wait(1000);
            }, () =>
            {
                #region OBD有线串口
                try
                {
                    PortObdWired.PortName = string.Format("COM{0}", Setting.Current.有线OBD串口号);
                    PortObdWired.BaudRate = Setting.Current.有线OBD波特率;
                    PortObdWired.Parity = Setting.Current.有线OBD校验位;
                    PortObdWired.DataBits = Setting.Current.有线OBD数据位;
                    PortObdWired.StopBits = Setting.Current.有线OBD停止位;
                    PortObdWired.Open();
                    //AsObdWired.Active = PortObdWired.IsOpen;
                    ShowGreen(InfoWait, "打开有线OBD串口成功", 2000);
                }
                catch
                {
                    ShowRed(InfoWait, "打开有线OBD串口失败", 2000);
                }
                #endregion
                #region OBD无线串口
                try
                {
                    PortObdWireless.PortName = string.Format("COM{0}", Setting.Current.无线OBD串口号);
                    PortObdWireless.BaudRate = Setting.Current.无线OBD波特率;
                    PortObdWireless.Parity = Setting.Current.无线OBD校验位;
                    PortObdWireless.DataBits = Setting.Current.无线OBD数据位;
                    PortObdWireless.StopBits = Setting.Current.无线OBD停止位;
                    PortObdWireless.Open();
                    //AsObdWireless.Active = PortObdWireless.IsOpen;
                    ShowGreen(InfoWait, "打开无线OBD串口成功", 2000);
                }
                catch
                {
                    ShowRed(InfoWait, "打开无线OBD串口失败", 2000);
                }
                #endregion
                #region 扫码枪串口
                try
                {
                    PortScan.PortName = string.Format("COM{0}", Setting.Current.扫码枪串口号);
                    PortScan.BaudRate = Setting.Current.扫码枪波特率;
                    PortScan.Parity = Setting.Current.扫码枪校验位;
                    PortScan.DataBits = Setting.Current.扫码枪数据位;
                    PortScan.StopBits = Setting.Current.扫码枪停止位;
                    PortScan.Open();
                    //AsScaner.Active = PortScan.IsOpen;
                    ShowGreen(InfoWait, "打开扫码枪串口成功", 1000);
                }
                catch
                {
                    ShowRed(InfoWait, "打开扫码枪串口失败", 1000);
                }
                #endregion

                App.AssetForm.SetObdState(true, PortObdWired.IsOpen);
                App.AssetForm.SetObdState(false, PortObdWireless.IsOpen);

                App.AssetForm.SetScanerState(PortScan.IsOpen);

                LoadModels();

                Init();
            });
        }

        private void BtnWork_Click(object sender, EventArgs e)
        {
            DoTask(CbxVinCode.Text);
        }

        private void PortScan_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                DxUI.Wait(50);
                string input = PortScan.ReadExisting().Replace("\0", "").Trim();

                if (string.IsNullOrEmpty(input))
                {
                    return;
                }
                DoTask(input);
            }));
        }

        private void Init()
        {
            try
            {
                App.AssetForm.PosBackFcw();
                App.AssetForm.PosBackLdw();

                LabTitle.Text = Setting.Current.程序标题;
                TabX.SelectedTab = PageScan;
                BtnWork.Enabled = true;
                CbxVinCode.DataSource = Setting.Current.扫码历史;
                App.TaskExit = false;
                App.TaskBusy = false;
                AsBusy.Active = false;
                App.CurrentModel = new VehiModel();
                LabWaitPosFcw.Visible = false;
                LabResultFcw.Text = "--";
                LabResultFcw.BackColor = Color.SlateGray;
                LabResultAllFcw.Text = "--";
                LabResultAllFcw.BackColor = Color.SlateGray;
                LabErrFcw.Text = "--";

                LabWaitPosLdw.Visible = false;
                LabResultLdw.Text = "--";
                LabResultLdw.BackColor = Color.SlateGray;
                LabResultAllLdw.Text = "--";
                LabResultAllLdw.BackColor = Color.SlateGray;
                LabErrLdw.Text = "";

                LabVinWait.Text = LabVinFcw.Text = LabVinLdw.Text = LabVinResult.Text = "--";

                ShowGold(InfoWait, "等待扫码...");
                CbxModel.SelectedIndex = 0;
                CbxVinCode.Focus();
            }
            catch { }
        }

        private void LoadModels()
        {
            CbxModel.Items.Clear();
            CbxModel.Items.Add(new VehiModel { ModelCode = "----" });
            CbxModel.SelectedIndex = 0;

            VehiModel[] list = null;
            this.DxAsync
            (
                () => { list = VehiModel.List().ToArray(); },
                () => { CbxModel.Items.AddRange(list); }
            );
        }

        private void DoTask(string input)
        {
            #region 判断任务状态，防止重复进入
            lock (busy_lock)
            {
                if (App.TaskBusy)
                {
                    AlertX("系统忙碌中，不能重复开启任务", 3000);
                    return;
                }
                if (string.IsNullOrEmpty(input))
                {
                    AlertX("请扫描或录入车辆识别码", 3000);
                    return;
                }
                if (!AsAuto.Active)
                {
                    AlertX("请将机柜上的操作模式切换为[自动]", 3000);
                    return;
                }
                if (Setting.Current.车型识别方式 == Setting.VehiModelReadMode.ScanOrSelect)
                {
                    if (CbxModel.SelectedIndex == 0)
                    {
                        foreach (VehiModel model in CbxModel.Items)
                        {
                            if (0 == string.Compare(input, model.ModelCode, true))
                            {
                                CbxModel.SelectedItem = model;
                                return; ;
                            }
                        }
                        if (CbxModel.SelectedIndex == 0)
                        {
                            AlertX("选择车型或扫描车型码", 3000);
                            return;
                        }
                    }
                }
                App.TaskBusy = true;
                AsBusy.Active = true;
                App.TaskExit = false;
                BtnWork.Enabled = false;
            }
            #endregion

            Setting.Current.AddVinHistory(input);

            DxLogger logger = DxLogger.X("COMM");
            SerialPort obd = null;
            string command = string.Empty, result = string.Empty;
            int timeout = 5000, delay = 200;

            #region 初始化任务信息
            CbxVinCode.Text = input;
            App.CurrentTask.FromVin(input);//初始化任务信息
            if (Setting.Current.车型识别方式 == Setting.VehiModelReadMode.ScanOrSelect)
            {
                var model = CbxModel.SelectedItem as VehiModel;
                App.CurrentModel = model;
                App.CurrentTask.ModelCode = model.ModelCode;
                App.CurrentTask.ModelID = model.ModelID;
            }
            else
            {
                foreach (VehiModel model in CbxModel.Items)
                {
                    if (model != null && model.ModelID == App.CurrentTask.ModelID)
                    {
                        App.CurrentModel = model;
                        CbxModel.SelectedItem = model;
                        break;
                    }
                }
            }
            LabVinWait.Text = LabVinFcw.Text = LabVinLdw.Text = LabVinResult.Text = App.CurrentTask.VinCode;
            TabX.SelectedTab = PageWait;
            ShowGreen(InfoWait, string.Format("已扫码:{0}", input), 2000);
            #endregion

            #region 等待车辆到位
            for (int i = 0; !AsPosOkVehi.Active; i++)
            {
                if (App.TaskExit) { goto WhenExit; }
                ShowGold(InfoWait, string.Format("等待车辆到位...{0}", i), 1000);
                Application.DoEvents();
            }
            ShowGreen(InfoWait, "车辆已到位", 1000);
            #endregion

            #region 连接OBD
            if (App.TaskExit) { goto WhenExit; }
            obd = null;
            ShowGold(InfoWait, "请连接OBD...");
            while (obd == null)
            {
                if (App.TaskExit) { goto WhenExit; }
                if (Talk(PortObdWired, "AT I", 200).Contains("ELM327 v1.3a"))
                {
                    obd = PortObdWired;
                }
                else if (Talk(PortObdWireless, "AT I", 200).Contains("ELM327 v1.3a"))
                {
                    obd = PortObdWireless;
                }
                else
                {
                    DxUI.Wait(200);
                }
            }
            ShowGreen(InfoWait, "OBD已连接", 1000);
            #endregion

            #region OBD建立通信
            if (App.TaskExit) { goto WhenExit; }
            ShowGold(InfoWait, "正在建立通信...", 1000);
            Talk(obd, "AT WS");
            Talk(obd, "AT Z");
            Talk(obd, "AT SP 0");
            Talk(obd, "AT E0");
            Talk(obd, "AT H1");
            Talk(obd, "ST CSEGR 1");
            string reply = Talk(obd, "0100", 200);
            //if (false == reply.Contains("F8 7F 00 17"))
            //{
            //    ShowRed("建立通信失败,任务中止", 2000);
            //    goto Exit;
            //}
            ShowGreen(InfoWait, "建立通信成功", 1000);
            #endregion

            #region 摆正车辆
            if (App.TaskExit) { goto WhenExit; }
            ShowGold(InfoWait, "正在摆正车辆，请等待...");
            App.AssetForm.PusherMove(true, App.CurrentModel.PusherForcePercentF, App.CurrentModel.PusherForcePercentR);
            while (App.AssetForm.PusherRuning)
            {
                if (App.TaskExit) { goto WhenExit; }
                DxUI.Wait(1000);
            }
            ShowGreen(InfoWait, "车辆已摆正");
            #endregion

            #region FCW

            if (App.TaskExit) { goto WhenExit; }
            LabWaitPosFcw.Visible = false;
            TabX.SelectedTab = PageFCW;
            ShowGold(InfoFcw, "准备进行FCW标定", 2000);

            #region FCW设备移动

            ShowGold(InfoFcw, "正在移动FCW设备，请等待...");
            if (App.TaskExit) { goto WhenExit; }
            App.AssetForm.PosBackLdw();// 保证LDW设备归位

            if (App.TaskExit) { goto WhenExit; }
            int aebsFR = Setting.Current.FCW原点距前轴中心毫米数 - App.CurrentModel.RadarNearHead - 10 * Setting.Current.FCW标定距离厘米数;
            int aebsLR = Setting.Current.FCW原点距摆正中线毫米数 + App.CurrentModel.RadarNearMiddle;
            int aebsUD = App.CurrentModel.RadarHeight - Setting.Current.FCW原点距场地地面毫米数;

            LabTargetFcwFR.Text = aebsFR.ToString();
            LabTargetFcwLR.Text = aebsLR.ToString();
            LabTargetFcwUD.Text = aebsUD.ToString();

            App.AssetForm.PosMoveFcw(aebsFR, aebsLR, aebsUD);

            if (App.TaskExit) { goto WhenExit; }
            App.AssetForm.PusherMove(false, App.CurrentModel.PusherForcePercentF, App.CurrentModel.PusherForcePercentR);// 摆正器收回

            while (!App.AssetForm.PosOkFcw)
            {
                if (App.TaskExit) { goto WhenExit; }
                DxUI.Wait(1000);
            }
            LabWaitPosFcw.Visible = true;
            ShowGreen(InfoFcw, "FCW标定设备已到位", 1000);
            #endregion

            #region FCW标定

            if (App.TaskExit) { goto WhenExit; }

            ShowGold(InfoFcw, "开始FCW标定...", 1000);

            #region 初始化
            command = "AT WS ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 重启模块
            command = "AT E0 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// EchoOFF
            command = "AT H1 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// HeaderON
            command = "ST P41 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 切换11位250K
            command = "AT AL ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// AllowLongByte
            command = "AT CFC1 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// FC ON
            command = "AT ST 19 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 设置指令超时时间(毫秒)
            command = "AT SH 500 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 发送地址
            command = "AT CRA 501 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 接收地址
            #endregion

            #region FCW标定
            if (App.TaskExit) { goto WhenExit; }

            for (int i = 0; !App.TaskExit && i < Setting.Current.FCW标定尝试次数; i++)
            {
                //command = "03 00 B4 00 1E 00 00 00 ";
                command = "03";
                command += Convert.ToByte(Setting.Current.FCW标定距离厘米数).ToString("X2").PadLeft(4, '0').PadLeft(5, ' ');
                command += Convert.ToByte(Setting.Current.FCW标定范围厘米数).ToString("X2").PadLeft(4, '0').PadLeft(5, ' ');
                command += " 00 00 00";
                result = Talk(obd, command, timeout); DxUI.Wait(delay);// FCW标定

                command = "AT MA ";
                result = Talk(obd, command, timeout); DxUI.Wait(delay);// 监控FCW标定结果

                if (result.StartsWith("501 01"))
                {
                    App.CurrentTask.CaliFcwPass = true;
                    break;
                }
            }
            if (!App.TaskExit)
            {
                App.CurrentTask.CaliFcwDone = true;
                if (!App.CurrentTask.CaliFcwPass && result.Length >= 9)
                {
                    string err = result.Substring(7, 2);
                    if (err == "01")
                    {
                        App.CurrentTask.CaliFcwError = "安装位置向左偏出3°范围";
                    }
                    else if (err == "02")
                    {
                        App.CurrentTask.CaliFcwError = "安装位置向右偏出3°范围";
                    }
                }
                if (App.CurrentTask.CaliFcwPass)
                {
                    LabResultAllFcw.Text = LabResultFcw.Text = "OK";
                    LabResultAllFcw.BackColor = LabResultFcw.BackColor = Color.Lime;
                    LabErrFcw.Text = "";
                    ShowGreen(InfoFcw, "FCW标定已成功", 1000);
                }
                else
                {
                    LabResultAllFcw.Text = LabResultFcw.Text = "NOK";
                    LabResultAllFcw.BackColor = LabResultFcw.BackColor = Color.Tomato;
                    LabErrFcw.Text = App.CurrentTask.CaliFcwError;
                    ShowRed(InfoFcw, "FCW标定已失败", 1000);
                }
            }
            if (App.TaskExit) { goto WhenExit; }
            #endregion

            #endregion

            #region FCW设备归位

            App.AssetForm.PosBackFcw();//让FCW设备归位

            #endregion

            #endregion

            #region LDW

            if (App.TaskExit) { goto WhenExit; }

            LabWaitPosLdw.Visible = false;
            TabX.SelectedTab = PageLDW;
            ShowGold(InfoLdw, "准备进行LDW标定", 2000);

            #region LDW设备移动
            if (App.TaskExit) { goto WhenExit; }

            ShowGold(InfoLdw, "正在移动LDW设备，请等待...");

            App.AssetForm.PosBackFcw();//保证FCW设备归位

            int ldwFR = Setting.Current.LDW原点距前轴中心毫米数 - App.CurrentModel.CameraNearHead - Setting.Current.LDW标定距离毫米数;
            int ldwUD = Setting.Current.LDW原点距场地地面毫米数 - App.CurrentModel.CameraHeight;

            LabTargetLdwFR.Text = ldwFR.ToString();
            LabTargetLdwUD.Text = ldwUD.ToString();

            if (App.TaskExit) { goto WhenExit; }
            App.AssetForm.PosMoveLdw(ldwFR, ldwUD);
            DxUI.Wait(2000);//等待电机启动

            while (!App.AssetForm.PosOkLdw || !App.AssetForm.PosOkFcw)
            {
                if (App.TaskExit) { goto WhenExit; }
                DxUI.Wait(1000);
            }
            LabWaitPosLdw.Visible = true;
            ShowGreen(InfoLdw, "LDW标定设备已到位", 1000);

            #endregion

            #region LDW标定
            ShowGold(InfoLdw, "开始LDW标定...", 1000);

            #region 初始化通信参数
            command = "AT WS ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 重启模块
            command = "AT E0 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// EchoOFF
            command = "AT H1 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// HeaderON
            if (App.CurrentModel.CanProtocol == VehiModel.CAN_PROTOCOL.ISO_15765v4_CAN_29bit_250K)
            {
                command = "AT SP 9 ";
                result = Talk(obd, command, timeout); DxUI.Wait(delay);// 切换CAN 250K
            }
            else if (App.CurrentModel.CanProtocol == VehiModel.CAN_PROTOCOL.ISO_15765v4_CAN_29bit_500K)
            {
                command = "AT SP 7 ";
                result = Talk(obd, command, timeout); DxUI.Wait(delay);// 切换CAN 500K
            }
            command = "AT AL ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// AllowLongByte
            command = "AT CFC1 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// FC ON
            command = "ST CSEGT 1 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 
            command = "ST CSEGR 1 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 
            command = "AT ST 19 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 设置指令超时时间(毫秒)
            command = "AT SH DA9EFA ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 发送地址
            command = "AT CRA 18DAFA9E ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 接收地址
            #endregion

            #region 摄像头标定
            if (App.TaskExit) { goto WhenExit; }
            command = "10 01 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 进入默认模式
            command = "10 03 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 进入扩展模式
            command = "22 F190 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 读取VIN
            command = "22 F18C ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 读取ECU
            command = "22 CB06 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 读取标定结果
            command = "27 01 ";

            #region 解锁一级安全
            if (App.TaskExit) { goto WhenExit; }
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 请求一级安全
            if (result.Contains(" 67 01 "))
            {
                //byte[] seed = result.Substring(result.Length - 5, 5).Split(' ').Select(o => Convert.ToByte(o, 16)).ToArray();
                string ss = result.Substring(result.IndexOf(" 67 01 ") + 7, 5);
                logger.AddLog(ss);
                byte[] seed = ss.Split(' ').Select(o => Convert.ToByte(o, 16)).ToArray();
                logger.AddLog("[==] SecuritySeed: {0x" + seed[0].ToString("X2") + ", 0x" + seed[1].ToString("X2") + "}", Color.MistyRose);

                byte[] keys = new Algorithm().AlgorithmOne(seed[0], seed[1]);
                logger.AddLog("[==] SecurityKeys: {0x" + keys[0].ToString("X2") + ", 0x" + keys[1].ToString("X2") + "}", Color.MistyRose);

                command = "27 02 " + keys[0].ToString("X2") + " " + keys[1].ToString("X2");
                result = Talk(obd, command, timeout); DxUI.Wait(delay);// 解锁一级安全
            }
            /*
            if (!result.Contains(" 67 02"))
            {
                if (result.Contains(" 7F 27 12"))
                {
                    ShowRed(InfoLdw, "不支持的子功能", 3000);
                }
                else if (result.Contains(" 7F 27 13"))
                {
                    ShowRed(InfoLdw, "报文长度错误或格式非法", 3000);
                }
                else if (result.Contains(" 7F 27 22"))
                {
                    ShowRed(InfoLdw, "条件未满足", 3000);
                }
                else if (result.Contains(" 7F 27 24"))
                {
                    ShowRed(InfoLdw, "请求序列错误", 3000);
                }
                else if (result.Contains(" 7F 27 35"))
                {
                    ShowRed(InfoLdw, "密钥无效", 1000);
                }
                else if (result.Contains(" 7F 27 36"))
                {
                    ShowRed(InfoLdw, "超出密钥访问次数限制", 3000);
                }
                else if (result.Contains(" 7F 27 7E"))
                {
                    ShowRed(InfoLdw, "当前会话下不支持该子功能", 3000);
                }
                else if (result.Contains(" 7F 27 7F"))
                {
                    ShowRed(InfoLdw, "当前会话下不支持该服务", 3000);
                }

                goto WhenExit;
            }
            */
            #endregion

            #region 写入VIN
            if (App.TaskExit) { goto WhenExit; }
            command = "2E F190 " + string.Join(" ", App.CurrentTask.VinCode.Select(o => Convert.ToByte(o).ToString("X2"))).Trim();
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 写入VIN
            #endregion

            #region 写入车型参数
            if (App.TaskExit) { goto WhenExit; }
            command = "2E CB2B ";
            command += Convert.ToString(App.CurrentModel.VehiWidth, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');//车身宽度
            command += Convert.ToString(App.CurrentModel.CameraNearHead, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');//摄像头距离前轴
            command += Convert.ToString(Setting.Current.LDW标定距离毫米数, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');//标定距离
            command += Convert.ToString(300, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');
            command += Convert.ToString(App.CurrentModel.CameraHeight, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');//摄像头高度
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 写入车型参数
            #endregion

            #region 写入标定板参数
            if (App.TaskExit) { goto WhenExit; }
            //command = "2E CB2C 00E1 00E1 007A 007A 00AF 01 00";
            command = "2E CB2C";
            command += Convert.ToString(Setting.Current.摄像头左标板中心高厘米数, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' '); ;
            command += Convert.ToString(Setting.Current.摄像头右标板中心高厘米数, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' '); ;
            command += Convert.ToString(Setting.Current.摄像头左标板中心距中线厘米数, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');
            command += Convert.ToString(Setting.Current.摄像头右标板中心距中线厘米数, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');
            command += Convert.ToString(Setting.Current.摄像头标板方格边长毫米数, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' ');
            command += Convert.ToString((int)Setting.Current.摄像头右标板左下角方块颜色, 16).ToUpper().PadLeft(2, '0').PadLeft(3, ' ');
            command += Convert.ToString((int)Setting.Current.摄像头左标板右下角方块颜色, 16).ToUpper().PadLeft(2, '0').PadLeft(3, ' ');
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 写入标定板参数
            #endregion

            #region 写入摄像头外参
            if (App.TaskExit) { goto WhenExit; }
            //command = "2E CB2D 0044 0020 0000 0000";
            command = "2E CB2D";
            command += Convert.ToString(App.CurrentModel.CameraPitch, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' '); ;
            command += Convert.ToString(App.CurrentModel.CameraYaw, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' '); ;
            command += Convert.ToString(App.CurrentModel.CameraRoll, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' '); ;
            command += Convert.ToString(App.CurrentModel.CameraNearMiddle, 16).ToUpper().PadLeft(4, '0').PadLeft(5, ' '); ;
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 写入摄像头外参
            #endregion

            command = "10 03 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 进入扩展模式

            #region 解锁二级安全
            if (App.TaskExit) { goto WhenExit; }
            command = "27 03 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 请求二级安全
            if (result.Contains(" 67 03 "))
            {
                //byte[] seed = result.Substring(result.Length - 5, 5).Split(' ').Select(o => Convert.ToByte(o, 16)).ToArray();
                string ss = result.Substring(result.IndexOf(" 67 03 ") + 7, 5);
                logger.AddLog(ss);
                byte[] seed = ss.Split(' ').Select(o => Convert.ToByte(o, 16)).ToArray();
                logger.AddLog("[==] SecuritySeed: {0x" + seed[0].ToString("X2") + ", 0x" + seed[1].ToString("X2") + "}", Color.MistyRose);

                byte[] keys = new byte[2];
                if (App.CurrentModel.SecurityLevel == VehiModel.SECURITY_LEVEL.LEVEL_2)
                {
                    keys = new Algorithm().AlgorithmTwo(seed[0], seed[1]);
                }
                else if (App.CurrentModel.SecurityLevel == VehiModel.SECURITY_LEVEL.LEVEL_3)
                {
                    keys = new Algorithm().AlgorithmThree(seed[0], seed[1]);
                }
                logger.AddLog("[==] SecurityKeys: {0x" + keys[0].ToString("X2") + ", 0x" + keys[1].ToString("X2") + "}", Color.MistyRose);

                command = "27 04 " + keys[0].ToString("X2") + " " + keys[1].ToString("X2");
                result = Talk(obd, command, timeout); DxUI.Wait(delay);// 解锁二级安全
            }
            /*
            if (!result.Contains("67 04"))
            {
                if (result.Contains(" 7F 27 12"))
                {
                    ShowRed(InfoLdw, "不支持的子功能", 3000);
                }
                else if (result.Contains(" 7F 27 13"))
                {
                    ShowRed(InfoLdw, "报文长度错误或格式非法", 3000);
                }
                else if (result.Contains(" 7F 27 22"))
                {
                    ShowRed(InfoLdw, "条件未满足", 3000);
                }
                else if (result.Contains(" 7F 27 24"))
                {
                    ShowRed(InfoLdw, "请求序列错误", 3000);
                }
                else if (result.Contains(" 7F 27 35"))
                {
                    ShowRed(InfoLdw, "密钥无效", 1000);
                }
                else if (result.Contains(" 7F 27 36"))
                {
                    ShowRed(InfoLdw, "超出密钥访问次数限制", 3000);
                }
                else if (result.Contains(" 7F 27 7E"))
                {
                    ShowRed(InfoLdw, "当前会话下不支持该子功能", 3000);
                }
                else if (result.Contains(" 7F 27 7F"))
                {
                    ShowRed(InfoLdw, "当前会话下不支持该服务", 3000);
                }
                goto WhenExit;
            }
            */
            #endregion

            if (App.TaskExit) { goto WhenExit; }

            ShowGold(InfoLdw, "LDW标定中...");

            for (int i = 0; !App.TaskExit && i < Setting.Current.LDW标定尝试次数; i++)
            {
                command = "31 01 C012 ";
                result = Talk(obd, command, timeout); DxUI.Wait(2000);// 启动静态标定

                App.CurrentTask.CaliLdwDone = true;

                #region 读取标定状态
                while (!App.TaskExit)
                {
                    result = Talk(obd, "31 03 C012 ", timeout); DxUI.Wait(500);// 读取标定状态

                    if (result.Contains("71 03 C0 12 01"))
                    {
                        continue;
                    }
                    else if (result.Contains("71 03 C0 12 00"))
                    {
                        App.CurrentTask.CaliLdwError = "未能启动标定程序，可能通信协议版本不一致";
                        break;
                    }
                    else if (result.Contains("71 03 C0 12 02"))
                    {
                        App.CurrentTask.CaliLdwError = "标定失败,请重装ADAS设备，熄火10秒再试";
                        break;
                    }
                    else if (result.Contains("71 03 C0 12 03"))
                    {
                        App.CurrentTask.CaliLdwError = "探测不到目标，请检查目标板是否就位";
                        break;
                    }
                    else if (result.Contains("71 03 C0 12 04"))
                    {
                        App.CurrentTask.CaliLdwError = "目标数量太多，可能环境有干扰";
                        break;
                    }
                    else if (result.Contains("71 03 C0 12 05"))
                    {
                        App.CurrentTask.CaliLdwError = "";//标定完成
                        break;
                    }
                }
                #endregion
                if (result.Contains("71 03 C0 12 05")) { break; }
            }
            #region 处理标定结果

            result = Talk(obd, "22 CB06 ", timeout); DxUI.Wait(delay);// 读取标定结果
            int index = result.IndexOf("62 CB 06 ");
            if (index >= 0)
            {
                string ret = result.Substring(index + 9, 2);
                if (ret == "01")
                {
                    App.CurrentTask.CaliLdwPass = true;
                    App.CurrentTask.CaliLdwError = "";
                }
                else if (result.Length >= index + 15)
                {
                    string err = result.Substring(index + 12, 2);
                    App.CurrentTask.CaliLdwPass = false;
                    Dictionary<string, string> errs = new Dictionary<string, string>();
                    #region 失败原因集合
                    errs.Add("00", "未定义");
                    errs.Add("01", "无错误");
                    errs.Add("02", "找不到目标");
                    errs.Add("03", "Roll Angle 太大");
                    errs.Add("04", "FOE超出范围");
                    errs.Add("05", "参数加载失败");
                    errs.Add("06", "参数距离不合适");
                    errs.Add("07", "方块边长不合适");
                    errs.Add("08", "Yaw不合适");
                    errs.Add("09", "Horizon不合适");
                    errs.Add("0A", "目标过多");
                    errs.Add("0B", "运行错误");
                    errs.Add("0C", "速度太大");
                    errs.Add("0D", "没有VIN");
                    errs.Add("0E", "EyeQ未准备好");
                    errs.Add("0F", "高压");
                    errs.Add("10", "低压");
                    errs.Add("11", "发生busoff");
                    errs.Add("12", "高温");
                    errs.Add("13", "摄像头遮挡");
                    errs.Add("14", "内部故障");
                    errs.Add("15", "软件不匹配");
                    errs.Add("16", "Ry超界");
                    errs.Add("17", "Rz超界");
                    errs.Add("18", "图像过亮");
                    errs.Add("19", "图像过暗");
                    #endregion
                    App.CurrentTask.CaliLdwError = string.Format("{0}", errs.ContainsKey(err) ? errs[err] : "");
                }
            }
            #endregion

            if (App.TaskExit) { goto WhenExit; }

            command = "31 02 C012 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 结束静态标定
            command = "14 FF FF FF ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 清除故障
            command = "19 02 09 ";
            result = Talk(obd, command, timeout); DxUI.Wait(delay);// 读取故障
            #endregion

            if (App.CurrentTask.CaliLdwPass)
            {
                LabResultAllLdw.Text = LabResultLdw.Text = "OK";
                LabResultAllLdw.BackColor = LabResultLdw.BackColor = Color.Lime;
                ShowGreen(InfoLdw, "LDW标定已成功", 2000);
                LabErrLdw.Text = "";
            }
            else
            {
                LabResultAllLdw.Text = LabResultLdw.Text = "NOK";
                LabResultAllLdw.BackColor = LabResultLdw.BackColor = Color.Tomato;
                ShowRed(InfoLdw, "LDW标定已失败", 2000);
                LabErrLdw.Text = App.CurrentTask.CaliLdwError;
            }
            LabErrLdw.Text = App.CurrentTask.CaliLdwError;
            #endregion

            #region LDW设备归位
            App.AssetForm.PosBackLdw();
            #endregion

            #endregion

            #region 保存数据
            if (App.TaskExit) { goto WhenExit; }

            TabX.SelectedTab = PageResult;
            ShowGold(InfoResult, "保存数据...", 1000);
            if (App.CurrentTask.Save())
            {
                ShowGreen(InfoResult, "数据已保存", 2000);
            }
            else
            {
                ShowRed(InfoResult, "数据保存失败", 2000);
            }
            #endregion

            #region 打印结果
            if (App.TaskExit) { goto WhenExit; }

            using (Report report = new Report())
            {
                try
                {
                    report.Load(Path.Combine(Application.StartupPath, "Report", "ReportPrint.frx"));

                    VehiTask task = App.CurrentTask;
                    report.SetText("TxtVinCode", task.VinCode);
                    report.SetText("TxtModel", task.ModelCode);
                    report.SetText("TxtTimeS", string.Format("{0:yyyy-MM-dd HH:mm:ss}", task.TimeS));
                    report.SetText("TxtTimeE", string.Format("{0:yyyy-MM-dd HH:mm:ss}", task.TimeE));
                    report.SetText("TxtFcwResult", (!task.CaliFcwDone ? "未进行" : (task.CaliFcwPass ? "标定成功" : "标定失败")));
                    report.SetText("TxtFcwDesc", task.CaliFcwError);
                    report.SetText("TxtLdwResult", (!task.CaliLdwDone ? "未进行" : (task.CaliLdwPass ? "标定成功" : "标定失败")));
                    report.SetText("TxtLdwDesc", task.CaliLdwError);
                    report.SetText("TxtTimePrint", string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));

                    report.PrintSettings.ShowDialog = false;
                    report.Print();
                }
                catch { }
            }
            ShowGold(InfoResult, "正在打印结果...", 1000);
            for (int i = 0; i <= 5; i++)
            {
                DxUI.Wait(1000);
            }
            if (App.TaskExit) { goto WhenExit; }
            ShowGreen(InfoResult, "打印已完成，请取打印单", 2000);
        #endregion

        #region 如果放弃
        WhenExit:
            if (App.TaskExit)
            {
                TabX.SelectedTab = PageResult;
                ShowRed(InfoResult, "操作已放弃", 3000);
            }
            #endregion

            #region 断开OBD
            ShowGold(InfoResult, "请断开OBD连接...");
            while (obd != null && obd.IsOpen && Talk(obd, "AT I", 200).Contains("ELM327 v1.3a"))
            {
                DxUI.Wait(500);
            }
            ShowGold(InfoResult, "OBD连接已断开", 1000);
            #endregion

            #region 车辆驶离
            App.AssetForm.PosBackFcw();
            App.AssetForm.PosBackFcw();
            ShowGold(InfoLdw, "等待设备归位...");
            while (!App.AssetForm.PosOkFcw || !App.AssetForm.PosOkLdw)
            {
                DxUI.Wait(1000);
            }
            ShowGold(InfoResult, "请将车辆驶离...");
            for (int i = 0; i < Setting.Current.等待车辆驶离秒数; i++)
            {
                DxUI.Wait(1000);
            }
            #endregion

            #region 收尾工作
            Init();
            #endregion
        }

        #region 串口通信
        private string Talk(SerialPort port, string text, int timeout = 2000)
        {
            string result = string.Empty;
            try
            {
                port.DiscardInBuffer();
                port.DiscardOutBuffer();
                DxLogger.X("COMM").AddLog("[=>] " + text, Color.LightSalmon);
                port.Write(text + "\r");

                DateTime start = DateTime.Now;
                while (DateTime.Now.Subtract(start).TotalMilliseconds < timeout)
                {
                    if (!port.IsOpen) { break; }
                    if (port.BytesToRead > 0)
                    {
                        DxUI.Wait(50);
                        result = string.Format("{0}", port.ReadExisting()).Replace("\r", "").Replace(">", "").Trim();
                        break;
                    }
                    Application.DoEvents();
                }
            }
            catch (Exception ex)
            {
                DxLogger.X("COMM").AddLog(ex.ToString(), Color.Tomato);
            }
            DxLogger.X("COMM").AddLog("[<=] " + result, Color.LightGreen);
            return result.Trim();
        }
        #endregion

        #region 提示/等待
        public void WaitEvent(int ms)
        {
            if (ms > 0)
            {
                DateTime now = DateTime.Now;
                while (DateTime.Now.Subtract(now).TotalMilliseconds < ms)
                {
                    Application.DoEvents();
                    Thread.Sleep(1);
                }
            }
        }
        private void ShowInfo(Label label, string info, Color color, int keepMs = 0)
        {
            try
            {
                label.Invoke(new MethodInvoker(delegate
                {
                    DxLogger.X().AddLog(info, color);
                    label.ForeColor = color;
                    label.Text = info;
                    WaitEvent(keepMs);
                }));
            }
            catch { }
        }
        public void ShowInfo(Label label, string info, int keepMs = 0) { ShowInfo(label, info, Color.LightCyan, keepMs); }
        public void ShowRed(Label label, string info, int keepMs = 0) { ShowInfo(label, info, Color.Tomato, keepMs); }
        public void ShowGold(Label label, string info, int keepMs = 0) { ShowInfo(label, info, Color.Gold, keepMs); }
        public void ShowGreen(Label label, string info, int keepMs = 0) { ShowInfo(label, info, Color.Lime, keepMs); }

        public void AlertX(string info, int keepMs = 2000)
        {
            if (TabX.SelectedTab != PageScan)
            {
                return;
            }

            HiddenInfo.Visible = true;
            //HiddenInfo.Dock = DockStyle.None;
            //HiddenInfo.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            ShowRed(HiddenInfo, info, keepMs);
            HiddenInfo.Visible = false;
        }
        #endregion

        private void AsPosOkVehi_OnChange(object sender, EventArgs e) { AsPos.Visible = AsPosOkVehi.Active; }

        private void CbxModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            VehiModel model = CbxModel.SelectedItem as VehiModel;
            if (model != null)
            {
                App.CurrentModel = model;
                App.CurrentTask.ModelID = model.ModelID;
                App.CurrentTask.ModelCode = model.ModelCode;
            }
        }
    }
}
