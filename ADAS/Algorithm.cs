﻿
namespace ADAS
{
    public class Algorithm
    {
        public static ushort SeedValue = 0;

        public static ushort KeyValue = 0;

        public static byte KeyHigh = 0;

        public static byte KeyLow = 0;

        public static byte A = 158;

        public static ushort B = 26493;

        public static byte[] ArrayOfKeyValue;

        public byte[] AlgorithmOne(byte seedHigh, byte seedLow)
        {
            SeedValue = (ushort)((seedHigh << 8) | seedLow);
            KeyValue = (ushort)(~((SeedValue << 3) + SeedValue) ^ (A - 10));
            KeyHigh = (byte)((uint)(KeyValue >> 8) & 0xFFu);
            KeyLow = (byte)(KeyValue & 0xFFu);
            ArrayOfKeyValue[0] = KeyHigh;
            ArrayOfKeyValue[1] = KeyLow;
            return ArrayOfKeyValue;
        }

        public byte[] AlgorithmTwo(byte seedHigh, byte seedLow)
        {
            SeedValue = (ushort)((seedHigh << 8) | seedLow);
            KeyValue = (ushort)(~SeedValue + (B - 10));
            KeyHigh = (byte)((uint)(KeyValue >> 8) & 0xFFu);
            KeyLow = (byte)(KeyValue & 0xFFu);
            ArrayOfKeyValue[0] = KeyHigh;
            ArrayOfKeyValue[1] = KeyLow;
            return ArrayOfKeyValue;
        }

        public byte[] AlgorithmThree(byte seedHigh, byte seedLow)
        {
            SeedValue = (ushort)((seedHigh << 8) | seedLow);
            KeyLow = (byte)((~seedLow << 1) ^ (A - 10));
            KeyHigh = (byte)((~seedHigh << 1) ^ (B - 10));
            ArrayOfKeyValue[0] = KeyHigh;
            ArrayOfKeyValue[1] = KeyLow;
            return ArrayOfKeyValue;
        }

        static Algorithm()
        {
            byte[] array = (ArrayOfKeyValue = new byte[2]);
        }
    }
}
