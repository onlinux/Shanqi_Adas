﻿using ADAS.Appx;
using DXP.Win;
using System;
using System.Windows.Forms;

namespace ADAS
{
    public partial class FrmAsset : Form
    {
        public FrmAsset() { InitializeComponent(); }

        private void FrmAsset_Load(object sender, EventArgs e)
        {
            AxAuto_OnChange(AxAuto, new EventArgs());
        }

        private void AxAuto_OnChange(object sender, EventArgs e)
        {
            GrpFcw.Enabled = GrpLdw.Enabled = AxAuto.Active;
        }

        #region FCW

        public bool PosOkFcw { get { return (AxPosOkFcwFR.Active && AxPosOkFcwLR.Active && AxPosOkFcwUD.Active); } }
        public void PosBackFcw()
        {
            if (!AxAuto.Active) { return; }

            AxZeroFcwFR.Active = false; DxUI.Wait(55); AxZeroFcwFR.Active = true; DxUI.Wait(55);
            AxZeroFcwLR.Active = false; DxUI.Wait(55); AxZeroFcwLR.Active = true; DxUI.Wait(55);
            AxZeroFcwUD.Active = false; DxUI.Wait(55); AxZeroFcwUD.Active = true; DxUI.Wait(55);
        }
        public void PosMoveFcw(int fr, int lr, int ud)
        {
            if (!AxAuto.Active) { return; }

            AxZeroFcwFR.Active = false; DxUI.Wait(55); AxMoveFcwFR.Active = false; DxUI.Wait(55);
            AxZeroFcwLR.Active = false; DxUI.Wait(55); AxMoveFcwLR.Active = false; DxUI.Wait(55);
            AxZeroFcwUD.Active = false; DxUI.Wait(55); AxMoveFcwUD.Active = false; DxUI.Wait(55);

            AxTargetFcwFR.Value = fr; DxUI.Wait(55); AxMoveFcwFR.Active = true; DxUI.Wait(55);
            AxTargetFcwLR.Value = lr; DxUI.Wait(55); AxMoveFcwLR.Active = true; DxUI.Wait(55);
            AxTargetFcwUD.Value = ud; DxUI.Wait(55); AxMoveFcwUD.Active = true; DxUI.Wait(55);
        }


        private void AxPosOkFcwFR_OnChange(object sender, EventArgs e)
        {
            if (AxPosOkFcwFR.Active)
            {
                AxZeroFcwFR.Active = false; DxUI.Wait(55);
                AxMoveFcwFR.Active = false; DxUI.Wait(55);
            }
        }
        private void AxPosOkFcwLR_OnChange(object sender, EventArgs e)
        {
            if (AxPosOkFcwLR.Active)
            {
                AxZeroFcwLR.Active = false; DxUI.Wait(55);
                AxMoveFcwLR.Active = false; DxUI.Wait(55);
            }
        }
        private void AxPosOkFcwUD_OnChange(object sender, EventArgs e)
        {
            if (AxPosOkFcwUD.Active)
            {
                AxZeroFcwUD.Active = false; DxUI.Wait(55);
                AxMoveFcwUD.Active = false; DxUI.Wait(55);
            }
        }

        #endregion

        #region LDW

        public bool PosOkLdw { get { return (AxPosOkLdwFR.Active && AxPosOkLdwUD.Active); } }
        public void PosBackLdw()
        {
            if (!AxAuto.Active) { return; }

            AxZeroLdwFR.Active = false; DxUI.Wait(55); AxZeroLdwFR.Active = true; DxUI.Wait(55);
            AxZeroLdwUD.Active = false; DxUI.Wait(55); AxZeroLdwUD.Active = true; DxUI.Wait(55);
        }
        public void PosMoveLdw(int fr, int ud)
        {
            if (!AxAuto.Active) { return; }

            AxZeroLdwFR.Active = false; DxUI.Wait(55); AxMoveLdwFR.Active = false; DxUI.Wait(55);
            AxZeroLdwUD.Active = false; DxUI.Wait(55); AxMoveLdwUD.Active = false; DxUI.Wait(55);

            AxTargetLdwFR.Value = fr; DxUI.Wait(55); AxMoveLdwFR.Active = true; DxUI.Wait(55);
            AxTargetLdwUD.Value = ud; DxUI.Wait(55); AxMoveLdwUD.Active = true; DxUI.Wait(55);
        }

        private void AxPosOkLdwFR_OnChange(object sender, EventArgs e)
        {
            if (AxPosOkLdwFR.Active)
            {
                AxZeroLdwFR.Active = false; DxUI.Wait(55);
                AxMoveLdwFR.Active = false; DxUI.Wait(55);
            }
        }
        private void AxPosOkLdwUD_OnChange(object sender, EventArgs e)
        {
            if (AxPosOkLdwUD.Active)
            {
                AxZeroLdwUD.Active = false; DxUI.Wait(55);
                AxMoveLdwUD.Active = false; DxUI.Wait(55);
            }
        }

        #endregion

        #region 摆正车辆
        public bool PusherRuning { get { return (AxPusherRunF.Active || AxPusherRunR.Active); } }
        public bool PusherRunF { get { return AxPusherRunF.Active; } }
        public bool PusherRunR { get { return AxPusherRunR.Active; } }

        public void PusherMove(bool extent, int forcePercentF = 80, int forcePercentR = 200)
        {
            if (!AxAuto.Active) { return; }

            AxPusherForcePercentF.Value = (forcePercentF > 0 ? forcePercentF : 80); DxUI.Wait(55);
            AxPusherForcePercentR.Value = (forcePercentR > 0 ? forcePercentR : 200); DxUI.Wait(55);

            if (extent)
            {
                AxPusherOutF.Active = false; DxUI.Wait(55);
                AxPusherOutF.Active = true; DxUI.Wait(55);
                //
                AxPusherOutR.Active = false; DxUI.Wait(55);
                AxPusherOutR.Active = true; DxUI.Wait(55);
            }
            else
            {
                AxPusherInF.Active = false; DxUI.Wait(55);
                AxPusherInF.Active = true; DxUI.Wait(55);
                //
                AxPusherInR.Active = false; DxUI.Wait(55);
                AxPusherInR.Active = true; DxUI.Wait(55);
            }
            int i = 0;
            while (!App.AssetForm.PusherRuning && i < 1000 * Appx.Setting.Current.等待摆正器启动秒数)
            {
                i += 100;
                DxUI.Wait(100);
            }
        }
        #endregion


        public void SetObdState(bool wired, bool open) { (wired ? AsObdWired : AsObdWireless).Active = open; }

        public void SetScanerState(bool open) { AsScaner.Active = open; }
    }
}
