﻿namespace ADAS
{
    partial class FrmWork
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWork));
            this.PortScan = new System.IO.Ports.SerialPort(this.components);
            this.PortObdWired = new System.IO.Ports.SerialPort(this.components);
            this.PortObdWireless = new System.IO.Ports.SerialPort(this.components);
            this.PageLDW = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.InfoLdw = new System.Windows.Forms.Label();
            this.LabWaitPosLdw = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.axiSevenSegmentIntegerX5 = new AxisDigitalLibrary.AxiSevenSegmentIntegerX();
            this.axiSevenSegmentIntegerX6 = new AxisDigitalLibrary.AxiSevenSegmentIntegerX();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.LabTargetLdwUD = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.LabTargetLdwFR = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.LabErrLdw = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.LabVinLdw = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.LabResultLdw = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.PageFCW = new System.Windows.Forms.TabPage();
            this.LabErrFcw = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.LabVinFcw = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.LabResultFcw = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.InfoFcw = new System.Windows.Forms.Label();
            this.LabWaitPosFcw = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.axiSevenSegmentIntegerX3 = new AxisDigitalLibrary.AxiSevenSegmentIntegerX();
            this.axiSevenSegmentIntegerX2 = new AxisDigitalLibrary.AxiSevenSegmentIntegerX();
            this.axiSevenSegmentIntegerX1 = new AxisDigitalLibrary.AxiSevenSegmentIntegerX();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.LabTargetFcwLR = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LabTargetFcwUD = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LabTargetFcwFR = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PageScan = new System.Windows.Forms.TabPage();
            this.HiddenInfo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.LabTitle = new System.Windows.Forms.Label();
            this.CbxVinCode = new System.Windows.Forms.ComboBox();
            this.BtnWork = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CbxModel = new DXP.Win.UxCombo();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AsBusy = new AxisDigitalLibrary.AxiLedRectangleX();
            this.label3 = new System.Windows.Forms.Label();
            this.AsManual = new AxisDigitalLibrary.AxiLedRectangleX();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.AsAuto = new AxisDigitalLibrary.AxiLedRectangleX();
            this.AsPosOkVehi = new AxisDigitalLibrary.AxiLedRectangleX();
            this.label5 = new System.Windows.Forms.Label();
            this.AsPos = new AxisDigitalLibrary.AxiLedRectangleX();
            this.TabX = new System.Windows.Forms.TabControl();
            this.PageWait = new System.Windows.Forms.TabPage();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.LabVinWait = new System.Windows.Forms.Label();
            this.InfoWait = new System.Windows.Forms.Label();
            this.PageResult = new System.Windows.Forms.TabPage();
            this.LabVinResult = new System.Windows.Forms.Label();
            this.InfoResult = new System.Windows.Forms.Label();
            this.LabResultAllLdw = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.LabResultAllFcw = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.PageLDW.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.PageFCW.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.PageScan.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AsBusy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsManual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsPosOkVehi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsPos)).BeginInit();
            this.TabX.SuspendLayout();
            this.PageWait.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.PageResult.SuspendLayout();
            this.SuspendLayout();
            // 
            // PortScan
            // 
            this.PortScan.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.PortScan_DataReceived);
            // 
            // PortObdWired
            // 
            this.PortObdWired.BaudRate = 115200;
            // 
            // PageLDW
            // 
            this.PageLDW.Controls.Add(this.pictureBox2);
            this.PageLDW.Controls.Add(this.InfoLdw);
            this.PageLDW.Controls.Add(this.LabWaitPosLdw);
            this.PageLDW.Controls.Add(this.groupBox6);
            this.PageLDW.Controls.Add(this.pictureBox5);
            this.PageLDW.Controls.Add(this.groupBox7);
            this.PageLDW.Controls.Add(this.LabErrLdw);
            this.PageLDW.Controls.Add(this.label50);
            this.PageLDW.Controls.Add(this.LabVinLdw);
            this.PageLDW.Controls.Add(this.label47);
            this.PageLDW.Controls.Add(this.LabResultLdw);
            this.PageLDW.Controls.Add(this.label43);
            this.PageLDW.Location = new System.Drawing.Point(4, 24);
            this.PageLDW.Margin = new System.Windows.Forms.Padding(0);
            this.PageLDW.Name = "PageLDW";
            this.PageLDW.Size = new System.Drawing.Size(1342, 583);
            this.PageLDW.TabIndex = 2;
            this.PageLDW.Text = "LDW标定";
            this.PageLDW.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::ADAS.Properties.Resources.PicLDW;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(440, 453);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 39;
            this.pictureBox2.TabStop = false;
            // 
            // InfoLdw
            // 
            this.InfoLdw.BackColor = System.Drawing.Color.Black;
            this.InfoLdw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InfoLdw.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.InfoLdw.Font = new System.Drawing.Font("微软雅黑", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.InfoLdw.ForeColor = System.Drawing.Color.Gold;
            this.InfoLdw.Location = new System.Drawing.Point(0, 453);
            this.InfoLdw.Name = "InfoLdw";
            this.InfoLdw.Size = new System.Drawing.Size(1342, 130);
            this.InfoLdw.TabIndex = 38;
            this.InfoLdw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabWaitPosLdw
            // 
            this.LabWaitPosLdw.AutoSize = true;
            this.LabWaitPosLdw.BackColor = System.Drawing.Color.Lime;
            this.LabWaitPosLdw.Font = new System.Drawing.Font("宋体", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabWaitPosLdw.Location = new System.Drawing.Point(1197, 49);
            this.LabWaitPosLdw.Name = "LabWaitPosLdw";
            this.LabWaitPosLdw.Size = new System.Drawing.Size(97, 67);
            this.LabWaitPosLdw.TabIndex = 37;
            this.LabWaitPosLdw.Text = "OK";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.axiSevenSegmentIntegerX5);
            this.groupBox6.Controls.Add(this.axiSevenSegmentIntegerX6);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Font = new System.Drawing.Font("微软雅黑", 14.25F);
            this.groupBox6.Location = new System.Drawing.Point(930, 27);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(263, 111);
            this.groupBox6.TabIndex = 36;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "当前位置";
            // 
            // axiSevenSegmentIntegerX5
            // 
            this.axiSevenSegmentIntegerX5.Location = new System.Drawing.Point(106, 66);
            this.axiSevenSegmentIntegerX5.Name = "axiSevenSegmentIntegerX5";
            this.axiSevenSegmentIntegerX5.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSevenSegmentIntegerX5.OcxState")));
            this.axiSevenSegmentIntegerX5.Size = new System.Drawing.Size(88, 32);
            this.axiSevenSegmentIntegerX5.TabIndex = 10;
            // 
            // axiSevenSegmentIntegerX6
            // 
            this.axiSevenSegmentIntegerX6.Location = new System.Drawing.Point(106, 24);
            this.axiSevenSegmentIntegerX6.Name = "axiSevenSegmentIntegerX6";
            this.axiSevenSegmentIntegerX6.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSevenSegmentIntegerX6.OcxState")));
            this.axiSevenSegmentIntegerX6.Size = new System.Drawing.Size(88, 32);
            this.axiSevenSegmentIntegerX6.TabIndex = 9;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(206, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 25);
            this.label27.TabIndex = 4;
            this.label27.Text = "mm";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(15, 74);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(88, 25);
            this.label28.TabIndex = 3;
            this.label28.Text = "上下位置";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(206, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(48, 25);
            this.label29.TabIndex = 2;
            this.label29.Text = "mm";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(15, 32);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 25);
            this.label30.TabIndex = 0;
            this.label30.Text = "目标距离";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(756, 59);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(83, 47);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 35;
            this.pictureBox5.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.LabTargetLdwUD);
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Controls.Add(this.label36);
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.LabTargetLdwFR);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Font = new System.Drawing.Font("微软雅黑", 14.25F);
            this.groupBox7.Location = new System.Drawing.Point(480, 27);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(218, 111);
            this.groupBox7.TabIndex = 34;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "目标位置";
            // 
            // LabTargetLdwUD
            // 
            this.LabTargetLdwUD.AutoSize = true;
            this.LabTargetLdwUD.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabTargetLdwUD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.LabTargetLdwUD.Location = new System.Drawing.Point(108, 68);
            this.LabTargetLdwUD.Name = "LabTargetLdwUD";
            this.LabTargetLdwUD.Size = new System.Drawing.Size(46, 21);
            this.LabTargetLdwUD.TabIndex = 5;
            this.LabTargetLdwUD.Text = "350";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(174, 70);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(48, 25);
            this.label35.TabIndex = 4;
            this.label35.Text = "mm";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 70);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(88, 25);
            this.label36.TabIndex = 3;
            this.label36.Text = "上下位置";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(174, 34);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(48, 25);
            this.label37.TabIndex = 2;
            this.label37.Text = "mm";
            // 
            // LabTargetLdwFR
            // 
            this.LabTargetLdwFR.AutoSize = true;
            this.LabTargetLdwFR.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabTargetLdwFR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.LabTargetLdwFR.Location = new System.Drawing.Point(108, 32);
            this.LabTargetLdwFR.Name = "LabTargetLdwFR";
            this.LabTargetLdwFR.Size = new System.Drawing.Size(58, 21);
            this.LabTargetLdwFR.TabIndex = 1;
            this.LabTargetLdwFR.Text = "2500";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 34);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(88, 25);
            this.label39.TabIndex = 0;
            this.label39.Text = "目标距离";
            // 
            // LabErrLdw
            // 
            this.LabErrLdw.BackColor = System.Drawing.Color.Silver;
            this.LabErrLdw.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabErrLdw.Location = new System.Drawing.Point(640, 390);
            this.LabErrLdw.Name = "LabErrLdw";
            this.LabErrLdw.Size = new System.Drawing.Size(680, 40);
            this.LabErrLdw.TabIndex = 33;
            this.LabErrLdw.Text = "--";
            this.LabErrLdw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label50.Location = new System.Drawing.Point(460, 390);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(180, 40);
            this.label50.TabIndex = 32;
            this.label50.Text = "失败原因";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabVinLdw
            // 
            this.LabVinLdw.BackColor = System.Drawing.Color.Silver;
            this.LabVinLdw.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabVinLdw.Location = new System.Drawing.Point(640, 250);
            this.LabVinLdw.Name = "LabVinLdw";
            this.LabVinLdw.Size = new System.Drawing.Size(680, 40);
            this.LabVinLdw.TabIndex = 29;
            this.LabVinLdw.Text = "LX142382";
            this.LabVinLdw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label47.Location = new System.Drawing.Point(460, 250);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(180, 40);
            this.label47.TabIndex = 28;
            this.label47.Text = "VIN码";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabResultLdw
            // 
            this.LabResultLdw.BackColor = System.Drawing.Color.SlateGray;
            this.LabResultLdw.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabResultLdw.Location = new System.Drawing.Point(640, 320);
            this.LabResultLdw.Name = "LabResultLdw";
            this.LabResultLdw.Size = new System.Drawing.Size(680, 40);
            this.LabResultLdw.TabIndex = 25;
            this.LabResultLdw.Text = "--";
            this.LabResultLdw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label43.Location = new System.Drawing.Point(460, 320);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(180, 40);
            this.label43.TabIndex = 24;
            this.label43.Text = "标定结果";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PageFCW
            // 
            this.PageFCW.Controls.Add(this.LabErrFcw);
            this.PageFCW.Controls.Add(this.label18);
            this.PageFCW.Controls.Add(this.LabVinFcw);
            this.PageFCW.Controls.Add(this.label33);
            this.PageFCW.Controls.Add(this.LabResultFcw);
            this.PageFCW.Controls.Add(this.label38);
            this.PageFCW.Controls.Add(this.pictureBox1);
            this.PageFCW.Controls.Add(this.InfoFcw);
            this.PageFCW.Controls.Add(this.LabWaitPosFcw);
            this.PageFCW.Controls.Add(this.groupBox5);
            this.PageFCW.Controls.Add(this.pictureBox4);
            this.PageFCW.Controls.Add(this.groupBox4);
            this.PageFCW.Location = new System.Drawing.Point(4, 24);
            this.PageFCW.Margin = new System.Windows.Forms.Padding(0);
            this.PageFCW.Name = "PageFCW";
            this.PageFCW.Size = new System.Drawing.Size(1342, 583);
            this.PageFCW.TabIndex = 1;
            this.PageFCW.Text = "FCW标定";
            this.PageFCW.UseVisualStyleBackColor = true;
            // 
            // LabErrFcw
            // 
            this.LabErrFcw.BackColor = System.Drawing.Color.Silver;
            this.LabErrFcw.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabErrFcw.Location = new System.Drawing.Point(640, 390);
            this.LabErrFcw.Name = "LabErrFcw";
            this.LabErrFcw.Size = new System.Drawing.Size(680, 40);
            this.LabErrFcw.TabIndex = 51;
            this.LabErrFcw.Text = "--";
            this.LabErrFcw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(460, 390);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(180, 40);
            this.label18.TabIndex = 50;
            this.label18.Text = "失败原因";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabVinFcw
            // 
            this.LabVinFcw.BackColor = System.Drawing.Color.Silver;
            this.LabVinFcw.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabVinFcw.Location = new System.Drawing.Point(640, 250);
            this.LabVinFcw.Name = "LabVinFcw";
            this.LabVinFcw.Size = new System.Drawing.Size(680, 40);
            this.LabVinFcw.TabIndex = 49;
            this.LabVinFcw.Text = "LX142382";
            this.LabVinFcw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label33.Location = new System.Drawing.Point(460, 250);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(180, 40);
            this.label33.TabIndex = 48;
            this.label33.Text = "VIN码";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabResultFcw
            // 
            this.LabResultFcw.BackColor = System.Drawing.Color.SlateGray;
            this.LabResultFcw.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabResultFcw.Location = new System.Drawing.Point(640, 320);
            this.LabResultFcw.Name = "LabResultFcw";
            this.LabResultFcw.Size = new System.Drawing.Size(680, 40);
            this.LabResultFcw.TabIndex = 47;
            this.LabResultFcw.Text = "--";
            this.LabResultFcw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label38.Location = new System.Drawing.Point(460, 320);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(180, 40);
            this.label38.TabIndex = 46;
            this.label38.Text = "标定结果";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::ADAS.Properties.Resources.PicFCW;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(440, 453);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // InfoFcw
            // 
            this.InfoFcw.BackColor = System.Drawing.Color.Black;
            this.InfoFcw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InfoFcw.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.InfoFcw.Font = new System.Drawing.Font("微软雅黑", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.InfoFcw.ForeColor = System.Drawing.Color.Gold;
            this.InfoFcw.Location = new System.Drawing.Point(0, 453);
            this.InfoFcw.Name = "InfoFcw";
            this.InfoFcw.Size = new System.Drawing.Size(1342, 130);
            this.InfoFcw.TabIndex = 44;
            this.InfoFcw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabWaitPosFcw
            // 
            this.LabWaitPosFcw.AutoSize = true;
            this.LabWaitPosFcw.BackColor = System.Drawing.Color.Lime;
            this.LabWaitPosFcw.Font = new System.Drawing.Font("宋体", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabWaitPosFcw.Location = new System.Drawing.Point(1226, 60);
            this.LabWaitPosFcw.Name = "LabWaitPosFcw";
            this.LabWaitPosFcw.Size = new System.Drawing.Size(97, 67);
            this.LabWaitPosFcw.TabIndex = 27;
            this.LabWaitPosFcw.Text = "OK";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.axiSevenSegmentIntegerX3);
            this.groupBox5.Controls.Add(this.axiSevenSegmentIntegerX2);
            this.groupBox5.Controls.Add(this.axiSevenSegmentIntegerX1);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Font = new System.Drawing.Font("微软雅黑", 14.25F);
            this.groupBox5.Location = new System.Drawing.Point(930, 27);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(289, 169);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "当前位置";
            // 
            // axiSevenSegmentIntegerX3
            // 
            this.axiSevenSegmentIntegerX3.Location = new System.Drawing.Point(106, 121);
            this.axiSevenSegmentIntegerX3.Name = "axiSevenSegmentIntegerX3";
            this.axiSevenSegmentIntegerX3.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSevenSegmentIntegerX3.OcxState")));
            this.axiSevenSegmentIntegerX3.Size = new System.Drawing.Size(102, 32);
            this.axiSevenSegmentIntegerX3.TabIndex = 11;
            // 
            // axiSevenSegmentIntegerX2
            // 
            this.axiSevenSegmentIntegerX2.Location = new System.Drawing.Point(106, 77);
            this.axiSevenSegmentIntegerX2.Name = "axiSevenSegmentIntegerX2";
            this.axiSevenSegmentIntegerX2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSevenSegmentIntegerX2.OcxState")));
            this.axiSevenSegmentIntegerX2.Size = new System.Drawing.Size(102, 32);
            this.axiSevenSegmentIntegerX2.TabIndex = 10;
            // 
            // axiSevenSegmentIntegerX1
            // 
            this.axiSevenSegmentIntegerX1.Location = new System.Drawing.Point(106, 33);
            this.axiSevenSegmentIntegerX1.Name = "axiSevenSegmentIntegerX1";
            this.axiSevenSegmentIntegerX1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axiSevenSegmentIntegerX1.OcxState")));
            this.axiSevenSegmentIntegerX1.Size = new System.Drawing.Size(102, 32);
            this.axiSevenSegmentIntegerX1.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(216, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 25);
            this.label17.TabIndex = 8;
            this.label17.Text = "mm";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 128);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 25);
            this.label19.TabIndex = 6;
            this.label19.Text = "左右位置";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(216, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 25);
            this.label21.TabIndex = 4;
            this.label21.Text = "mm";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 84);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 25);
            this.label22.TabIndex = 3;
            this.label22.Text = "上下位置";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(216, 40);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 25);
            this.label23.TabIndex = 2;
            this.label23.Text = "mm";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 40);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 25);
            this.label25.TabIndex = 0;
            this.label25.Text = "目标距离";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(764, 85);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(124, 45);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 25;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.LabTargetFcwLR);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.LabTargetFcwUD);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.LabTargetFcwFR);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox4.Location = new System.Drawing.Point(480, 27);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(233, 169);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "目标位置";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(174, 128);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 25);
            this.label16.TabIndex = 8;
            this.label16.Text = "mm";
            // 
            // LabTargetFcwLR
            // 
            this.LabTargetFcwLR.AutoSize = true;
            this.LabTargetFcwLR.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabTargetFcwLR.ForeColor = System.Drawing.Color.Blue;
            this.LabTargetFcwLR.Location = new System.Drawing.Point(103, 127);
            this.LabTargetFcwLR.Name = "LabTargetFcwLR";
            this.LabTargetFcwLR.Size = new System.Drawing.Size(58, 21);
            this.LabTargetFcwLR.TabIndex = 7;
            this.LabTargetFcwLR.Text = "1400";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 128);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 25);
            this.label14.TabIndex = 6;
            this.label14.Text = "左右位置";
            // 
            // LabTargetFcwUD
            // 
            this.LabTargetFcwUD.AutoSize = true;
            this.LabTargetFcwUD.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabTargetFcwUD.ForeColor = System.Drawing.Color.Blue;
            this.LabTargetFcwUD.Location = new System.Drawing.Point(103, 83);
            this.LabTargetFcwUD.Name = "LabTargetFcwUD";
            this.LabTargetFcwUD.Size = new System.Drawing.Size(46, 21);
            this.LabTargetFcwUD.TabIndex = 5;
            this.LabTargetFcwUD.Text = "350";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(174, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 25);
            this.label10.TabIndex = 4;
            this.label10.Text = "mm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 25);
            this.label9.TabIndex = 3;
            this.label9.Text = "上下位置";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(174, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 25);
            this.label8.TabIndex = 2;
            this.label8.Text = "mm";
            // 
            // LabTargetFcwFR
            // 
            this.LabTargetFcwFR.AutoSize = true;
            this.LabTargetFcwFR.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabTargetFcwFR.ForeColor = System.Drawing.Color.Blue;
            this.LabTargetFcwFR.Location = new System.Drawing.Point(103, 39);
            this.LabTargetFcwFR.Name = "LabTargetFcwFR";
            this.LabTargetFcwFR.Size = new System.Drawing.Size(58, 21);
            this.LabTargetFcwFR.TabIndex = 1;
            this.LabTargetFcwFR.Text = "2500";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 25);
            this.label6.TabIndex = 0;
            this.label6.Text = "目标距离";
            // 
            // PageScan
            // 
            this.PageScan.Controls.Add(this.HiddenInfo);
            this.PageScan.Controls.Add(this.panel1);
            this.PageScan.Controls.Add(this.label2);
            this.PageScan.Controls.Add(this.pictureBox9);
            this.PageScan.Controls.Add(this.groupBox2);
            this.PageScan.Controls.Add(this.groupBox1);
            this.PageScan.Controls.Add(this.AsPos);
            this.PageScan.Location = new System.Drawing.Point(4, 24);
            this.PageScan.Margin = new System.Windows.Forms.Padding(0);
            this.PageScan.Name = "PageScan";
            this.PageScan.Size = new System.Drawing.Size(1342, 583);
            this.PageScan.TabIndex = 0;
            this.PageScan.Text = "主界面";
            this.PageScan.UseVisualStyleBackColor = true;
            // 
            // HiddenInfo
            // 
            this.HiddenInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.HiddenInfo.BackColor = System.Drawing.Color.Black;
            this.HiddenInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HiddenInfo.Font = new System.Drawing.Font("微软雅黑", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.HiddenInfo.ForeColor = System.Drawing.Color.Gold;
            this.HiddenInfo.Location = new System.Drawing.Point(-2, 340);
            this.HiddenInfo.Name = "HiddenInfo";
            this.HiddenInfo.Size = new System.Drawing.Size(1346, 130);
            this.HiddenInfo.TabIndex = 95;
            this.HiddenInfo.Text = "请连接OBD...";
            this.HiddenInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.HiddenInfo.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.LabTitle);
            this.panel1.Controls.Add(this.CbxVinCode);
            this.panel1.Controls.Add(this.BtnWork);
            this.panel1.Location = new System.Drawing.Point(69, 83);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(1254, 288);
            this.panel1.TabIndex = 93;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(112, 170);
            this.label1.Margin = new System.Windows.Forms.Padding(11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 72);
            this.label1.TabIndex = 81;
            this.label1.Text = "VIN码:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabTitle
            // 
            this.LabTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.LabTitle.Font = new System.Drawing.Font("微软雅黑", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabTitle.ForeColor = System.Drawing.Color.Black;
            this.LabTitle.Location = new System.Drawing.Point(5, 5);
            this.LabTitle.Margin = new System.Windows.Forms.Padding(5);
            this.LabTitle.Name = "LabTitle";
            this.LabTitle.Size = new System.Drawing.Size(1244, 100);
            this.LabTitle.TabIndex = 80;
            this.LabTitle.Text = "杰胜卡特驾驶辅助标定系统";
            this.LabTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CbxVinCode
            // 
            this.CbxVinCode.BackColor = System.Drawing.SystemColors.Info;
            this.CbxVinCode.Font = new System.Drawing.Font("新宋体", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CbxVinCode.ForeColor = System.Drawing.Color.Sienna;
            this.CbxVinCode.FormattingEnabled = true;
            this.CbxVinCode.Location = new System.Drawing.Point(329, 163);
            this.CbxVinCode.Margin = new System.Windows.Forms.Padding(11);
            this.CbxVinCode.Name = "CbxVinCode";
            this.CbxVinCode.Size = new System.Drawing.Size(672, 61);
            this.CbxVinCode.TabIndex = 82;
            // 
            // BtnWork
            // 
            this.BtnWork.Enabled = false;
            this.BtnWork.Font = new System.Drawing.Font("微软雅黑", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnWork.ForeColor = System.Drawing.Color.Black;
            this.BtnWork.Location = new System.Drawing.Point(1023, 163);
            this.BtnWork.Margin = new System.Windows.Forms.Padding(11);
            this.BtnWork.Name = "BtnWork";
            this.BtnWork.Size = new System.Drawing.Size(139, 76);
            this.BtnWork.TabIndex = 83;
            this.BtnWork.Text = "开始";
            this.BtnWork.UseVisualStyleBackColor = true;
            this.BtnWork.Click += new System.EventHandler(this.BtnWork_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label2.Location = new System.Drawing.Point(62, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 50);
            this.label2.TabIndex = 92;
            this.label2.Text = "杰胜卡特";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::ADAS.Properties.Resources.mmexport1609130530440;
            this.pictureBox9.Location = new System.Drawing.Point(6, 6);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(50, 50);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 91;
            this.pictureBox9.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.CbxModel);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(555, 485);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(350, 100);
            this.groupBox2.TabIndex = 89;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "车型选择";
            // 
            // CbxModel
            // 
            this.CbxModel.BackColor = System.Drawing.Color.Black;
            this.CbxModel.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CbxModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxModel.Font = new System.Drawing.Font("新宋体", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CbxModel.ForeColor = System.Drawing.Color.Lime;
            this.CbxModel.FormattingEnabled = true;
            this.CbxModel.Items.AddRange(new object[] {
            "车型"});
            this.CbxModel.Location = new System.Drawing.Point(121, 30);
            this.CbxModel.Margin = new System.Windows.Forms.Padding(11);
            this.CbxModel.Name = "CbxModel";
            this.CbxModel.Size = new System.Drawing.Size(196, 45);
            this.CbxModel.TabIndex = 87;
            this.CbxModel.SelectedIndexChanged += new System.EventHandler(this.CbxModel_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(30, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 36);
            this.label4.TabIndex = 86;
            this.label4.Text = "车型:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.AsBusy);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.AsManual);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.AsAuto);
            this.groupBox1.Controls.Add(this.AsPosOkVehi);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(0, 485);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(540, 100);
            this.groupBox1.TabIndex = 87;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "状态";
            // 
            // AsBusy
            // 
            this.AsBusy.Location = new System.Drawing.Point(378, 36);
            this.AsBusy.Name = "AsBusy";
            this.AsBusy.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsBusy.OcxState")));
            this.AsBusy.Size = new System.Drawing.Size(30, 30);
            this.AsBusy.TabIndex = 78;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.SteelBlue;
            this.label3.Location = new System.Drawing.Point(414, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 30);
            this.label3.TabIndex = 79;
            this.label3.Text = "工作状态";
            // 
            // AsManual
            // 
            this.AsManual.Location = new System.Drawing.Point(107, 36);
            this.AsManual.Name = "AsManual";
            this.AsManual.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsManual.OcxState")));
            this.AsManual.Size = new System.Drawing.Size(30, 30);
            this.AsManual.TabIndex = 75;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.Color.SteelBlue;
            this.label11.Location = new System.Drawing.Point(137, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 30);
            this.label11.TabIndex = 74;
            this.label11.Text = "手动";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.ForeColor = System.Drawing.Color.SteelBlue;
            this.label12.Location = new System.Drawing.Point(41, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 30);
            this.label12.TabIndex = 76;
            this.label12.Text = "自动";
            // 
            // AsAuto
            // 
            this.AsAuto.Location = new System.Drawing.Point(11, 36);
            this.AsAuto.Name = "AsAuto";
            this.AsAuto.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsAuto.OcxState")));
            this.AsAuto.Size = new System.Drawing.Size(30, 30);
            this.AsAuto.TabIndex = 77;
            // 
            // AsPosOkVehi
            // 
            this.AsPosOkVehi.Location = new System.Drawing.Point(219, 36);
            this.AsPosOkVehi.Name = "AsPosOkVehi";
            this.AsPosOkVehi.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsPosOkVehi.OcxState")));
            this.AsPosOkVehi.Size = new System.Drawing.Size(30, 30);
            this.AsPosOkVehi.TabIndex = 72;
            this.AsPosOkVehi.OnChange += new System.EventHandler(this.AsPosOkVehi_OnChange);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.SteelBlue;
            this.label5.Location = new System.Drawing.Point(255, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 30);
            this.label5.TabIndex = 73;
            this.label5.Text = "车辆到位";
            // 
            // AsPos
            // 
            this.AsPos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AsPos.Location = new System.Drawing.Point(1094, 485);
            this.AsPos.Name = "AsPos";
            this.AsPos.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AsPos.OcxState")));
            this.AsPos.Size = new System.Drawing.Size(250, 100);
            this.AsPos.TabIndex = 72;
            this.AsPos.Visible = false;
            // 
            // TabX
            // 
            this.TabX.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TabX.Controls.Add(this.PageScan);
            this.TabX.Controls.Add(this.PageWait);
            this.TabX.Controls.Add(this.PageFCW);
            this.TabX.Controls.Add(this.PageLDW);
            this.TabX.Controls.Add(this.PageResult);
            this.TabX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabX.ItemSize = new System.Drawing.Size(20, 20);
            this.TabX.Location = new System.Drawing.Point(0, 0);
            this.TabX.Margin = new System.Windows.Forms.Padding(0);
            this.TabX.Multiline = true;
            this.TabX.Name = "TabX";
            this.TabX.Padding = new System.Drawing.Point(0, 0);
            this.TabX.SelectedIndex = 0;
            this.TabX.Size = new System.Drawing.Size(1350, 611);
            this.TabX.TabIndex = 21;
            // 
            // PageWait
            // 
            this.PageWait.Controls.Add(this.pictureBox8);
            this.PageWait.Controls.Add(this.pictureBox7);
            this.PageWait.Controls.Add(this.pictureBox6);
            this.PageWait.Controls.Add(this.LabVinWait);
            this.PageWait.Controls.Add(this.InfoWait);
            this.PageWait.Location = new System.Drawing.Point(4, 24);
            this.PageWait.Margin = new System.Windows.Forms.Padding(0);
            this.PageWait.Name = "PageWait";
            this.PageWait.Size = new System.Drawing.Size(1342, 583);
            this.PageWait.TabIndex = 5;
            this.PageWait.Text = "车辆准备";
            this.PageWait.UseVisualStyleBackColor = true;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(600, 246);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(143, 64);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 26;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(828, 153);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(250, 250);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 25;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(265, 153);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(250, 250);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 24;
            this.pictureBox6.TabStop = false;
            // 
            // LabVinWait
            // 
            this.LabVinWait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabVinWait.Font = new System.Drawing.Font("宋体", 49F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabVinWait.Location = new System.Drawing.Point(265, 39);
            this.LabVinWait.Name = "LabVinWait";
            this.LabVinWait.Size = new System.Drawing.Size(813, 77);
            this.LabVinWait.TabIndex = 23;
            this.LabVinWait.Text = "--";
            this.LabVinWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoWait
            // 
            this.InfoWait.BackColor = System.Drawing.Color.Black;
            this.InfoWait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InfoWait.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.InfoWait.Font = new System.Drawing.Font("微软雅黑", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.InfoWait.ForeColor = System.Drawing.Color.Gold;
            this.InfoWait.Location = new System.Drawing.Point(0, 453);
            this.InfoWait.Name = "InfoWait";
            this.InfoWait.Size = new System.Drawing.Size(1342, 130);
            this.InfoWait.TabIndex = 22;
            this.InfoWait.Text = "请连接OBD...";
            this.InfoWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PageResult
            // 
            this.PageResult.Controls.Add(this.LabVinResult);
            this.PageResult.Controls.Add(this.InfoResult);
            this.PageResult.Controls.Add(this.LabResultAllLdw);
            this.PageResult.Controls.Add(this.label55);
            this.PageResult.Controls.Add(this.LabResultAllFcw);
            this.PageResult.Controls.Add(this.label53);
            this.PageResult.Location = new System.Drawing.Point(4, 24);
            this.PageResult.Margin = new System.Windows.Forms.Padding(0);
            this.PageResult.Name = "PageResult";
            this.PageResult.Size = new System.Drawing.Size(1342, 583);
            this.PageResult.TabIndex = 4;
            this.PageResult.Text = "标定结果";
            this.PageResult.UseVisualStyleBackColor = true;
            // 
            // LabVinResult
            // 
            this.LabVinResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabVinResult.Font = new System.Drawing.Font("宋体", 49F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabVinResult.Location = new System.Drawing.Point(247, 62);
            this.LabVinResult.Name = "LabVinResult";
            this.LabVinResult.Size = new System.Drawing.Size(860, 77);
            this.LabVinResult.TabIndex = 29;
            this.LabVinResult.Text = "--";
            this.LabVinResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoResult
            // 
            this.InfoResult.BackColor = System.Drawing.Color.Black;
            this.InfoResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InfoResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.InfoResult.Font = new System.Drawing.Font("微软雅黑", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.InfoResult.ForeColor = System.Drawing.Color.Gold;
            this.InfoResult.Location = new System.Drawing.Point(0, 453);
            this.InfoResult.Name = "InfoResult";
            this.InfoResult.Size = new System.Drawing.Size(1342, 130);
            this.InfoResult.TabIndex = 28;
            this.InfoResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabResultAllLdw
            // 
            this.LabResultAllLdw.BackColor = System.Drawing.Color.Lime;
            this.LabResultAllLdw.Font = new System.Drawing.Font("宋体", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabResultAllLdw.Location = new System.Drawing.Point(752, 301);
            this.LabResultAllLdw.Name = "LabResultAllLdw";
            this.LabResultAllLdw.Size = new System.Drawing.Size(355, 67);
            this.LabResultAllLdw.TabIndex = 27;
            this.LabResultAllLdw.Text = "OK";
            this.LabResultAllLdw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("宋体", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label55.Location = new System.Drawing.Point(232, 298);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(399, 67);
            this.label55.TabIndex = 26;
            this.label55.Text = "LDW标定结果";
            // 
            // LabResultAllFcw
            // 
            this.LabResultAllFcw.BackColor = System.Drawing.Color.Lime;
            this.LabResultAllFcw.Font = new System.Drawing.Font("宋体", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabResultAllFcw.Location = new System.Drawing.Point(747, 196);
            this.LabResultAllFcw.Name = "LabResultAllFcw";
            this.LabResultAllFcw.Size = new System.Drawing.Size(360, 67);
            this.LabResultAllFcw.TabIndex = 25;
            this.LabResultAllFcw.Text = "OK";
            this.LabResultAllFcw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("宋体", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label53.Location = new System.Drawing.Point(232, 193);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(399, 67);
            this.label53.TabIndex = 24;
            this.label53.Text = "FCW标定结果";
            // 
            // FrmWork
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 611);
            this.Controls.Add(this.TabX);
            this.Name = "FrmWork";
            this.Text = "FrmWork";
            this.Load += new System.EventHandler(this.FrmWork_Load);
            this.PageLDW.ResumeLayout(false);
            this.PageLDW.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.PageFCW.ResumeLayout(false);
            this.PageFCW.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axiSevenSegmentIntegerX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.PageScan.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AsBusy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsManual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsPosOkVehi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AsPos)).EndInit();
            this.TabX.ResumeLayout(false);
            this.PageWait.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.PageResult.ResumeLayout(false);
            this.PageResult.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort PortScan;
        private System.IO.Ports.SerialPort PortObdWired;
        private System.IO.Ports.SerialPort PortObdWireless;
        private System.Windows.Forms.TabPage PageLDW;
        private System.Windows.Forms.TabPage PageFCW;
        private System.Windows.Forms.TabPage PageScan;
        private System.Windows.Forms.GroupBox groupBox1;
        private AxisDigitalLibrary.AxiLedRectangleX AsManual;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private AxisDigitalLibrary.AxiLedRectangleX AsAuto;
        private AxisDigitalLibrary.AxiLedRectangleX AsPosOkVehi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnWork;
        private System.Windows.Forms.ComboBox CbxVinCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabTitle;
        private System.Windows.Forms.TabControl TabX;
        private System.Windows.Forms.GroupBox groupBox2;
        private DXP.Win.UxCombo CbxModel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabResultLdw;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label LabErrLdw;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TabPage PageResult;
        private System.Windows.Forms.Label LabResultAllFcw;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label LabResultAllLdw;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TabPage PageWait;
        private System.Windows.Forms.Label InfoWait;
        private AxisDigitalLibrary.AxiLedRectangleX AsBusy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabWaitPosFcw;
        private System.Windows.Forms.GroupBox groupBox5;
        private AxisDigitalLibrary.AxiSevenSegmentIntegerX axiSevenSegmentIntegerX3;
        private AxisDigitalLibrary.AxiSevenSegmentIntegerX axiSevenSegmentIntegerX2;
        private AxisDigitalLibrary.AxiSevenSegmentIntegerX axiSevenSegmentIntegerX1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label LabTargetFcwLR;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label LabTargetFcwUD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LabTargetFcwFR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label LabVinWait;
        private System.Windows.Forms.Label LabWaitPosLdw;
        private System.Windows.Forms.GroupBox groupBox6;
        private AxisDigitalLibrary.AxiSevenSegmentIntegerX axiSevenSegmentIntegerX5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private AxisDigitalLibrary.AxiSevenSegmentIntegerX axiSevenSegmentIntegerX6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label LabTargetLdwUD;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label LabTargetLdwFR;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label InfoResult;
        private System.Windows.Forms.Label LabVinLdw;
        private System.Windows.Forms.Label LabVinResult;
        private AxisDigitalLibrary.AxiLedRectangleX AsPos;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label InfoFcw;
        private System.Windows.Forms.Label LabErrFcw;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label LabVinFcw;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label LabResultFcw;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label InfoLdw;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label HiddenInfo;
    }
}