﻿using System;
using System.Windows.Forms;
using ADAS.Appx;
using ADAS.Properties;
using DXP;

namespace ADAS
{
    public partial class FrmApp : Form
    {
        public FrmApp() { InitializeComponent(); }

        private void FrmX_Load(object sender, EventArgs e)
        {
            MaximizedBounds = Screen.FromControl(this).WorkingArea;

            Text = Setting.Current.程序标题;

            BtnWork.Tag = App.WorkForm;
            BtnQuery.Tag = App.QueryForm;
            BtnLogger.Tag = App.LoggerForm;
            BtnAsset.Tag = App.AssetForm;
            BtnModel.Tag = App.ModelForm;
            BtnSetting.Tag = App.ConfigForm;

            SelButton(BtnWork);
            LayoutTools.Enabled = true;
        }

        private void BtnExitTask_Click(object sender, EventArgs e)
        {
            //BeginInvoke(new Action(delegate
            //{
            //    App.TaskExit = true;
            //}));
            App.TaskExit = true;
        }

        private void MnuSysAbort_Click(object sender, EventArgs e)
        {
            App.TaskExit = true;
        }

        #region 模块切换
        private Button CurButton = new Button();
        private void SelButton(Button button)
        {
            if (button == CurButton) { return; }

            Form form = button.Tag as Form;
            if (!LayoutPages.Controls.Contains(button.Tag as Form))
            {
                form.TopLevel = false;
                form.TopMost = false;
                form.FormBorderStyle = FormBorderStyle.None;
                form.Dock = DockStyle.Fill;
                LayoutPages.Controls.Add(form);
            }
            form.Show();
            form.BringToFront();

            CurButton.BackgroundImage = Resources.ButtonSilent;
            button.BackgroundImage = Resources.ButtonActive;
            CurButton = button;

            LayoutPages.Focus();
        }
        private void BtnWork_Click(object sender, EventArgs e) { SelButton(BtnWork); }
        private void BtnQuery_Click(object sender, EventArgs e) { SelButton(BtnQuery); App.QueryForm.QueryToday(); }
        private void BtnLogger_Click(object sender, EventArgs e) { SelButton(BtnLogger); }
        private void BtnAsset_Click(object sender, EventArgs e) { SelButton(BtnAsset); }
        private void BtnModel_Click(object sender, EventArgs e) { SelButton(BtnModel); }
        private void BtnSetting_Click(object sender, EventArgs e) { SelButton(BtnSetting); }
        #endregion

        private void MnuSysExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult.Yes != DxBox.Show("确定要退出程序吗？", "操作确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                e.Cancel = true;
                return;
            }
        }
    }
}
