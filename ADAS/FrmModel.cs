﻿using ADAS.Data;
using DXP;
using FastReport;
using FastReport.Barcode;
using FastReport.Export.Image;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace ADAS
{
    public partial class FrmModel : Form
    {
        public FrmModel()
        {
            InitializeComponent();
        }

        private void FrmModel_Load(object sender, EventArgs e)
        {
            foreach (var protocol in Enum.GetValues(typeof(VehiModel.CAN_PROTOCOL)))
            {
                CbxProtocol.Items.Add(protocol);
            }
            foreach (var security in Enum.GetValues(typeof(VehiModel.SECURITY_LEVEL)))
            {
                CbxSecurity.Items.Add(security);
            }
            ListModel();
        }

        private void GrdModel_SelectionChanged(object sender, EventArgs e)
        {
            VehiModel model = null;
            if (GrdModel.SelectedRows != null && GrdModel.SelectedRows.Count > 0)
            {
                model = GrdModel.SelectedRows[0].DataBoundItem as VehiModel;
            }

            NbxModelID.Value = (model == null ? 0 : model.ModelID);
            TbxModelCode.Text = (model == null ? "" : model.ModelCode);
            NbxVehiHeight.Value = (model == null ? 0 : model.VehiHeight);
            NbxVehiWidth.Value = (model == null ? 0 : model.VehiWidth);

            NbxCameraHeight.Value = (model == null ? 0 : model.CameraHeight);
            NbxCameraNearHead.Value = (model == null ? 0 : model.CameraNearHead);
            NbxCameraNearMiddle.Value = (model == null ? 0 : model.CameraNearMiddle);
            NbxCameraPitch.Value = (model == null ? 0 : model.CameraPitch);
            NbxCameraRoll.Value = (model == null ? 0 : model.CameraRoll);
            NbxCameraYaw.Value = (model == null ? 0 : model.CameraYaw);

            NbxRadarHeight.Value = (model == null ? 0 : model.RadarHeight);
            NbxRadarNearHead.Value = (model == null ? 0 : model.RadarNearHead);
            NbxRadarNearMiddle.Value = (model == null ? 0 : model.RadarNearMiddle);

            NbxPusherForcePercentF.Value = (model == null ? 80 : model.PusherForcePercentF); ;
            NbxPusherForcePercentR.Value = (model == null ? 200 : model.PusherForcePercentR); ;

            BtnModelSave.Enabled = (model != null && model.ModelID > 0);
            BtnModelDel.Enabled = (model != null && model.ModelID > 0);

            CbxProtocol.Text = (model == null ? VehiModel.CAN_PROTOCOL.ISO_15765v4_CAN_29bit_250K.ToString() : model.CanProtocol.ToString());
            CbxSecurity.Text = (model == null ? VehiModel.SECURITY_LEVEL.LEVEL_2.ToString() : model.SecurityLevel.ToString());

            ListRule();
        }

        private void BtnModelSave_Click(object sender, EventArgs e)
        {
            VehiModel model = new VehiModel
            {
                ModelID = (int)NbxModelID.Value,
                ModelCode = TbxModelCode.Text.Trim(),
                VehiHeight = (int)NbxVehiHeight.Value,
                VehiWidth = (int)NbxVehiWidth.Value,
                CameraHeight = (int)NbxCameraHeight.Value,
                CameraNearHead = (int)NbxCameraNearHead.Value,
                CameraNearMiddle = (int)NbxCameraNearMiddle.Value,
                CameraPitch = (int)NbxCameraPitch.Value,
                CameraRoll = (int)NbxCameraRoll.Value,
                CameraYaw = (int)NbxCameraYaw.Value,
                RadarHeight = (int)NbxRadarHeight.Value,
                RadarNearHead = (int)NbxRadarNearHead.Value,
                RadarNearMiddle = (int)NbxRadarNearMiddle.Value,
                PusherForcePercentF = (int)NbxPusherForcePercentF.Value,
                PusherForcePercentR = (int)NbxPusherForcePercentR.Value,
                CanProtocol = (VehiModel.CAN_PROTOCOL)Enum.Parse(typeof(VehiModel.CAN_PROTOCOL), CbxProtocol.Text),
                SecurityLevel = (VehiModel.SECURITY_LEVEL)Enum.Parse(typeof(VehiModel.SECURITY_LEVEL), CbxSecurity.Text),
            };
            if (VehiModel.Save(model))
            {
                DxBox.Show("保存成功", "操作提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ListModel();
            }
            else
            {
                DxBox.Show("保存失败", "操作提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BtnModelNew_Click(object sender, EventArgs e)
        {
            NbxModelID.Value = 0;
            TbxModelCode.Text = "";
            NbxVehiHeight.Value = 0;
            NbxVehiWidth.Value = 0;

            NbxCameraHeight.Value = 0;
            NbxCameraNearHead.Value = 0;
            NbxCameraNearMiddle.Value = 0;
            NbxCameraPitch.Value = 0;
            NbxCameraRoll.Value = 0;
            NbxCameraYaw.Value = 0;

            NbxRadarHeight.Value = 0;
            NbxRadarNearHead.Value = 0;
            NbxRadarNearMiddle.Value = 0;

            NbxPusherForcePercentF.Value = 200;
            NbxPusherForcePercentR.Value = 250;

            CbxProtocol.Text = VehiModel.CAN_PROTOCOL.ISO_15765v4_CAN_29bit_250K.ToString();
            CbxSecurity.Text = VehiModel.SECURITY_LEVEL.LEVEL_2.ToString();

            BtnModelDel.Enabled = false;
            BtnModelSave.Enabled = true;
        }

        private void BtnModelDel_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == DxBox.Show("确定删除本车型吗？", "操作确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                VehiModel.Del((int)NbxModelID.Value);
                ListModel();
            }
        }

        private void ListModel()
        {
            GrdModel.AutoGenerateColumns = false;

            using (BackgroundWorker worker = new BackgroundWorker())
            {
                List<VehiModel> list = new List<VehiModel>();
                worker.DoWork += (xs, xe) => { list = VehiModel.List(); };
                worker.RunWorkerCompleted += (xs, xe) => { GrdModel.DataSource = list; };
                worker.RunWorkerAsync();
            }
        }

        private void ListRule()
        {
            using (BackgroundWorker worker = new BackgroundWorker())
            {
                List<VinRule> list = new List<VinRule>();
                worker.DoWork += (xs, xe) =>
                {
                    list = VinRule.List((int)NbxModelID.Value);
                };
                worker.RunWorkerCompleted += (xs, xe) =>
                {
                    GrdRule.AutoGenerateColumns = false;
                    GrdRule.DataSource = list;
                };
                worker.RunWorkerAsync();
            }
        }

        private void GrdRule_SelectionChanged(object sender, EventArgs e)
        {
            VinRule rule = null;
            if (GrdRule.SelectedRows != null && GrdRule.SelectedRows.Count > 0)
            {
                rule = GrdRule.SelectedRows[0].DataBoundItem as VinRule;
            }
            NbxRuleID.Value = (rule == null ? 0 : rule.RuleID);
            TbxVinMap.Text = (rule == null ? "" : rule.VinMap);

            BtnRuleSave.Enabled = (rule != null && rule.ModelID > 0);
            BtnRuleDel.Enabled = (rule != null && rule.ModelID > 0);
        }

        private void GrdRule_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > -1 && NbxModelID.Value == 0)
            {
                GrdRule_SelectionChanged(null, null);
            }
        }

        private void BtnRuleDel_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == DxBox.Show("确定删除本规则吗？", "操作确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                VinRule.Del((int)NbxRuleID.Value);
                ListRule();
            }
        }

        private void BtnRileNew_Click(object sender, EventArgs e)
        {
            NbxRuleID.Value = 0;
            TbxVinMap.Text = "";

            BtnRuleDel.Enabled = false;
            BtnRuleSave.Enabled = true;
        }

        private void BtnRuleSave_Click(object sender, EventArgs e)
        {
            VinRule rule = new VinRule
            {
                RuleID = (int)NbxRuleID.Value,
                VinMap = TbxVinMap.Text.Trim(),
                ModelID = (int)NbxModelID.Value,
            };
            if (VinRule.Save(rule))
            {
                DxBox.Show("保存成功", "操作提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ListRule();
            }
            else
            {
                DxBox.Show("保存失败", "操作提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GrdModel_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GrdModel.Columns[e.ColumnIndex].Name == "ColQR" && e.RowIndex >= 0)
            {
                GrdModel.Rows[e.RowIndex].Selected = true;
                VehiModel model = GrdModel.Rows[e.RowIndex].DataBoundItem as VehiModel;
                if (model != null)
                {
                    try
                    {
                        using (Report report = new Report())
                        {
                            report.Load(Path.Combine(Application.StartupPath, "Report", "ModelQR.frx"));
                            (report.FindObject("CodeX") as BarcodeObject).Text = model.ModelCode;
                            report.Prepare();
                            using (var export = new ImageExport())
                            {
                                export.ImageFormat = ImageExportFormat.Jpeg;
                                FileInfo file = new FileInfo(Path.Combine(Application.StartupPath, "Report", "Export", string.Format("{0:yyyyMMddHHmmssfff}.jpg", DateTime.Now)));
                                if (!file.Directory.Exists)
                                {
                                    file.Directory.Create();
                                }
                                export.Export(report, file.FullName);
                                DxBox.Show(string.Format("条形码已导出到：\n{0}", file.FullName), "操作提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                    catch { }
                }
            }
        }
    }
}
