﻿using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using ADAS.Appx;
using DXP;
using DXP.Data;

namespace ADAS.Data
{
    class VinRule : XEntity
    {
        public VinRule() : base("VinRule") { }

        public int RuleID { get { return Get("RuleID", 0); } set { Set("RuleID", value, true, true); } }

        public string VinMap { get { return Get("VinMap", ""); } set { Set("VinMap", value); } }
        public int ModelID { get { return Get("ModelID", 0); } set { Set("ModelID", value); } }

        public static List<VinRule> List(int modelId)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                string sql = "SELECT * FROM VinRule WHERE ModelID=@ModelID ORDER BY VinMap ";
                DbParameter[] pms = new DbParameter[] 
                {
                    XContext.NewParameter(XDbType.Access,"ModelID",modelId)
                };

                DxLogger.X("DBA").AddLog(sql);
                DxLogger.X("DBA").AddLog(string.Format("[{0}]", string.Join(", ", pms.Select(o => string.Format("@{0}:{1}", o.ParameterName, o.Value)))));

                XResult<List<VinRule>> result = context.List<VinRule>(sql, pms);
                if (result.Succeed)
                {
                    DxLogger.X("DBA").AddLog(string.Format("Succeed! Data Count:{0}", result.Data.Count));
                }
                else
                {
                    DxLogger.X("DBA").AddLog(string.Format("Falure:{0}", (result.Error == null ? "" : result.Error.ToString())), Color.Tomato);
                }
                return result.Data;
            }
        }

        public static List<VinRule> All()
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                string sql = "SELECT * FROM VinRule ORDER BY VinMap ";

                DxLogger.X("DBA").AddLog(sql);

                XResult<List<VinRule>> result = context.List<VinRule>(sql);
                if (result.Succeed)
                {
                    DxLogger.X("DBA").AddLog(string.Format("Succeed! Data Count:{0}", result.Data.Count));
                }
                else
                {
                    DxLogger.X("DBA").AddLog(string.Format("Falure:{0}", (result.Error == null ? "" : result.Error.ToString())), Color.Tomato);
                }
                return result.Data;
            }
        }


        public static void Del(int id)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                context.DeleteOnCommit(new VinRule { RuleID = id });
                context.CommitChanges();
            }
        }

        public static bool Save(VinRule rule)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                if (rule.RuleID > 0)
                {
                    context.UpdateOnCommit(rule);
                }
                else
                {
                    context.InsertOnCommit(rule);
                }
                return context.CommitChanges();
            }
        }

    }
}
