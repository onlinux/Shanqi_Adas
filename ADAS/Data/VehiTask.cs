﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using ADAS.Appx;
using DXP;
using DXP.Data;

namespace ADAS.Data
{
    class VehiTask : XEntity
    {
        public VehiTask() : base("VehiTask") { }

        #region properties

        public int TaskID { get { return Get("TaskID", 0); } set { Set("TaskID", value, true, true); } }

        public string VinCode { get { return Get("VinCode", ""); } set { Set("VinCode", value); } }
        public DateTime TimeS { get { return Get("TimeS", DateTime.Now); } set { Set("TimeS", value); } }
        public DateTime TimeE { get { return Get("TimeE", DateTime.Now); } set { Set("TimeE", value); } }
        public int ModelID { get { return Get("ModelID", 0); } set { Set("ModelID", value); } }
        public string ModelCode { get { return Get("ModelCode", ""); } set { Set("ModelCode", value); } }
        public bool CaliLdwDone { get { return Get("CaliLdwDone", false); } set { Set("CaliLdwDone", value); } }
        public bool CaliLdwPass { get { return Get("CaliLdwPass", false); } set { Set("CaliLdwPass", value); } }
        public string CaliLdwError { get { return Get("CaliLdwError", ""); } set { Set("CaliLdwError", value); } }
        public bool CaliFcwDone { get { return Get("CaliAebsDone", false); } set { Set("CaliAebsDone", value); } }
        public bool CaliFcwPass { get { return Get("CaliAebsPass", false); } set { Set("CaliAebsPass", value); } }
        public string CaliFcwError { get { return Get("CaliAebsError", ""); } set { Set("CaliAebsError", value); } }

        #endregion

        #region methods

        public void FromVin(string vin)
        {
            VinCode = string.Format("{0}", vin).Trim();
            TimeS = DateTime.Now;

            int modelid = 0;
            foreach (VinRule rule in VinRule.All())
            {
                if (VinCode.Length != rule.VinMap.Length) { continue; }

                for (int i = 0; i < rule.VinMap.Length; i++)
                {
                    if (rule.VinMap[i] != VinCode[i] && rule.VinMap[i] != '?') { continue; }
                }
                modelid = rule.ModelID;
                break;
            }
            if (modelid > 0)
            {
                VehiModel model = VehiModel.Get(modelid);
                ModelID = model.ModelID;
                ModelCode = model.ModelCode;
            }

        }

        public bool Save()
        {
            TimeE = DateTime.Now;

            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                context.InsertOnCommit(this);
                return context.CommitChanges();
            }
        }

        public static List<VehiTask> QueryByDate(DateTime dateS, DateTime dateE)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                string sql = "SELECT * FROM VehiTask WHERE TimeS>=@DateS AND TimeE<@DateE ORDER BY TimeS DESC ";
                DbParameter[] pms = new DbParameter[] 
                { 
                    XContext.NewParameter(XDbType.Access, "DateS", dateS.Date),
                    XContext.NewParameter(XDbType.Access, "DateE", dateE.Date.AddDays(1)),
                };

                DxLogger.X("DBA").AddLog(sql);
                DxLogger.X("DBA").AddLog(string.Format("[{0}]", string.Join(", ", pms.Select(o => string.Format("@{0}:{1}", o.ParameterName, o.Value)))));

                var result = context.List<VehiTask>(sql, pms);
                if (result.Succeed)
                {
                    DxLogger.X("DBA").AddLog(string.Format("Succeed! Data Count:{0}", result.Data.Count));
                }
                else
                {
                    DxLogger.X("DBA").AddLog(string.Format("Falure:{0}", (result.Error == null ? "" : result.Error.ToString())), Color.Tomato);
                }
                return result.Data;
            }
        }

        public static List<VehiTask> QueryByVin(string vin)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                string sql = "SELECT * FROM VehiTask WHERE VinCode LIKE @VinCode ORDER BY TimeS DESC ";
                DbParameter[] pms = new DbParameter[] 
                { 
                    XContext.NewParameter(XDbType.Access, "VinCode", "%"+vin.Trim()+"%"),
                };

                DxLogger.X("DBA").AddLog(sql);
                DxLogger.X("DBA").AddLog(string.Format("[{0}]", string.Join(", ", pms.Select(o => string.Format("@{0}:{1}", o.ParameterName, o.Value)))));

                var result = context.List<VehiTask>(sql, pms);
                if (result.Succeed)
                {
                    DxLogger.X("DBA").AddLog(string.Format("Succeed! Data Count:{0}", result.Data.Count));
                }
                else
                {
                    DxLogger.X("DBA").AddLog(string.Format("Falure:{0}", (result.Error == null ? "" : result.Error.ToString())), Color.Tomato);
                }
                return result.Data;
            }
        }

        #endregion
    }
}
