﻿using ADAS.Appx;
using DXP;
using DXP.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

namespace ADAS.Data
{
    class VehiModel : XEntity
    {
        public VehiModel() : base("VehiModel") { }

        #region properties

        [Browsable(false), CategoryAttribute("车型"), DescriptionAttribute("车型ID")]
        public int ModelID { get { return Get("ModelID", 0); } set { Set("ModelID", value, true, true); } }

        [Browsable(true), CategoryAttribute("车型"), DescriptionAttribute("车型代号")]
        public string ModelCode { get { return Get("ModelCode", ""); } set { Set("ModelCode", value); } }
        [Browsable(true), CategoryAttribute("车型"), DescriptionAttribute("车身高度(mm)")]
        public int VehiHeight { get { return Get("VehiHeight", 0); } set { Set("VehiHeight", value); } }
        [Browsable(true), CategoryAttribute("车型"), DescriptionAttribute("车身宽度(mm)")]
        public int VehiWidth { get { return Get("VehiWidth", 0); } set { Set("VehiWidth", value); } }

        [Browsable(true), CategoryAttribute("摄像头参数"), DescriptionAttribute("摄像头高度(mm)")]
        public int CameraHeight { get { return Get("CameraHeight", 0); } set { Set("CameraHeight", value); } }
        [Browsable(true), CategoryAttribute("摄像头参数"), DescriptionAttribute("摄像头距前轴距离(mm)")]
        public int CameraNearHead { get { return Get("CameraNearHead", 0); } set { Set("CameraNearHead", value); } }
        [Browsable(true), CategoryAttribute("摄像头参数"), DescriptionAttribute("摄像头距中线距离(mm)")]
        public int CameraNearMiddle { get { return Get("CameraNearMiddle", 0); } set { Set("CameraNearMiddle", value); } }
        [Browsable(true), CategoryAttribute("摄像头参数"), DescriptionAttribute("摄像头Pitch")]
        public int CameraPitch { get { return Get("CameraPitch", 0); } set { Set("CameraPitch", value); } }
        [Browsable(true), CategoryAttribute("摄像头参数"), DescriptionAttribute("摄像头Roll")]
        public int CameraRoll { get { return Get("CameraRoll", 0); } set { Set("CameraRoll", value); } }
        [Browsable(true), CategoryAttribute("摄像头参数"), DescriptionAttribute("摄像头Yaw")]
        public int CameraYaw { get { return Get("CameraYaw", 0); } set { Set("CameraYaw", value); } }

        [Browsable(true), CategoryAttribute("雷达参数"), DescriptionAttribute("雷达高度(mm)")]
        public int RadarHeight { get { return Get("RadarHeight", 0); } set { Set("RadarHeight", value); } }
        [Browsable(true), CategoryAttribute("雷达参数"), DescriptionAttribute("雷达距前轴距离(mm)")]
        public int RadarNearHead { get { return Get("RadarNearHead", 0); } set { Set("RadarNearHead", value); } }
        [Browsable(true), CategoryAttribute("雷达参数"), DescriptionAttribute("雷达距中线距离(mm)")]
        public int RadarNearMiddle { get { return Get("RadarNearMiddle", 0); } set { Set("RadarNearMiddle", value); } }

        [Browsable(true), CategoryAttribute("摆正器参数"), DescriptionAttribute("前轴推力百分比(%)")]
        public int PusherForcePercentF { get { return Get("PusherForcePercentF", 0); } set { Set("PusherForcePercentF", value); } }
        [Browsable(true), CategoryAttribute("摆正器参数"), DescriptionAttribute("后轴推力百分比(%)")]
        public int PusherForcePercentR { get { return Get("PusherForcePercentR", 0); } set { Set("PusherForcePercentR", value); } }

        public CAN_PROTOCOL CanProtocol { get { return Get("CanProtocol", CAN_PROTOCOL.ISO_15765v4_CAN_29bit_250K); } set { Set("CanProtocol", value.ToString()); } }
        public SECURITY_LEVEL SecurityLevel { get { return Get("SecurityLevel", SECURITY_LEVEL.LEVEL_2); } set { Set("SecurityLevel", value.ToString()); } }

        public enum CAN_PROTOCOL
        {
            ISO_15765v4_CAN_29bit_250K,
            ISO_15765v4_CAN_29bit_500K,
        }
        public enum SECURITY_LEVEL
        {
            LEVEL_1,
            LEVEL_2,
            LEVEL_3,
        }
        #endregion

        public override string ToString()
        {
            return ModelCode;
        }

        public static VehiModel Get(int id)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                string sql = string.Format("SELECT TOP 1 * FROM VehiModel WHERE ModelID={0} ORDER BY ModelCode", id);
                DxLogger.X("DBA").AddLog(sql);
                XResult<VehiModel> result = context.Single<VehiModel>(sql);
                if (result.Succeed)
                {
                    DxLogger.X("DBA").AddLog(string.Format("Succeed! Data Count:{0}", result.Data));
                }
                else
                {
                    DxLogger.X("DBA").AddLog(string.Format("Falure:{0}", (result.Error == null ? "" : result.Error.ToString())), Color.Tomato);
                }
                return result.Data;
            }
        }

        public static List<VehiModel> List()
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                string sql = "SELECT * FROM VehiModel ORDER BY ModelCode";
                DxLogger.X("DBA").AddLog(sql);
                XResult<List<VehiModel>> result = context.List<VehiModel>(sql);
                if (result.Succeed)
                {
                    DxLogger.X("DBA").AddLog(string.Format("Succeed! Data Count:{0}", result.Data.Count));
                }
                else
                {
                    DxLogger.X("DBA").AddLog(string.Format("Falure:{0}", (result.Error == null ? "" : result.Error.ToString())), Color.Tomato);
                }
                return result.Data;
            }
        }

        public static void Del(int id)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                context.DeleteOnCommit(new VehiModel { ModelID = id });
                context.CommitChanges();
                context.RunSql("DELETE FROM VinRule WHERE ModelID=" + id);
            }
        }

        public static bool Save(VehiModel model)
        {
            using (XContext context = new XContext(XDbType.Access, Setting.Current.数据连接字符串))
            {
                if (model.ModelID > 0)
                {
                    context.UpdateOnCommit(model);
                }
                else
                {
                    context.InsertOnCommit(model);
                }
                return context.CommitChanges();
            }
        }
    }
}
