﻿namespace ADAS
{
    partial class FrmApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmApp));
            this.MenuX = new System.Windows.Forms.MenuStrip();
            this.MnuSys = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuSysAbort = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MnuSysExit = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuHelpHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnWork = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.BtnQuery = new System.Windows.Forms.Button();
            this.BtnLogger = new System.Windows.Forms.Button();
            this.BtnAsset = new System.Windows.Forms.Button();
            this.BtnModel = new System.Windows.Forms.Button();
            this.BtnSetting = new System.Windows.Forms.Button();
            this.LayoutTools = new System.Windows.Forms.FlowLayoutPanel();
            this.BtnExitTask = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.LayoutPages = new System.Windows.Forms.Panel();
            this.MenuX.SuspendLayout();
            this.LayoutTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuX
            // 
            this.MenuX.BackColor = System.Drawing.Color.FloralWhite;
            this.MenuX.GripMargin = new System.Windows.Forms.Padding(0);
            this.MenuX.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuSys,
            this.MnuHelp});
            this.MenuX.Location = new System.Drawing.Point(0, 0);
            this.MenuX.Name = "MenuX";
            this.MenuX.Padding = new System.Windows.Forms.Padding(0);
            this.MenuX.Size = new System.Drawing.Size(1350, 24);
            this.MenuX.TabIndex = 4;
            this.MenuX.Text = "menuStrip1";
            // 
            // MnuSys
            // 
            this.MnuSys.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuSysAbort,
            this.toolStripSeparator1,
            this.MnuSysExit});
            this.MnuSys.Name = "MnuSys";
            this.MnuSys.Size = new System.Drawing.Size(59, 24);
            this.MnuSys.Text = "系统(&S)";
            // 
            // MnuSysAbort
            // 
            this.MnuSysAbort.Name = "MnuSysAbort";
            this.MnuSysAbort.Size = new System.Drawing.Size(180, 22);
            this.MnuSysAbort.Text = "放弃检测";
            this.MnuSysAbort.Click += new System.EventHandler(this.MnuSysAbort_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // MnuSysExit
            // 
            this.MnuSysExit.Name = "MnuSysExit";
            this.MnuSysExit.Size = new System.Drawing.Size(180, 22);
            this.MnuSysExit.Text = "退出";
            this.MnuSysExit.Click += new System.EventHandler(this.MnuSysExit_Click);
            // 
            // MnuHelp
            // 
            this.MnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuHelpHelp,
            this.MnuHelpAbout});
            this.MnuHelp.Name = "MnuHelp";
            this.MnuHelp.Size = new System.Drawing.Size(59, 24);
            this.MnuHelp.Text = "帮助(&H)";
            // 
            // MnuHelpHelp
            // 
            this.MnuHelpHelp.Name = "MnuHelpHelp";
            this.MnuHelpHelp.Size = new System.Drawing.Size(94, 22);
            this.MnuHelpHelp.Text = "帮助";
            // 
            // MnuHelpAbout
            // 
            this.MnuHelpAbout.Name = "MnuHelpAbout";
            this.MnuHelpAbout.Size = new System.Drawing.Size(94, 22);
            this.MnuHelpAbout.Text = "关于";
            // 
            // BtnWork
            // 
            this.BtnWork.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.BtnWork.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnWork.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnWork.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.BtnWork.ForeColor = System.Drawing.Color.SaddleBrown;
            this.BtnWork.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnWork.ImageKey = "IconCalibration.png";
            this.BtnWork.ImageList = this.imageList1;
            this.BtnWork.Location = new System.Drawing.Point(2, 2);
            this.BtnWork.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.BtnWork.Name = "BtnWork";
            this.BtnWork.Size = new System.Drawing.Size(118, 90);
            this.BtnWork.TabIndex = 0;
            this.BtnWork.Text = "主界面";
            this.BtnWork.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnWork.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnWork.UseVisualStyleBackColor = true;
            this.BtnWork.Click += new System.EventHandler(this.BtnWork_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "IconAsset.png");
            this.imageList1.Images.SetKeyName(1, "IconCalibration.png");
            this.imageList1.Images.SetKeyName(2, "IconConfig.png");
            this.imageList1.Images.SetKeyName(3, "IconLogger.png");
            this.imageList1.Images.SetKeyName(4, "IconQuery.png");
            this.imageList1.Images.SetKeyName(5, "IconVehicle.png");
            // 
            // BtnQuery
            // 
            this.BtnQuery.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.BtnQuery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnQuery.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.BtnQuery.ForeColor = System.Drawing.Color.SaddleBrown;
            this.BtnQuery.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnQuery.ImageKey = "IconQuery.png";
            this.BtnQuery.ImageList = this.imageList1;
            this.BtnQuery.Location = new System.Drawing.Point(122, 2);
            this.BtnQuery.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.BtnQuery.Name = "BtnQuery";
            this.BtnQuery.Size = new System.Drawing.Size(118, 90);
            this.BtnQuery.TabIndex = 1;
            this.BtnQuery.Text = "数据查询";
            this.BtnQuery.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnQuery.UseVisualStyleBackColor = true;
            this.BtnQuery.Click += new System.EventHandler(this.BtnQuery_Click);
            // 
            // BtnLogger
            // 
            this.BtnLogger.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.BtnLogger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnLogger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnLogger.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.BtnLogger.ForeColor = System.Drawing.Color.SaddleBrown;
            this.BtnLogger.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnLogger.ImageKey = "IconLogger.png";
            this.BtnLogger.ImageList = this.imageList1;
            this.BtnLogger.Location = new System.Drawing.Point(242, 2);
            this.BtnLogger.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.BtnLogger.Name = "BtnLogger";
            this.BtnLogger.Size = new System.Drawing.Size(118, 90);
            this.BtnLogger.TabIndex = 2;
            this.BtnLogger.Text = "日志记录";
            this.BtnLogger.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnLogger.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnLogger.UseVisualStyleBackColor = true;
            this.BtnLogger.Click += new System.EventHandler(this.BtnLogger_Click);
            // 
            // BtnAsset
            // 
            this.BtnAsset.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.BtnAsset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnAsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAsset.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.BtnAsset.ForeColor = System.Drawing.Color.SaddleBrown;
            this.BtnAsset.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnAsset.ImageKey = "IconAsset.png";
            this.BtnAsset.ImageList = this.imageList1;
            this.BtnAsset.Location = new System.Drawing.Point(362, 2);
            this.BtnAsset.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.BtnAsset.Name = "BtnAsset";
            this.BtnAsset.Size = new System.Drawing.Size(118, 90);
            this.BtnAsset.TabIndex = 3;
            this.BtnAsset.Text = "设备诊断";
            this.BtnAsset.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnAsset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnAsset.UseVisualStyleBackColor = true;
            this.BtnAsset.Click += new System.EventHandler(this.BtnAsset_Click);
            // 
            // BtnModel
            // 
            this.BtnModel.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.BtnModel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnModel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnModel.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.BtnModel.ForeColor = System.Drawing.Color.SaddleBrown;
            this.BtnModel.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnModel.ImageKey = "IconVehicle.png";
            this.BtnModel.ImageList = this.imageList1;
            this.BtnModel.Location = new System.Drawing.Point(482, 2);
            this.BtnModel.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.BtnModel.Name = "BtnModel";
            this.BtnModel.Size = new System.Drawing.Size(118, 90);
            this.BtnModel.TabIndex = 4;
            this.BtnModel.Text = "车型管理";
            this.BtnModel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnModel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnModel.UseVisualStyleBackColor = true;
            this.BtnModel.Click += new System.EventHandler(this.BtnModel_Click);
            // 
            // BtnSetting
            // 
            this.BtnSetting.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.BtnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSetting.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.BtnSetting.ForeColor = System.Drawing.Color.SaddleBrown;
            this.BtnSetting.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnSetting.ImageKey = "IconConfig.png";
            this.BtnSetting.ImageList = this.imageList1;
            this.BtnSetting.Location = new System.Drawing.Point(602, 2);
            this.BtnSetting.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.BtnSetting.Name = "BtnSetting";
            this.BtnSetting.Size = new System.Drawing.Size(118, 90);
            this.BtnSetting.TabIndex = 5;
            this.BtnSetting.Text = "系统设置";
            this.BtnSetting.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnSetting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnSetting.UseVisualStyleBackColor = true;
            this.BtnSetting.Click += new System.EventHandler(this.BtnSetting_Click);
            // 
            // LayoutTools
            // 
            this.LayoutTools.AutoSize = true;
            this.LayoutTools.BackColor = System.Drawing.Color.FloralWhite;
            this.LayoutTools.Controls.Add(this.BtnWork);
            this.LayoutTools.Controls.Add(this.BtnQuery);
            this.LayoutTools.Controls.Add(this.BtnLogger);
            this.LayoutTools.Controls.Add(this.BtnAsset);
            this.LayoutTools.Controls.Add(this.BtnModel);
            this.LayoutTools.Controls.Add(this.BtnSetting);
            this.LayoutTools.Controls.Add(this.BtnExitTask);
            this.LayoutTools.Controls.Add(this.button2);
            this.LayoutTools.Controls.Add(this.button3);
            this.LayoutTools.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LayoutTools.Location = new System.Drawing.Point(0, 635);
            this.LayoutTools.Name = "LayoutTools";
            this.LayoutTools.Size = new System.Drawing.Size(1350, 94);
            this.LayoutTools.TabIndex = 12;
            // 
            // BtnExitTask
            // 
            this.BtnExitTask.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.BtnExitTask.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnExitTask.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnExitTask.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.BtnExitTask.ForeColor = System.Drawing.Color.SaddleBrown;
            this.BtnExitTask.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnExitTask.ImageKey = "IconConfig.png";
            this.BtnExitTask.ImageList = this.imageList1;
            this.BtnExitTask.Location = new System.Drawing.Point(722, 2);
            this.BtnExitTask.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.BtnExitTask.Name = "BtnExitTask";
            this.BtnExitTask.Size = new System.Drawing.Size(118, 90);
            this.BtnExitTask.TabIndex = 6;
            this.BtnExitTask.Text = "放弃检测";
            this.BtnExitTask.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnExitTask.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnExitTask.UseVisualStyleBackColor = true;
            this.BtnExitTask.Click += new System.EventHandler(this.BtnExitTask_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.SaddleBrown;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.ImageKey = "IconConfig.png";
            this.button2.ImageList = this.imageList1;
            this.button2.Location = new System.Drawing.Point(842, 2);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 90);
            this.button2.TabIndex = 7;
            this.button2.Text = "系统登入";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::ADAS.Properties.Resources.ButtonSilent;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.SaddleBrown;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.ImageKey = "IconConfig.png";
            this.button3.ImageList = this.imageList1;
            this.button3.Location = new System.Drawing.Point(962, 2);
            this.button3.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(118, 90);
            this.button3.TabIndex = 8;
            this.button3.Text = "系统登出";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // LayoutPages
            // 
            this.LayoutPages.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LayoutPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutPages.Location = new System.Drawing.Point(0, 24);
            this.LayoutPages.Name = "LayoutPages";
            this.LayoutPages.Size = new System.Drawing.Size(1350, 611);
            this.LayoutPages.TabIndex = 13;
            // 
            // FrmApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.LayoutPages);
            this.Controls.Add(this.LayoutTools);
            this.Controls.Add(this.MenuX);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmX";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmApp_FormClosing);
            this.Load += new System.EventHandler(this.FrmX_Load);
            this.MenuX.ResumeLayout(false);
            this.MenuX.PerformLayout();
            this.LayoutTools.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuX;
        private System.Windows.Forms.ToolStripMenuItem MnuSys;
        private System.Windows.Forms.ToolStripMenuItem MnuSysAbort;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MnuSysExit;
        private System.Windows.Forms.ToolStripMenuItem MnuHelp;
        private System.Windows.Forms.ToolStripMenuItem MnuHelpHelp;
        private System.Windows.Forms.ToolStripMenuItem MnuHelpAbout;
        private System.Windows.Forms.Button BtnWork;
        private System.Windows.Forms.Button BtnModel;
        private System.Windows.Forms.Button BtnAsset;
        private System.Windows.Forms.Button BtnLogger;
        private System.Windows.Forms.Button BtnQuery;
        private System.Windows.Forms.Button BtnSetting;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.FlowLayoutPanel LayoutTools;
        private System.Windows.Forms.Panel LayoutPages;
        private System.Windows.Forms.Button BtnExitTask;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}