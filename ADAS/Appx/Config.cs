﻿using DXP;
using System.ComponentModel;

namespace ADAS.Appx
{
    public static class Config
    {
        private static readonly DxConfig X = DxConfig.X();

        public static class 应用程序
        {
            [Browsable(true), Description("程序标题")]
            public static string 程序标题 { get { return X.Get("App/Title", "杰胜卡特驾驶辅助标定系统"); } set { X.Set("App/Title", value); } }
        }

        public static class 数据库
        {
            [Browsable(true), Description("连接字符串")]
            public static string 连接字符串 { get { return X.Get("Db/DbConnString", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Data\Data.mdb;Persist Security Info=True"); } set { X.Set("Db/DbConnString", value); } }

        }
    }
}
