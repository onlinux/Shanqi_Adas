﻿using DXP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;

namespace ADAS.Appx
{
    [Serializable]
    public class Setting
    {
        #region Current
        private static Setting current = new Setting();
        public static Setting Current { get { return current; } }
        #endregion

        private readonly DxConfig X = DxConfig.X();


        #region 应用程序
        [Browsable(true), Category("[应用程序]"), Description("程序标题")]
        public string 程序标题 { get { return X.Get("App/Title", "杰胜卡特驾驶辅助标定系统"); } set { X.Set("App/Title", value); } }
        #endregion

        #region 数据库
        [Browsable(true), Category("数据库"), Description("数据连接字符串")]
        public string 数据连接字符串 { get { return X.Get("Db/DbConnString", @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Data\Data.mdb;Persist Security Info=True"); } set { X.Set("Db/DbConnString", value); } }
        #endregion

        #region 车型识别方式
        public enum VehiModelReadMode { AutoMatch, ScanOrSelect }
        [Browsable(true), Category("[应用程序]"), Description("车型识别方式(AutoMatch-根据车辆识别码自动匹配,ScanOrInput-扫描车型码或手动选择)")]
        public VehiModelReadMode 车型识别方式 { get { return X.Get("App/VehiModelReadMode", VehiModelReadMode.AutoMatch); } set { X.Set("App/VehiModelReadMode", value.ToString()); } }
        #endregion

        #region 串口-扫码枪
        [Browsable(true), Category("串口参数-扫码枪"), Description("串口号")]
        public int 扫码枪串口号 { get { return X.Get("Scaner/Port", 0); } set { X.Set("Scaner/Port", value.ToString()); } }
        [Browsable(true), Category("串口参数-扫码枪"), Description("波特率")]
        public int 扫码枪波特率 { get { return X.Get("Scaner/BaudRate", 9600); } set { X.Set("Scaner/BaudRate", value.ToString()); } }
        [Browsable(true), Category("串口参数-扫码枪"), Description("校验位")]
        public Parity 扫码枪校验位 { get { return X.Get("Scaner/Parity", Parity.None); } set { X.Set("Scaner/Parity", value.ToString()); } }
        [Browsable(true), Category("串口参数-扫码枪"), Description("数据位")]
        public int 扫码枪数据位 { get { return X.Get("Scaner/DataBits", 8); } set { X.Set("Scaner/DataBits", value.ToString()); } }
        [Browsable(true), Category("串口参数-扫码枪"), Description("停止位")]
        public StopBits 扫码枪停止位 { get { return X.Get("Scaner/StopBits", StopBits.One); } set { X.Set("Scaner/StopBits", value.ToString()); } }
        #endregion

        #region 串口-有线OBD
        [Browsable(true), Category("串口参数-有线OBD"), Description("是否启用")]
        public bool 有线OBD启用 { get { return X.Get("ObdWired/Enabled", true); } set { X.Set("ObdWired/Enabled", value.ToString()); } }
        [Browsable(true), Category("串口参数-有线OBD"), Description("串口号")]
        public int 有线OBD串口号 { get { return X.Get("ObdWired/Port", 0); } set { X.Set("ObdWired/Port", value.ToString()); } }
        [Browsable(true), Category("串口参数-有线OBD"), Description("波特率")]
        public int 有线OBD波特率 { get { return X.Get("ObdWired/BaudRate", 115200); } set { X.Set("ObdWired/BaudRate", value.ToString()); } }
        [Browsable(true), Category("串口参数-有线OBD"), Description("校验位")]
        public Parity 有线OBD校验位 { get { return X.Get("ObdWired/Parity", Parity.None); } set { X.Set("ObdWired/Parity", value.ToString()); } }
        [Browsable(true), Category("串口参数-有线OBD"), Description("数据位")]
        public int 有线OBD数据位 { get { return X.Get("ObdWired/DataBits", 8); } set { X.Set("ObdWired/DataBits", value.ToString()); } }
        [Browsable(true), Category("串口参数-有线OBD"), Description("停止位")]
        public StopBits 有线OBD停止位 { get { return X.Get("ObdWired/StopBits", StopBits.One); } set { X.Set("ObdWired/StopBits", value.ToString()); } }
        #endregion

        #region 串口-无线OBD
        [Browsable(true), Category("串口参数-无线OBD"), Description("是否启用")]
        public bool 无线OBD启用 { get { return X.Get("ObdWireless/Enabled", true); } set { X.Set("ObdWireless/Enabled", value.ToString()); } }
        [Browsable(true), Category("串口参数-无线OBD"), Description("串口号")]
        public int 无线OBD串口号 { get { return X.Get("ObdWireless/Port", 0); } set { X.Set("ObdWireless/Port", value.ToString()); } }
        [Browsable(true), Category("串口参数-无线OBD"), Description("波特率")]
        public int 无线OBD波特率 { get { return X.Get("ObdWireless/BaudRate", 9600); } set { X.Set("ObdWireless/BaudRate", value.ToString()); } }
        [Browsable(true), Category("串口参数-无线OBD"), Description("校验位")]
        public Parity 无线OBD校验位 { get { return X.Get("ObdWireless/Parity", Parity.None); } set { X.Set("ObdWireless/Parity", value.ToString()); } }
        [Browsable(true), Category("串口参数-无线OBD"), Description("数据位")]
        public int 无线OBD数据位 { get { return X.Get("ObdWireless/DataBits", 8); } set { X.Set("ObdWireless/DataBits", value.ToString()); } }
        [Browsable(true), Category("串口参数-无线OBD"), Description("停止位")]
        public StopBits 无线OBD停止位 { get { return X.Get("ObdWireless/StopBits", StopBits.One); } set { X.Set("ObdWireless/StopBits", value.ToString()); } }
        #endregion

        #region 标定参数-FCW
        [Browsable(true), Category("标定参数-FCW"), Description("FCWFCW标定尝试次数")]
        public int FCW标定尝试次数 { get { return X.Get("FCW/FcwTryCount", 2); } set { X.Set("FCW/FcwTryCount", value.ToString()); } }

        [Browsable(true), Category("标定参数-FCW"), Description("FCWFCW原点距前轴中心毫米数(mm)")]
        public int FCW原点距前轴中心毫米数 { get { return X.Get("FCW/ZeroFR", 3315); } set { X.Set("FCW/ZeroFR", value.ToString()); } }
        [Browsable(true), Category("标定参数-FCW"), Description("FCWFCW原点距摆正中线毫米数(mm)")]
        public int FCW原点距摆正中线毫米数 { get { return X.Get("FCW/ZeroLR", 2235); } set { X.Set("FCW/ZeroLR", value.ToString()); } }
        [Browsable(true), Category("标定参数-FCW"), Description("FCW原点距场地地面毫米数(mm)")]
        public int FCW原点距场地地面毫米数 { get { return X.Get("FCW/ZeroUD", 680); } set { X.Set("FCW/ZeroUD", value.ToString()); } }
        [Browsable(true), Category("标定参数-FCW"), Description("FCW标定距离毫米数(mm)")]
        public int FCW标定距离厘米数 { get { return X.Get("FCW/FcwCaliDistance", 180); } set { X.Set("FCW/FcwCaliDistance", value.ToString()); } }
        [Browsable(true), Category("标定参数-FCW"), Description("FCW标定范围厘米数(cm)")]
        public int FCW标定范围厘米数 { get { return X.Get("FCW/FcwCaliRange", 30); } set { X.Set("FCW/FcwCaliRange", value.ToString()); } }
        #endregion

        #region 标定参数-LDW
        [Browsable(true), Category("标定参数-LDW"), Description("LDW标定尝试次数")]
        public int LDW标定尝试次数 { get { return X.Get("LDW/LdwTryCount", 2); } set { X.Set("LDW/LdwTryCount", value.ToString()); } }

        [Browsable(true), Category("标定参数-LDW"), Description("LDW原点距前轴中心毫米数(mm)")]
        public int LDW原点距前轴中心毫米数 { get { return X.Get("LDW/ZeroFR", 6535); } set { X.Set("LDW/ZeroFR", value.ToString()); } }
        [Browsable(true), Category("标定参数-LDW"), Description("LDW原点距场地地面毫米数(mm)")]
        public int LDW原点距场地地面毫米数 { get { return X.Get("LDW/ZeroUD", 4620); } set { X.Set("LDW/ZeroUD", value.ToString()); } }
        [Browsable(true), Category("标定参数-LDW"), Description("LDW标定距离毫米数(mm)")]
        public int LDW标定距离毫米数 { get { return X.Get("LDW/LdwCaliFR", 5100); } set { X.Set("LDW/LdwCaliFR", value.ToString()); } }

        [Browsable(true), Category("标定参数-摄像头标定板"), Description("摄像头左标板中心高厘米数")]
        public int 摄像头左标板中心高厘米数 { get { return X.Get("LDW/BoardHeightLeft", 225); } set { X.Set("LDW/BoardHeightLeft", value.ToString()); } }
        [Browsable(true), Category("标定参数-摄像头标定板"), Description("摄像头右标板中心高厘米数")]
        public int 摄像头右标板中心高厘米数 { get { return X.Get("LDW/BoardHeightRight", 225); } set { X.Set("LDW/BoardHeightRight", value.ToString()); } }

        [Browsable(true), Category("标定参数-摄像头标定板"), Description("摄像头左标板中心距中线厘米数")]
        public int 摄像头左标板中心距中线厘米数 { get { return X.Get("LDW/BoardNearMiddleLeft", 122); } set { X.Set("LDW/BoardNearMiddleLeft", value.ToString()); } }
        [Browsable(true), Category("标定参数-摄像头标定板"), Description("摄像头右标板中心距中线厘米数")]
        public int 摄像头右标板中心距中线厘米数 { get { return X.Get("LDW/BoardNearMiddleRight", 122); } set { X.Set("LDW/BoardNearMiddleRight", value.ToString()); } }

        [Browsable(true), Category("标定参数-摄像头标定板"), Description("图案方格边长毫米数")]
        public int 摄像头标板方格边长毫米数 { get { return X.Get("LDW/BoardGridSize", 175); } set { X.Set("LDW/BoardGridSize", value.ToString()); } }

        [Browsable(true), Category("标定参数-摄像头标定板"), Description("摄像头右标板左下角方块颜色(White-白色，Black-黑色)")]
        public CameraTargetBoardGridColor 摄像头右标板左下角方块颜色 { get { return X.Get("LDW/GridColorInnerBottomRight", CameraTargetBoardGridColor.Black); } set { X.Set("LDW/GridColorInnerBottomRight", value.ToString()); } }
        [Browsable(true), Category("标定参数-摄像头标定板"), Description("摄像头左标板右下角方块颜色(White-白色，Black-黑色)")]
        public CameraTargetBoardGridColor 摄像头左标板右下角方块颜色 { get { return X.Get("LDW/GridColorInnerBottomLeft", CameraTargetBoardGridColor.White); } set { X.Set("LDW/GridColorInnerBottomLeft", value.ToString()); } }

        public enum CameraTargetBoardGridColor { White = 0, Black = 1 }

        #endregion

        #region 操作等待秒数
        [Browsable(true), Category("动作等待"), Description("等待摆正器启动秒数")]
        public int 等待摆正器启动秒数 { get { return X.Get("ActionWait/PusherStartWait", 2); } set { X.Set("ActionWait/PusherStartWait", value.ToString()); } }
        [Browsable(true), Category("动作等待"), Description("等待车辆驶离秒数")]
        public int 等待车辆驶离秒数 { get { return X.Get("ActionWait/VehiLeaveWait", 10); } set { X.Set("ActionWait/VehiLeaveWait", value.ToString()); } }
        #endregion

        #region 扫码历史
        [Browsable(true), Category("[应用程序]"), Description("扫码历史")]
        public List<string> 扫码历史
        {
            get
            {
                List<string> list = new List<string>();
                foreach (var x in X.Get("History/VinList", "").Split(','))
                {
                    if (!string.IsNullOrEmpty(x))
                    {
                        list.Add(x.Trim());
                    }
                }
                return list;
            }
            set
            {
                string list = string.Join(",", value);
                if (list.EndsWith(","))
                {
                    list.Remove(list.Length - 1);
                }
                X.Set("History/VinList", list);
            }
        }
        public void AddVinHistory(string vin)
        {
            string str = string.Format("{0}", vin).Trim().ToUpper();

            if (!string.IsNullOrEmpty(str))
            {
                List<string> list = new List<string>();
                list.Add(str);
                foreach (string x in 扫码历史)
                {
                    if (list.Count >= 5) { break; }
                    if (x != str) { list.Add(x); }
                }
                扫码历史 = list;
            }
        }
        #endregion

    }
}
