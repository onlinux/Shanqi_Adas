﻿
using ADAS.Data;
namespace ADAS.Appx
{
    static class App
    {
        public static FrmApp AppForm { get; private set; }
        public static FrmLogger LoggerForm { get; private set; }
        public static FrmConfig ConfigForm { get; private set; }
        public static FrmModel ModelForm { get; private set; }
        public static FrmAsset AssetForm { get; private set; }
        public static FrmQuery QueryForm { get; private set; }
        public static FrmWork WorkForm { get; private set; }

        static App()
        {
            CurrentTask = new VehiTask();
            CurrentModel = new VehiModel();

            AppForm = new FrmApp();
            LoggerForm = new FrmLogger();
            ConfigForm = new FrmConfig();
            ModelForm = new FrmModel();
            AssetForm = new FrmAsset();
            QueryForm = new FrmQuery();
            WorkForm = new FrmWork();
        }

        public static VehiTask CurrentTask { get; set; }
        public static VehiModel CurrentModel { get; set; }
        public static bool TaskExit { get; set; }
        public static bool TaskBusy { get; set; }
    }
}
